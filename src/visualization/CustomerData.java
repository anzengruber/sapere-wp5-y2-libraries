/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.util.Vector;

import displays.MessagePropagationAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;

public class CustomerData {
	private boolean inUse;
	private String id;
	private Vector<Float> weights;
	private Vector<String> foodPrefs;
	private Vector<String> interests;
	private int SCI;
	private int index = 0;
	
	private String currentChannel;
	private int contentIndex;
	
	public CustomerData() {
		SCI = 0;
		inUse = false;
		id = "";
		weights = new Vector<Float>();
		foodPrefs = new Vector<String>();
		interests = new Vector<String>();
		currentChannel = "";
		contentIndex = -1;
	}

	public void setData(String id, Vector<Float> weights, Vector<String> foodPrefs, 
			Vector<String> interests, String sci, String currentChannel, int contentIndex) {
		this.id = id;
		this.weights = weights;
		this.foodPrefs = foodPrefs;
		this.interests = interests;
		SCI = Integer.parseInt(sci);
		
		if (contentIndex == -1) {
			this.currentChannel = currentChannel;
			this.contentIndex = contentIndex;
		}
		
		inUse = true;
	}
	
	public void reset() {
		SCI = (int) (Math.random()*1000);
		inUse = false;
		id = "";
		weights.clear();
		weights.add(0f);weights.add(0f);weights.add(0f);
		foodPrefs.clear();
		interests.clear();
		currentChannel = "";
		contentIndex = -1;
	}
	
	public String getId() {
		return id;
	}

	public Vector<Float> getWeights() {
		return weights;
	}

	public Vector<String> getFoodPrefs() {
		return foodPrefs;
	}

	public Vector<String> getInterests() {
		return interests;
	}
	
	public int getSCI() {
		return SCI;
	}
	
	public String getCurrentChannel() {
		return currentChannel;
	}
	
	public int getContentIndex() {
		return contentIndex;
	}
	
	public void incContentIndex() {
		contentIndex++;
		sendUserProfile();
	}
	
	public boolean getInUse() {
		return inUse;
	}
	
	private void sendUserProfile() {
		if(id.equals("")) return;
		
		Vector<Property> tmpProperties = new Vector<Property>();
		tmpProperties.add(new Property("sci", ""+SCI));
		tmpProperties.add(new Property("content-channel", currentChannel));
		tmpProperties.add(new Property("content-index", ""+contentIndex));
		tmpProperties.add(new Property(PropertyName.DECAY.toString(), "15"));
		tmpProperties.add(new Property(PropertyName.DESTINATION.toString(), id));
		tmpProperties.add(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
		
		new MessagePropagationAgent("UserProfile:"+id).propagateLSA(tmpProperties);
	}

	public void setNewCurrentChannel(String channel) {
		currentChannel = channel;
		contentIndex = -1;
	}
	
	public String getInterest() {
		index++;
		return interests.elementAt(index % interests.size());
	}
	
	public boolean isInterest(String i) {
		for(String str : interests) {
			if(str.equals((i))) return true;
		}
		return false;
	}
}

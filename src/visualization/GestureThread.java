/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;


import org.OpenNI.ActiveHandEventArgs;
import org.OpenNI.Context;
import org.OpenNI.GeneralException;
import org.OpenNI.GestureGenerator;
import org.OpenNI.GestureRecognizedEventArgs;
import org.OpenNI.HandsGenerator;
import org.OpenNI.IObservable;
import org.OpenNI.IObserver;
import org.OpenNI.InactiveHandEventArgs;
import org.OpenNI.License;
import org.OpenNI.StatusException;

import com.primesense.NITE.NullEventArgs;
import com.primesense.NITE.PointEventArgs;
import com.primesense.NITE.SessionManager;
import com.primesense.NITE.StringPointValueEventArgs;
import com.primesense.NITE.SwipeDetector;
import com.primesense.NITE.VelocityAngleEventArgs;


public class GestureThread extends Thread {
	private GestureTracker tracker;
	private int initAttempts = 5;
	public boolean run;
	
	public GestureThread() {
		while(initAttempts>0) {
			tracker = new GestureTracker();
		}
	}
	
	@Override
	public void run() {
		while(run) {
			tracker.update();
		}
	}

	@Override
	public void destroy() {
		if(run) {
			run = false;
			try {
				tracker.context.stopGeneratingAll();
			} catch (StatusException e) {
				e.printStackTrace();
			}
			tracker.context.release();
			tracker.sessionMan.dispose();
		}
	}
	
	private class GestureTracker {
		/**
		 * 
		 */
	    private Context context;
	    private GestureGenerator gestureGen;
	    private HandsGenerator handsGen;
	    private SessionManager sessionMan;
	    
	    public GestureTracker() {
	    	try {	            
	            context = new Context();

	            // add the NITE Licence 
	            License licence = new License("PrimeSense", "0KOIk2JeIBYClPWVnMoRKn5cdY4="); 
	            context.addLicense(licence);

	            handsGen = HandsGenerator.create(context);    // OpenNI
	            handsGen.SetSmoothing(0.1f);

	            gestureGen = GestureGenerator.create(context);     // OpenNI
	            
	            context.startGeneratingAll();

	            setHandEvents(handsGen);
	            setGestureEvents(gestureGen);

	            try {
					sessionMan = new SessionManager(context, "RaiseHand", "RaiseHand");
					setSessionEvents(sessionMan);
					
					SwipeDetector sd  = initSwipeDetector();
					sessionMan.addListener(sd);
				} catch (GeneralException e) {
					e.printStackTrace();
				}
	            
				initAttempts = 0;
				
				run = true;
				System.out.println("GestureTracking initiated");
				
	        } catch (StatusException e) {
	        	initAttempts--;
	    	} catch (GeneralException e) {
	        	e.printStackTrace();
	        }
	    }
	    
	    private void setHandEvents(HandsGenerator handsGen) {
	      try {
	    	  handsGen.getHandCreateEvent().addObserver( 
	                            new IObserver<ActiveHandEventArgs>() {
	                            	public void update(IObservable<ActiveHandEventArgs> observable, 
	                            			ActiveHandEventArgs args)
	                            	{
	                            		PresentationService.instance().showGestureQuad("new-hand.png");	                            		
	                            	}});
	    	  handsGen.getHandDestroyEvent().addObserver( 
		                        new IObserver<InactiveHandEventArgs>() {
		                            public void update(IObservable<InactiveHandEventArgs> observable,
		                            		InactiveHandEventArgs args)
		                            {
		                            	PresentationService.instance().showGestureQuad("hand-lost.png");
		                            }});
	      }
	      catch (StatusException e) {
	        e.printStackTrace();
	      }
	    }
	    
	    private void setGestureEvents(GestureGenerator gestureGen)
	    {
	      try {
	        gestureGen.getGestureRecognizedEvent().addObserver( new IObserver<GestureRecognizedEventArgs>() {
	          public void update(IObservable<GestureRecognizedEventArgs> observable, 
	                             GestureRecognizedEventArgs args)
	          {
	        	  
	          }
	        });
	      }
	      catch (StatusException e) {
	        e.printStackTrace();
	      }
	    }
	    
		private void setSessionEvents(SessionManager sessionMan) {
		    try {
		      sessionMan.getSessionFocusProgressEvent().addObserver( 
		                          new IObserver<StringPointValueEventArgs>() {
		                        	  public void update(IObservable<StringPointValueEventArgs> observable, 
		                        			  StringPointValueEventArgs args)
		                        	  {}});
		      
		      sessionMan.getSessionStartEvent().addObserver( new IObserver<PointEventArgs>() {
		    	  public void update(IObservable<PointEventArgs> observable, PointEventArgs args)
		    	  {
		    		  PresentationService.instance().showGestureQuad("hand-tracking.png");
		    	  }});
		      
		      sessionMan.getSessionEndEvent().addObserver( new IObserver<NullEventArgs>() {
		    	  public void update(IObservable<NullEventArgs> observable, NullEventArgs args)
		    	  {
		    		  PresentationService.instance().showGestureQuad("stopped-tracking.png");
		    	  }});
		    }
		    catch (StatusException e) {
		    	e.printStackTrace();
		    }
		}
		
		private SwipeDetector initSwipeDetector() {
			SwipeDetector swipeDetector = null;
		    try {
		      swipeDetector = new SwipeDetector();
		      swipeDetector.setMotionSpeedThreshold(swipeDetector.getMotionSpeedThreshold()*2.3f);
		      swipeDetector.getSwipeRightEvent().addObserver(
		    		  new IObserver<VelocityAngleEventArgs>() {
		    			  public void update(IObservable<VelocityAngleEventArgs> observable,
		    					  VelocityAngleEventArgs args)
		    			  {
		    				  PresentationService.instance().moveCamera("Right");
		    			  }
		    		  });

		      swipeDetector.getSwipeLeftEvent().addObserver(
		    		  new IObserver<VelocityAngleEventArgs>() {
		    			  public void update(IObservable<VelocityAngleEventArgs> observable, 
		    					  VelocityAngleEventArgs args)
		    			  {
		    				  PresentationService.instance().moveCamera("Left");
		    			  }
		    		  });
		    }
		    catch (GeneralException e) {
		      e.printStackTrace();
		    }
		    
		    return swipeDetector;
		}
		
		public void update() {
			if(context!=null) {
				try {
					context.waitAnyUpdateAll();
				    sessionMan.update(context);
				} catch (StatusException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.util.Vector;

public class PresentationManager {

	private static PresentationManager instance = null;

	public static PresentationManager instance() {
		if (instance == null) {
			instance = new PresentationManager();
		}
		return instance;
	}

	
	public void showSteeringMsg(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
			PresentationService.instance().updateSteeringMsg(
					currentContent.elementAt(4), 
					currentContent.elementAt(2), 
					currentContent.elementAt(3), 
					currentContent.elementAt(5));
		}
	}
	
	public void showCustomerCount(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
//			int timeslot = Integer.parseInt(currentContent.elementAt(1).split(":")[0]);
//			int currentCustomerCount = Integer.parseInt(currentContent.elementAt(1).split(":")[1]);
//			int overallCustomerCount = Integer.parseInt(currentContent.elementAt(2));
				
		}
	}

	public void showPersonalData(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
			PresentationService.instance().setPersonalData(currentContent);
		}
	}


	public void hidePersonalData(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
			PresentationService.instance().removePersonalData(currentContent.elementAt(1));
		}
	}

	public void setZooming(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
			PresentationService.instance().setCurrentZoomBucket(currentContent.elementAt(0), currentContent.elementAt(1));
		}
	}

	public void setSpatialData(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
			PresentationService.instance().updateSpatialData(
					currentContent.elementAt(2), 
					currentContent.elementAt(3), 
					currentContent.elementAt(4));
		}
		
	}
	
	public void removeSteeringMessage(Vector<String> currentContent) {
		if (!PresentationVars.DISABLE_GUI && PresentationService.instance().event == 0) {
			PresentationService.instance().removeSteeringMsg(currentContent.elementAt(2));
		}
	}
}
/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Vector;

import visualization.components.BehaviorChart;
import visualization.components.BehaviorRatioChart;
import visualization.components.ContentChart;
import visualization.components.ConvFuncChart;
import visualization.components.LsaSpaceVisualization;
import visualization.components.ServusTVParser;
import visualization.components.SteeringMessage;
import visualization.components.ServusTVParser.ContentObject;
import visualization.components.StreamingEngine;
import visualization.components.ZoomingImage;

import com.jme3.app.FlyCamAppState;
import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.FileLocator;
import com.jme3.font.BitmapText;
import com.jme3.font.Rectangle;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

import displays.Start;

/**
 * test
 * 
 * @author AB
 */
public class PresentationService extends SimpleApplication {
	private long articleTime;
	private long lsaSpaceUpdateTime;
	private long lastUpdate, lastCameraMotion;
	public volatile int event;
	private String orientation = "", longitude = "", latitude = "";
	private String nodeName = MyProperties.instance().getProperty("name");
	private long lastButtonEvent = 0;
	private final long buttonSleepTime = 1000;
	
//	----- Gesture Tracking ------------------------------
	private volatile boolean appendGestureQuad = false;
	private Geometry gestureQuad_Geom;
	private Material gestureQuad_Mat;
	private KinectServer tracker;
	private long lastGestureQuadUpdate = 0;
	
//	----- Main Application ------------------------------
	private Node2D contentPanel;
	private ContentChart customerContent;
	private Vector<String> adverts;
	
//	----- Video Variables ---------------------------------
	private StreamingEngine streamingEngine;
	private AWTLoader loader;
	private Texture2D streamTex;
	
//	----- LSA Space Variables ------------------------------
	private Node2D lsaSpacePanel;
	private LsaSpaceVisualization lsaSpaceVisualization;
	private volatile String lsas = "";
	private BitmapText lsaSpace, nodeText;
	
//	----- BarChart Variables -----------------------
	private Node2D firstBehaviorNode, secondBehaviorNode;
	private Node2D firstBehaviorTextNode, secondBehaviorTextNode;
	private BehaviorChart firstBehaviorChart, secondBehaviorChart;
	private BehaviorRatioChart firstBehaviorRatioChart, secondBehaviorRatioChart;
	
//	----- Steering Variables -----------------------------
	private SteeringMessage firstSteeringMessage, secondSteeringMessage;
	
//	----- ConvFuncChart Variables -----------------------
	private Node2D conversionFuncPanel;
	private ConvFuncChart convFuncChart;
	private BitmapText convFuncText;
	float currentConvValue = 0;
	private long convFuncUpdateTime = 60000*1, lastConvFuncUpdate;
	
//	----- CustomerDataPanel Variables -----------------------
	private Node2D customerDataPanel, chartNode, socialNode;
	private Node2D firstSCINode, secondSCINode;
	private ServusTVParser servusTVParser;
	private Vector<String> contentChannels;
	private long lastTextScrollTime;
	private final long TEXTSCROLLDELAY = 1000;
	private boolean firstUp = true, secondUp = true, thirdUp = true, videoUp = true;
	private boolean isNewText = false;
	
//	---- ZoomPanel Variables ------------------
	private Node2D zoomPanel;
	private ZoomingImage zoomingImage;
	private int zoomDir = -1, zoomDis = -1, centerDir = -1, lastDis = -1;
	private float currentFrustrumSize = 0, sourceFrustrumSize = 0, targetFrustrumSize = 0;
	private float currentCamX, sourceCamX, targetCamX;
	boolean newMotion = false;
			
//	---- UserData ------------------
	private volatile CustomerData user_1, user_2;
	
//	---- Social Capital App Data ---------------------------------
	private float[] effort = new float[] {-21, -6, -15, 18, 15, -6, 3, 9, -18, -6};
	private float[] socR = new float[] {-27, -12, 21, 6, 27, -9, 6, 0, 6, -3};
	private float[] perR_1 = new float[] {12, 21, 6, -12, -3, -18, 9, 3, 3, 15};
	private float[] perR_2 = new float[] {-3, 12, 21, -15, -6, -3, -9, -3, 21, 12};
	private int SCO;
	private BitmapText firstMessage, secondMessage;
	
	private static PresentationService instance = null;
	
	public static PresentationService instance() {
		if (instance == null) {
			instance = new PresentationService();
		}
		return instance;
	}

	@Override
	public void destroy() {
		super.destroy();
		if(tracker!=null) tracker.quit();
		if(servusTVParser.clean()) {
			System.exit(0);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void simpleInitApp() {
		servusTVParser = new ServusTVParser();
		this.setPauseOnLostFocus(false);
		
		lastUpdate = System.currentTimeMillis();
		lastCameraMotion = System.currentTimeMillis();
		lastTextScrollTime = System.currentTimeMillis();
		articleTime = new Integer(MyProperties.instance().getProperty("page.turn.time")).intValue()*1000;
		lsaSpaceUpdateTime = 3000;
		lastConvFuncUpdate = System.currentTimeMillis();
		
		SCO = (int)((System.currentTimeMillis()%10000+250000)-(System.currentTimeMillis()%739));
		
		contentChannels = servusTVParser.getChannels();
		
		streamingEngine = new StreamingEngine();
		loader = new AWTLoader();
		streamTex = new Texture2D();
		
		user_1 = new CustomerData();
		user_2 = new CustomerData();
		
//		Init gesture Tracking 
		tracker = new KinectServer();
		tracker.start();
		
		adverts = new Vector<String>();
		File adFolder = new File(PresentationVars.ASSET_PATH+"/Textures/content/advertisements/");
		
		for (File ad : adFolder.listFiles()) {
	        if (!ad.isDirectory()) {
	        	adverts.add(ad.getName());
	        }
	    }
		
		// setup static view
		float frustumSize = PresentationVars.F_SCR_HEIGHT / 2;
		cam.setParallelProjection(true);
		float aspect = (float) cam.getWidth() / cam.getHeight();
		cam.setFrustum(-1000, 1000, -aspect * frustumSize,
				aspect * frustumSize, frustumSize, -frustumSize);
		cam.setLocation(new Vector3f(PresentationVars.F_SCR_WIDTH / 2,
				PresentationVars.F_SCR_HEIGHT / 2, cam.getLocation().z));
				
		// hide scene stats
		guiViewPort.detachScene(guiNode);
		stateManager.detach( stateManager.getState( FlyCamAppState.class ) );
		getInputManager().deleteMapping( SimpleApplication.INPUT_MAPPING_EXIT ); 
		
	    inputManager.addMapping("Left",   new KeyTrigger(KeyInput.KEY_J));
	    inputManager.addMapping("Right",  new KeyTrigger(KeyInput.KEY_K));
	    inputManager.addMapping("Mute",  new KeyTrigger(KeyInput.KEY_M));
	    inputManager.addMapping("ZoomImage",  new KeyTrigger(KeyInput.KEY_Z));
		
	    inputManager.addListener(moveLeftListener, new String[]{"Left"});
	    inputManager.addListener(moveRightListener, new String[]{"Right"});
	    inputManager.addListener(muteListener, new String[]{"Mute"});
	    inputManager.addListener(zoomListener, new String[]{"ZoomImage"});
	    
		// global objects
		PresentationVars.instance().set(PresentationVars.ASSET_MGR, assetManager);
		PresentationVars.instance().set(PresentationVars.TIMER, timer);
		timer.reset();

		// register custom path at asset manager
		assetManager.registerLocator(PresentationVars.ASSET_PATH,
				FileLocator.class.getName());
		
//		----- Init Gesture Quad ----------------------------------------------------------------
		
		Quad gestureQuad = new Quad(PresentationVars.SCR_WIDTH/8, PresentationVars.SCR_WIDTH/8);
		gestureQuad_Geom = new Geometry("gestureQuad_Geom", gestureQuad);
		gestureQuad_Mat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		gestureQuad_Mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		gestureQuad_Geom.setMaterial(gestureQuad_Mat.clone());
		Vector3f camPos = cam.getLocation();
		gestureQuad_Geom.setLocalTranslation(camPos.x-3*PresentationVars.SCR_WIDTH/8, camPos.y+PresentationVars.SCR_HEIGHT, 50);
		rootNode.attachChild(gestureQuad_Geom);
		
//		----- Init LSA space Node --------------------------------------------------------------
		lsaSpacePanel = new Node2D();
		
		lsaSpaceVisualization = new LsaSpaceVisualization(lsaSpacePanel, PresentationVars.SCR_WIDTH, PresentationVars.SCR_HEIGHT);
		
		Quad lsaSpacePanel_Quad = new Quad(PresentationVars.SCR_WIDTH, PresentationVars.SCR_HEIGHT);
		Geometry lsaSpacePanel_Geom = new Geometry("lsaSpacePanel_Geom", lsaSpacePanel_Quad);
		Material lsaSpacePanel_Mat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		lsaSpacePanel_Mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		lsaSpacePanel_Mat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		lsaSpacePanel_Geom.setMaterial(lsaSpacePanel_Mat);
		lsaSpacePanel.attachChild(lsaSpacePanel_Geom);
		lsaSpacePanel.setLocalTranslation(-PresentationVars.SCR_WIDTH, 0, -2);
		
		lsaSpace = new BitmapText(assetManager.loadFont("Interface/Fonts/Default.fnt"), false);
		lsaSpace.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize()/1.2f);
		lsaSpace.setColor(ColorRGBA.Green);   
		lsaSpace.setText("");
		Rectangle rect = new Rectangle(0, 0, PresentationVars.F_SCR_WIDTH-60, PresentationVars.F_SCR_HEIGHT/2-60);
		lsaSpace.setBox(rect);
		lsaSpace.setLocalScale(lsaSpace.getLocalScale().x, 1.7f*lsaSpace.getLocalScale().y, lsaSpace.getLocalScale().z);
		lsaSpace.setLocalTranslation(30, PresentationVars.SCR_HEIGHT/2-30, 0);
		lsaSpacePanel.attachChild(lsaSpace);
		
		nodeText = new BitmapText(assetManager.loadFont("Interface/Fonts/Default.fnt"), false);
		nodeText.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize()*1.2f);
		nodeText.setColor(ColorRGBA.Green);   
		nodeText.setText("");
		nodeText.setLocalTranslation(PresentationVars.SCR_WIDTH/2-nodeText.getLineWidth()/2, 
				PresentationVars.SCR_HEIGHT - 20 - nodeText.getLineHeight(), 0);
		lsaSpacePanel.attachChild(nodeText);
		
//		----- Init Main Content Node --------------------------------------------------------------
		
		contentPanel = new Node2D();
		customerContent = new ContentChart(contentPanel, PresentationVars.SCR_WIDTH, PresentationVars.SCR_HEIGHT);	
		
//		----- Init Social Capital Node --------------------------------------------------------------
		
		conversionFuncPanel = new Node2D();
		Quad lineChartPanelQuad = new Quad(PresentationVars.SCR_WIDTH, PresentationVars.SCR_HEIGHT / 4);
		Geometry lineChartGeom = new Geometry("lineChartGeo", lineChartPanelQuad);
		Material lineChartMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		lineChartMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		lineChartMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		lineChartGeom.setMaterial(lineChartMat);
		conversionFuncPanel.attachChild(lineChartGeom);
		convFuncChart = new ConvFuncChart(conversionFuncPanel, PresentationVars.SCR_WIDTH, PresentationVars.SCR_HEIGHT/4 - PresentationVars.SCR_HEIGHT/12, convFuncUpdateTime);
		Node convFuncChartNode = convFuncChart.getChartNode();
		convFuncChartNode.setLocalTranslation(conversionFuncPanel.getLocalTranslation().x+PresentationVars.SCR_WIDTH/16,
						conversionFuncPanel.getLocalTranslation().y+PresentationVars.SCR_HEIGHT/12,
						conversionFuncPanel.getLocalTranslation().z);
		
		convFuncText = new BitmapText(assetManager.loadFont("Interface/Fonts/Default.fnt"), false);
		convFuncText.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize());
		convFuncText.setColor(ColorRGBA.Green);
		convFuncText.setText("Current Conversion Ratio:\n31 credits : 10 Cents");
		convFuncText.setLocalTranslation(30, PresentationVars.SCR_HEIGHT/24, 0);
		conversionFuncPanel.attachChild(convFuncText);
		
		conversionFuncPanel.setLocalTranslation(0, 3*PresentationVars.SCR_HEIGHT / 4, 0);
		
		
		Quad blackQuad = new Quad(PresentationVars.SCR_WIDTH, 3*PresentationVars.SCR_HEIGHT / 4);
		Geometry blackGeom = new Geometry("blackGeom", blackQuad);
		Material blackMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		blackMat.setColor("Color", ColorRGBA.Black);
		blackGeom.setMaterial(blackMat);
		blackGeom.setLocalTranslation(0, 0, -2);
		
		chartNode = new Node2D();
		chartNode.attachChild(blackGeom);
		chartNode.attachChild(conversionFuncPanel);
		
		//----------------------------------------------------------
		
		Quad firstCapitalQuad = new Quad(PresentationVars.SCR_WIDTH/2, (5*PresentationVars.SCR_HEIGHT-3000)/24);
		Geometry firstCapitalGeom = new Geometry("firstCapitalGeom", firstCapitalQuad);
		Material firstCapitalMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		firstCapitalMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		firstCapitalMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		firstCapitalGeom.setMaterial(firstCapitalMat);
		firstCapitalGeom.setLocalTranslation(0, 
				conversionFuncPanel.getLocalTranslation().y - (5*PresentationVars.SCR_HEIGHT-3000)/24, 
				0);
		
		firstMessage = new BitmapText(assetManager.loadFont("Interface/Fonts/Default.fnt"), false);
		firstMessage.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize());
		firstMessage.setColor(ColorRGBA.Green);
		Rectangle rectFM = new Rectangle(0, 0, PresentationVars.SCR_WIDTH/3, 0);
		firstMessage.setBox(rectFM);
		firstMessage.setText("User: unset\r\n" +
				"SCI: unset Credits; SCO: unset Credits;\r\n" +
				"Value: unset Euro\r\n" +
				"Interests: unset\r\n\r\n" +
				"Recommended Behavior: unset\r\n" +
				"Capital Gained: unset Credits");
		firstMessage.setLocalTranslation(PresentationVars.SCR_WIDTH/4-firstMessage.getLineWidth()/2, 
				conversionFuncPanel.getLocalTranslation().y - (5*PresentationVars.SCR_HEIGHT-3000)/48 + firstMessage.getHeight()/2, 
				0);
		
		firstBehaviorNode = new Node2D();
		Quad firstBarChartPanelQuad = new Quad(PresentationVars.SCR_WIDTH / 2, PresentationVars.SCR_HEIGHT / 3);
		Geometry firstBarChartGeom = new Geometry("firstBarChartGeo", firstBarChartPanelQuad);
		Material firstBarChartMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		firstBarChartMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		firstBarChartMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		firstBarChartGeom.setMaterial(firstBarChartMat);
		firstBehaviorNode.attachChild(firstBarChartGeom);
		firstBehaviorChart = new BehaviorChart(firstBehaviorNode, PresentationVars.SCR_WIDTH / 2, PresentationVars.SCR_HEIGHT / 3);
		Node firstBarChartNode = firstBehaviorChart.getChartNode();
		firstBarChartNode.setLocalTranslation(firstBehaviorNode.getLocalTranslation().x+PresentationVars.SCR_WIDTH/16,
				firstBehaviorNode.getLocalTranslation().y+PresentationVars.SCR_HEIGHT/24,
				firstBehaviorNode.getLocalTranslation().z);	
		firstBehaviorNode.setLocalTranslation(0, 
				firstCapitalGeom.getLocalTranslation().y - PresentationVars.SCR_HEIGHT / 3, 
				0);
		
		firstBehaviorTextNode = new Node2D();
		Quad firstBehaviorTextQuad = new Quad(PresentationVars.SCR_WIDTH / 2, (5*PresentationVars.SCR_HEIGHT-3000)/24);
		Geometry firstBehaviorTextGeom = new Geometry("firstBehaviorTextGeo", firstBehaviorTextQuad);
		Material firstBehaviorTextMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		firstBehaviorTextMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		firstBehaviorTextMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		firstBehaviorTextGeom.setMaterial(firstBehaviorTextMat);
		firstBehaviorTextNode.attachChild(firstBehaviorTextGeom);
		firstBehaviorTextNode.setLocalTranslation(0, 
				firstBehaviorNode.getLocalTranslation().y - (5*PresentationVars.SCR_HEIGHT-3000)/24, 
				0);
		
		firstBehaviorRatioChart = new BehaviorRatioChart(firstBehaviorTextNode, PresentationVars.SCR_WIDTH / 2, (5*PresentationVars.SCR_HEIGHT-3000)/24-20);
		Node firstBehaviorRatioNode = firstBehaviorRatioChart.getChartNode();
		firstBehaviorRatioNode.setLocalTranslation(firstBehaviorRatioNode.getLocalTranslation().x+PresentationVars.SCR_WIDTH/32,
				firstBehaviorRatioNode.getLocalTranslation().y+(5*PresentationVars.SCR_HEIGHT-3000)/(24*16)+20,
				firstBehaviorRatioNode.getLocalTranslation().z);	
		
		firstSCINode = new Node2D();
		firstSteeringMessage = new SteeringMessage(firstSCINode, "computing");
		Node firstSteeringNode = firstSteeringMessage.getChartNode();
		firstSteeringNode.setLocalTranslation(0, 
				firstBehaviorTextNode.getLocalTranslation().y - 250, 
				0);	

		firstSCINode.attachChild(firstMessage);
		firstSCINode.attachChild(firstCapitalGeom);
		firstSCINode.attachChild(firstBehaviorNode);
		firstSCINode.attachChild(firstBehaviorTextNode);
		
		//----------------------------------------------------------
		
		Quad secondCapitalQuad = new Quad(PresentationVars.SCR_WIDTH/2, (5*PresentationVars.SCR_HEIGHT-3000)/24);
		Geometry secondCapitalGeom = new Geometry("secondCapitalGeom", secondCapitalQuad);
		Material secondCapitalMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		secondCapitalMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		secondCapitalMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		secondCapitalGeom.setMaterial(secondCapitalMat);
		secondCapitalGeom.setLocalTranslation(PresentationVars.SCR_WIDTH/2, 
				conversionFuncPanel.getLocalTranslation().y - (5*PresentationVars.SCR_HEIGHT-3000)/24,
				0);

		secondMessage = new BitmapText(assetManager.loadFont("Interface/Fonts/Default.fnt"), false);
		secondMessage.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize());
		secondMessage.setColor(ColorRGBA.Green);
		Rectangle rectSM = new Rectangle(0, 0, PresentationVars.SCR_WIDTH/3, 0);
		secondMessage.setBox(rectSM);
		secondMessage.setText("User: unset\r\n" +
				"SCI: unset Credits; SCO: unset Credits;\r\n" +
				"Value: unset Euro\r\n" +
				"Interests: unset\r\n\r\n" +
				"Recommended Behavior: unset\r\n" +
				"Capital Gained: unset Credits");
		secondMessage.setLocalTranslation(3*PresentationVars.SCR_WIDTH/4-secondMessage.getLineWidth()/2, 
				conversionFuncPanel.getLocalTranslation().y - (5*PresentationVars.SCR_HEIGHT-3000)/48 + secondMessage.getHeight()/2, 
				0);
		
		secondBehaviorNode = new Node2D();
		Quad secondBarChartPanelQuad = new Quad(PresentationVars.SCR_WIDTH / 2, PresentationVars.SCR_HEIGHT / 3);
		Geometry secondBarChartGeom = new Geometry("barChartGeo", secondBarChartPanelQuad);
		Material secondBarChartMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		secondBarChartMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		secondBarChartMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		secondBarChartGeom.setMaterial(secondBarChartMat);
		secondBehaviorNode.attachChild(secondBarChartGeom);
		secondBehaviorChart = new BehaviorChart(secondBehaviorNode, PresentationVars.SCR_WIDTH / 2, PresentationVars.SCR_HEIGHT / 3);
		Node secondBarChartNode = secondBehaviorChart.getChartNode();
		secondBarChartNode.setLocalTranslation(secondBehaviorNode.getLocalTranslation().x+PresentationVars.SCR_WIDTH/16,
				secondBehaviorNode.getLocalTranslation().y+PresentationVars.SCR_HEIGHT/24,
				secondBehaviorNode.getLocalTranslation().z);	
		secondBehaviorNode.setLocalTranslation(PresentationVars.SCR_WIDTH/2, 
				secondCapitalGeom.getLocalTranslation().y - PresentationVars.SCR_HEIGHT / 3, 
				0);
		
		secondBehaviorTextNode = new Node2D();
		Quad secondBehaviorTextQuad = new Quad(PresentationVars.SCR_WIDTH / 2, (5*PresentationVars.SCR_HEIGHT-3000)/24);
		Geometry secondBehaviorTextGeom = new Geometry("secondBehaviorTextGeo", secondBehaviorTextQuad);
		Material secondBehaviorTextMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		secondBehaviorTextMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		secondBehaviorTextMat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/console_bg_dark.png"));
		secondBehaviorTextGeom.setMaterial(secondBehaviorTextMat);
		secondBehaviorTextNode.attachChild(secondBehaviorTextGeom);
		secondBehaviorTextNode.setLocalTranslation(PresentationVars.SCR_WIDTH/2, 
				secondBehaviorNode.getLocalTranslation().y - (5*PresentationVars.SCR_HEIGHT-3000)/24, 
				0);
		
		secondBehaviorRatioChart = new BehaviorRatioChart(secondBehaviorTextNode, PresentationVars.SCR_WIDTH / 2, (5*PresentationVars.SCR_HEIGHT-3000)/24-20);
		Node secondBehaviorRatioNode = secondBehaviorRatioChart.getChartNode();
		secondBehaviorRatioNode.setLocalTranslation(secondBehaviorRatioNode.getLocalTranslation().x+PresentationVars.SCR_WIDTH/32,
				secondBehaviorRatioNode.getLocalTranslation().y+(5*PresentationVars.SCR_HEIGHT-3000)/(24*16)+20,
				secondBehaviorRatioNode.getLocalTranslation().z);	
		
		secondSCINode = new Node2D();
		secondSteeringMessage = new SteeringMessage(secondSCINode, "computing");
		Node secondSteeringNode = secondSteeringMessage.getChartNode();
		secondSteeringNode.setLocalTranslation(PresentationVars.SCR_WIDTH/2, 
				secondBehaviorTextNode.getLocalTranslation().y - 250,
				0);
		
		secondSCINode.attachChild(secondCapitalGeom);
		secondSCINode.attachChild(secondMessage);
		secondSCINode.attachChild(secondBehaviorNode);
		secondSCINode.attachChild(secondBehaviorTextNode);
		
//		----------------------------------------------------------------
		
		socialNode = new Node2D();
		socialNode.attachChild(firstSCINode);
		socialNode.attachChild(secondSCINode);
		
		customerDataPanel = new Node2D();
		customerDataPanel.setLocalTranslation(PresentationVars.SCR_WIDTH, 0, 0);
		customerDataPanel.attachChild(chartNode);
		customerDataPanel.attachChild(socialNode);
		
//		----- Init ZoomImage --------------------------------------------------------------
		zoomPanel = new Node2D();
		zoomingImage = new ZoomingImage(zoomPanel, PresentationVars.SCR_WIDTH, PresentationVars.SCR_HEIGHT);
		zoomPanel.setLocalTranslation(-PresentationVars.SCR_WIDTH/2, 2*PresentationVars.SCR_HEIGHT, 0);
		
//		----- Attach to root --------------------------------------------------------------
		
		rootNode.attachChild(lsaSpacePanel);
		rootNode.attachChild(customerDataPanel);
		rootNode.attachChild(contentPanel);
		rootNode.attachChild(zoomPanel);
		
		Start.startSapere();
	}

	public void moveCamera(String dir) {
		lastCameraMotion = System.currentTimeMillis();
		
		Vector3f v = cam.getLocation();
		if(dir.equals("Left")) {
			if(v.x > 0)
				cam.setLocation(new Vector3f(v.x - PresentationVars.SCR_WIDTH, v.y, v.z));
			
			showGestureQuad("swipe-left.png");
		}
		else if(dir.equals("Right")) {
			if(v.x < PresentationVars.SCR_WIDTH)
				cam.setLocation(new Vector3f(v.x + PresentationVars.SCR_WIDTH, v.y, v.z));
			
			showGestureQuad("swipe-right.png");
		}
	}
	
	private AnalogListener moveLeftListener = new AnalogListener() {
		
		public void onAnalog(String name, float value, float tpf) {
			if (name.equals("Left") && System.currentTimeMillis()-lastButtonEvent>buttonSleepTime) {
				lastCameraMotion = System.currentTimeMillis();
				lastButtonEvent = System.currentTimeMillis();
				Vector3f v = cam.getLocation();
				if(v.x > 0)
					cam.setLocation(new Vector3f(v.x - PresentationVars.SCR_WIDTH, v.y, v.z));
			}
		}
	};
	
	private AnalogListener moveRightListener = new AnalogListener() {
		public void onAnalog(String name, float value, float tpf) {
			if (name.equals("Right") && System.currentTimeMillis()-lastButtonEvent>buttonSleepTime) {
				lastCameraMotion = System.currentTimeMillis();
				lastButtonEvent = System.currentTimeMillis();
				Vector3f v = cam.getLocation();
				if(v.x < PresentationVars.SCR_WIDTH)
					cam.setLocation(new Vector3f(v.x + PresentationVars.SCR_WIDTH, v.y, v.z));
			}
		}
	};
	
	private AnalogListener muteListener = new AnalogListener() {
		public void onAnalog(String name, float value, float tpf) {
			if (name.equals("Mute") && System.currentTimeMillis()-lastButtonEvent>buttonSleepTime) {
				streamingEngine.toggleMute();
				lastButtonEvent = System.currentTimeMillis();
			}
		}
	};
	
	private AnalogListener zoomListener = new AnalogListener() {
		public void onAnalog(String name, float value, float tpf) {
			if (name.equals("ZoomImage") && System.currentTimeMillis()-lastButtonEvent>buttonSleepTime) {
				
				Vector3f v = cam.getLocation();
				if(v.y < PresentationVars.F_SCR_HEIGHT) {
					float frustumSize = PresentationVars.F_SCR_HEIGHT / 2;
					float aspect = (float) cam.getWidth() / cam.getHeight();
					cam.setFrustum(-1000, 1000, -aspect * frustumSize,
							aspect * frustumSize, 0, -frustumSize);
					
					
					cam.setLocation(new Vector3f(PresentationVars.F_SCR_WIDTH/2, v.y+PresentationVars.F_SCR_HEIGHT*2, v.z));
				}
				else {
					float frustumSize = PresentationVars.F_SCR_HEIGHT / 2;
					float aspect = (float) cam.getWidth() / cam.getHeight();
					cam.setFrustum(-1000, 1000, -aspect * frustumSize,
							aspect * frustumSize, frustumSize, -frustumSize);
					
					cam.setLocation(new Vector3f(PresentationVars.F_SCR_WIDTH/2, v.y-PresentationVars.F_SCR_HEIGHT*2, v.z));
				}
			
				lastButtonEvent = System.currentTimeMillis();
			}
		}
	};
	
	@Override
	public void simpleUpdate(float tpf) {
		this.gainFocus();
		
		
		SCO = (int)((System.currentTimeMillis()%10000+250000)-(System.currentTimeMillis()%4700));
		
		// Update ZoomImage
		if(cam.getLocation().y > PresentationVars.F_SCR_HEIGHT) {
			setZoomingFrustrum();
			cam.update();
		}
	
		// Video Streaming update
		if(cam.getLocation().x == PresentationVars.F_SCR_WIDTH / 2) {
			streamingEngine.resume();
			Geometry videoImage_Geom = (Geometry)contentPanel.getChild("videoGeom");
			Material videoImage_Mat = videoImage_Geom.getMaterial();
			BufferedImage img = streamingEngine.getFrame();				
			if(img != null) {
				streamTex.setImage(loader.load(img, true));
				videoImage_Mat.setTexture("ColorMap", streamTex.clone());
			}
		}
		else streamingEngine.pause();
		
		if(lastCameraMotion != 0 && System.currentTimeMillis()-lastCameraMotion > 120000) {
//			cam.setLocation(new Vector3f(PresentationVars.F_SCR_WIDTH / 2,
//					PresentationVars.F_SCR_HEIGHT / 2, cam.getLocation().z));
			lastCameraMotion = 0;
		}
		
		if(appendGestureQuad) {
			gestureQuad_Geom.setMaterial(gestureQuad_Mat.clone());
			Vector3f camPos = cam.getLocation();
			gestureQuad_Geom.setLocalTranslation(camPos.x-3*PresentationVars.SCR_WIDTH/8, camPos.y+PresentationVars.SCR_HEIGHT/3-30, 50);
			
			lastGestureQuadUpdate = System.currentTimeMillis();
			appendGestureQuad = false;
		}
		
		if(lastGestureQuadUpdate != 0 && System.currentTimeMillis()-lastGestureQuadUpdate > 2000) {
			gestureQuad_Geom.setLocalTranslation(gestureQuad_Geom.getLocalTranslation().x,
					gestureQuad_Geom.getLocalTranslation().y+PresentationVars.SCR_HEIGHT,
					gestureQuad_Geom.getLocalTranslation().z);
			lastGestureQuadUpdate = 0;
		}

//		Update LSA space
		if(System.currentTimeMillis()-lastUpdate > lsaSpaceUpdateTime) {
			lsaSpace.setText(lsas);
			nodeText.setText(nodeName+": Orientation: "+orientation+"; Longitude: "+longitude+"; Latitude: "+latitude);
			nodeText.setLocalTranslation(PresentationVars.SCR_WIDTH/2-nodeText.getLineWidth()/2, 
					PresentationVars.SCR_HEIGHT - 20 - nodeText.getLineHeight(), 0);
			lsaSpaceVisualization.update(lsas);
//			lastUpdate = 
		}
		
		lsaSpacePanel.update();
		
		if(user_1.getInUse()) {
//		Show User2
			firstSCINode.setLocalTranslation(0, 0, 0);
				
//		Update CustomerTexts
			//true == get data for first user
			firstMessage.setText(getCustomerText(true));
//		Update BehaviorChart
			//true == get behaviors for first user		
			float[] behaviors = getUtilityValues(true);
			
			firstBehaviorChart.updateBehaviors(behaviors);
			firstBehaviorChart.setBars();
			firstBehaviorChart.setLabelValues();
//			Update BehaviorRatioChart			
			firstBehaviorRatioChart.setBars();
//		Update SteeringInfo
			firstSteeringMessage.update();
		}
		else {
			firstSCINode.setLocalTranslation(PresentationVars.SCR_WIDTH, 0, 0);
		}
		firstSCINode.update();
		
		if(user_2.getInUse()) {
//		Show User2
			secondSCINode.setLocalTranslation(0, 0, 0);
			
//		Update CustomerTexts
			//false == get data for second user
			secondMessage.setText(getCustomerText(false));
//			Update BehaviorChart
			//false == get behaviors for second user
			float[] behaviors = getUtilityValues(false);
			
			secondBehaviorChart.updateBehaviors(behaviors);
			secondBehaviorChart.setBars();
			secondBehaviorChart.setLabelValues();
//			Update BehaviorRatioChart			
			secondBehaviorRatioChart.setBars();
//			Update SteeringInfo
			secondSteeringMessage.update();
		}
		else {
			secondSCINode.setLocalTranslation(PresentationVars.SCR_WIDTH, 0, 0);
		}		
		secondSCINode.update();
		
//		Update Conversion Function Chart
		if(System.currentTimeMillis()-lastConvFuncUpdate > convFuncUpdateTime) {
			currentConvValue = getCurrentConversionValue();
			convFuncText.setText(String.format("Current Conversion Ratio:\n%.2f credits : 10 Cents", currentConvValue ));
			convFuncChart.update(currentConvValue);				
			convFuncChart.setSpline();
			convFuncChart.setTimes();
			lastConvFuncUpdate = System.currentTimeMillis();
		}
		
		BitmapText videoText = (BitmapText)contentPanel.getChild("videoText");
		BitmapText firstText = (BitmapText)contentPanel.getChild("firstText");
		BitmapText secondText = (BitmapText)contentPanel.getChild("secondText");
		BitmapText thirdText = (BitmapText)contentPanel.getChild("thirdText");
		
		
		if(System.currentTimeMillis() - lastUpdate > articleTime) {
			isNewText = true;
			if(contentChannels.isEmpty()) contentChannels = servusTVParser.getChannels();
			
			Geometry firstImage_Geom = (Geometry)contentPanel.getChild("firstImageGeom");
			Material firstImage_Mat = firstImage_Geom.getMaterial();
			if(firstImage_Mat == null) firstImage_Mat = new Material();
		
			Geometry secondImage_Geom = (Geometry)contentPanel.getChild("secondImageGeom");
			Material secondImage_Mat = secondImage_Geom.getMaterial();
			if(secondImage_Mat == null) secondImage_Mat = new Material();
			
			Geometry thirdImage_Geom = (Geometry)contentPanel.getChild("thirdImageGeom");
			Material thirdImage_Mat = thirdImage_Geom.getMaterial();
			if(thirdImage_Mat == null) thirdImage_Mat = new Material();
			
			Geometry video_Geom = (Geometry)contentPanel.getChild("videoGeom");
			Material video_Mat = video_Geom.getMaterial();
			if(video_Mat == null) video_Mat = new Material();
			
			String firstChannel = ""; int firstIndex = -1;
			String secondChannel = ""; int secondIndex = -1;
			String thirdChannel = ""; int thirdIndex = -1;
			String videoChannel = ""; int videoIndex = -1;
			
			int playVideoForUser = 0;
			
			if(user_2.getInUse()) {
				secondChannel = user_2.getCurrentChannel();
				secondIndex = user_2.getContentIndex();
				playVideoForUser = 2;
				
				if(secondIndex >= servusTVParser.channelSize(secondChannel) || !user_2.isInterest(secondChannel)) {
					String nextChannel = user_2.getInterest();
					if(nextChannel.equals(secondChannel))
						nextChannel = user_2.getInterest();
					
					secondChannel = nextChannel;
					secondIndex = 0;
					user_2.setNewCurrentChannel(secondChannel);
				}
			}
			if(user_1.getInUse()) {
				firstChannel = user_1.getCurrentChannel();
				firstIndex = user_1.getContentIndex();
				playVideoForUser = 1;
				
				if(firstIndex >= servusTVParser.channelSize(firstChannel) || !user_1.isInterest(firstChannel)) {
					String nextChannel = user_1.getInterest();
					if(nextChannel.equals(firstChannel))
						nextChannel = user_1.getInterest();
					
					firstChannel = nextChannel;
					firstIndex = 0;
					user_1.setNewCurrentChannel(firstChannel);
				}
			}
			
//			--------------------------------------------------------------------------------------------
			
			boolean firstChannelValid = false, secondChannelValid = false;
			String firstChannelPrefix = "", secondChannelPrefix = "";
			for(String tmp:servusTVParser.getChannels()) {
				if(tmp.equals(firstChannel)) {
					firstChannelValid = true;
					firstChannelPrefix = user_1.getId() + " - ";
				}
				else if(tmp.equals(secondChannel)) {
					secondChannelValid = true;
					secondChannelPrefix = user_2.getId() + " - ";
				}
			}
			
			if(!firstChannelValid) {
				do {
					int randIndex = (int)(Math.floor(Math.random()*(contentChannels.size()-0.01)));
					firstChannel = contentChannels.elementAt(randIndex);
				} while(firstChannel.equals(secondChannel));
			}
			if(!secondChannelValid) {
				do{
					int randIndex = (int)(Math.floor(Math.random()*(contentChannels.size()-0.01)));
					secondChannel = contentChannels.elementAt(randIndex);
				} while(firstChannel.equals(secondChannel));
			}
			
			do {
				int randIndex = (int)(Math.floor(Math.random()*(contentChannels.size()-0.01)));
				videoChannel = contentChannels.elementAt(randIndex);
			}
			while(videoChannel.equals(secondChannel) || videoChannel.equals(firstChannel));
			
			do {
				int randIndex = (int)(Math.floor(Math.random()*(contentChannels.size()-0.01)));
				thirdChannel = contentChannels.elementAt(randIndex);
			}
			while(thirdChannel.equals(secondChannel) || thirdChannel.equals(firstChannel) || thirdChannel.equals(videoChannel));
			
//			--------------------------------------------------------------------------------------------
						
			if(servusTVParser.getNextContentTexture(firstChannel, firstIndex)) {
				user_1.incContentIndex();
				ContentObject pair = servusTVParser.getContentObject(firstChannel);
				if(pair != null) {
					if(pair.getTexture() != null) {
						firstImage_Mat.setTexture("ColorMap", pair.getTexture());
						firstText.setText(firstChannelPrefix + firstChannel+":\n"+pair.getText());
						firstText.updateModelBound();
					}
					
					if(playVideoForUser == 1) {
						if(user_1.isInterest(pair.getChannel()) && URLexists(pair.getVideoURL())) {
							if(streamingEngine.displayVideo(pair.getVideoURL())) {
								videoText.setText(firstChannelPrefix + firstChannel+":\n"+pair.getText());
								videoText.updateModelBound();
							}
						}
						else playVideoForUser = 0;
					}
				}
			}
			
			if(servusTVParser.getNextContentTexture(secondChannel, secondIndex)) {
				user_2.incContentIndex();
				ContentObject pair = servusTVParser.getContentObject(secondChannel);
				if(pair != null) {
					if(pair.getTexture() != null) {
						secondImage_Mat.setTexture("ColorMap", pair.getTexture());
						secondText.setText(secondChannelPrefix + secondChannel+":\n"+pair.getText());
						secondText.updateModelBound();
					}
					
					if(playVideoForUser == 2) {
						if(user_2.isInterest(pair.getChannel()) && URLexists(pair.getVideoURL())) {
							if(streamingEngine.displayVideo(pair.getVideoURL())) {
								videoText.setText(secondChannelPrefix + secondChannel+":\n"+pair.getText());
								videoText.updateModelBound();
							}
						}
						else playVideoForUser = 0;
					}
				}
			}
			
			int length = servusTVParser.channelSize(thirdChannel);
			thirdIndex = (int)Math.floor(length*(Math.random()-0.01));
			if(servusTVParser.getNextContentTexture(thirdChannel, thirdIndex)) {
				ContentObject pair = servusTVParser.getContentObject(thirdChannel);
				if(pair != null) {
					if(pair.getTexture() != null) {
						thirdImage_Mat.setTexture("ColorMap", pair.getTexture());
						thirdText.setText(thirdChannel+":\n"+pair.getText());
						thirdText.updateModelBound();
					}
				}
			}
			
			if(playVideoForUser == 0) {
				streamingEngine.stop();
				
				length = servusTVParser.channelSize(videoChannel);
				videoIndex = (int)Math.floor(length*(Math.random()-0.01));
				
				if(servusTVParser.getNextContentTexture(videoChannel, videoIndex)) {
					ContentObject pair = servusTVParser.getContentObject(videoChannel);
					
					if(pair != null) {
						video_Mat.setTexture("ColorMap", pair.getTexture());
						videoText.setText(videoChannel+":\n"+pair.getText());
						videoText.updateModelBound();
					}
				}
			}
			
			contentPanel.update();
			lastUpdate = System.currentTimeMillis();
		}
		
		if(System.currentTimeMillis() - lastTextScrollTime > TEXTSCROLLDELAY) {
			boolean finished = animateText(firstText, firstUp, isNewText);
			finished = animateText(secondText, secondUp, isNewText) && finished;
			finished = animateText(thirdText, thirdUp, isNewText) && finished;
			finished = animateText(videoText, videoUp, isNewText) && finished;
			if(finished) {
				firstUp = !firstUp; secondUp = !secondUp; thirdUp = !thirdUp; videoUp = !videoUp;
				lastTextScrollTime = System.currentTimeMillis();
			}
			isNewText = false;
		}
		
		// update visualization nodes
		chartNode.update();
		
		socialNode.update();
		
		// update main panel
		customerDataPanel.update();
		lsaSpacePanel.update();
		contentPanel.update();
	}

	private boolean animateText(BitmapText text, boolean up, boolean isNewText) {
		boolean finished = false;
		
		// firstText Locations
		float thisStartX = customerContent.getTextOffset(text.getName());
		Vector3f thisCurrentTranslation = text.getLocalTranslation();
		
		if(isNewText) {
			float thisBoxHeight = customerContent.getElementHeight()-20;
			
			text.setLocalTranslation(thisStartX, thisCurrentTranslation.y, thisCurrentTranslation.z);
			text.setBox(new Rectangle(0, 0, PresentationVars.SCR_WIDTH/4 - 20, thisBoxHeight));
			
			text.updateGeometricState();
			text.updateModelBound();
			
			float thisTextHeight = text.getLineCount()*text.getLineHeight();
			
			if(thisTextHeight > thisBoxHeight) {
				float newW = (PresentationVars.SCR_WIDTH/4)*thisTextHeight / thisBoxHeight;
				text.setBox(new Rectangle(0, 0, newW+20, thisBoxHeight));
			}
		}
		
		float textWidth = text.getLineWidth();
		float boxWidth = PresentationVars.SCR_WIDTH/4 - 20;
		
		if(textWidth > boxWidth+10) {
			float distance = textWidth - boxWidth;
			if(up) {
				if(thisCurrentTranslation.x > (thisStartX - distance))
					text.setLocalTranslation(
							thisCurrentTranslation.x - 1, 
							thisCurrentTranslation.y,
							thisCurrentTranslation.z);
				else finished = true;
			}
			else {
				text.setLocalTranslation(
					thisStartX, 
					thisCurrentTranslation.y,
					thisCurrentTranslation.z);
				finished = true;
			}
		}
		else finished = true;
		
		return finished;
	}

	public boolean URLexists(String URLName) {
	      boolean result = false;
	      try {
	    	  URL url = new URL(URLName);

	          InputStream input = url.openStream();
	          input.close();
	          
	          result = true;
	      } catch (Exception ex) {
	    	  System.out.println("Video not available");
	      }
	      return result;
	}
	
	private void setZoomingFrustrum() {
		float frustrumSize = PresentationVars.F_SCR_HEIGHT / 2;
		if(newMotion) {
			if(currentFrustrumSize == 0) currentFrustrumSize = frustrumSize;
			if(sourceFrustrumSize == 0) sourceFrustrumSize = frustrumSize;
			sourceFrustrumSize = currentFrustrumSize;
			
			sourceCamX = currentCamX;
			
			newMotion = false;
		}
		
		cam.setParallelProjection(true);
		float aspect = (float) cam.getWidth() / cam.getHeight();
		float left, right, top, bottom;
		
		targetFrustrumSize = getFrustrumSize(frustrumSize, zoomDis);
		float delta = (sourceFrustrumSize - targetFrustrumSize);
		
		if(Math.abs(currentFrustrumSize - targetFrustrumSize) < Math.abs(delta/10) || delta == 0) {
			currentFrustrumSize = targetFrustrumSize;
		}
		else currentFrustrumSize -= delta/10;
		
		left = -aspect * currentFrustrumSize;
		right = aspect * currentFrustrumSize;
		top = currentFrustrumSize;
		bottom = -currentFrustrumSize;

		cam.setFrustum(-1000, 1000, left, right, top, bottom);
		
		targetCamX = getCamX(zoomDir);
		delta = (sourceCamX - targetCamX);
		
		if(Math.abs(currentCamX - targetCamX) < Math.abs(delta/10) || delta == 0) {
			currentCamX = targetCamX;
		}
		else currentCamX -= delta/10;
		
		cam.setLocation(new Vector3f(currentCamX, cam.getLocation().y, cam.getLocation().z));
	}

	private float getCamX(int zoomDir) {
		float camX = 0;
		
		switch(zoomDir) {
		case 0:
			camX = 0;
			break;
		case 1:
			camX = PresentationVars.F_SCR_WIDTH/2;
			break;
		case 2:
			camX = PresentationVars.F_SCR_WIDTH;
			break;
		}
		
		return camX;
	}
	
	private float getFrustrumSize(float frustrumSize, int zoomDis) {
		switch(zoomDis) {
		case 0:
			frustrumSize /= 4;
			break;
		case 1:
			frustrumSize /= 2;
			break;
		case 2:
			frustrumSize = 3*frustrumSize/4;
			break;
		case 3:
			break;
	}
		return frustrumSize;
	}

	private String getCustomerText(boolean firstUser) {
		String id;
		int sci;
		Vector<String> interests;
		Vector<String> rec;
		
		if(firstUser) {
			id = user_1.getId();
			sci = user_1.getSCI();
			interests = user_1.getInterests();
			rec = firstBehaviorChart.getRecommendation();
		}
		else {
			id = user_2.getId();
			sci = user_2.getSCI();
			interests = user_2.getInterests();
			rec = secondBehaviorChart.getRecommendation();
		}
		
		float convParam = convFuncChart.getConversion();
		String result = String.format("User: %s\r\n" +
				"SCI: %d Credits; SCO: %d Credits;\r\n" +
				"Value: %.2f Euro\r\n" +
				"Interests: %s\r\n\r\n" +
				"Recommended Behavior: %s\r\n" +
				"Capital Gained: %s Credits", 
				id, sci, SCO, ((float)sci/convParam)/10f, interests.toString(), rec.elementAt(0), rec.elementAt(1)
				);
		return result;
	}

	private float[] getUtilityValues(boolean firstUser) {
		float[] result = new float[10];
		
		Vector<Float> weights;
		if(firstUser) {
			weights = user_1.getWeights();
			
			if(weights.size() == 3)
				for(int i = 0; i< 10; i++) {
					float social = weights.elementAt(0)*socR[i];
					float eff = weights.elementAt(1)*effort[i];
					float personal = weights.elementAt(2)*perR_1[i];
					
					result[i] = eff + personal + social;
					
					eff = Math.abs(eff); personal = Math.abs(personal); social = Math.abs(social);
					float sum = eff+personal+social;
					firstBehaviorRatioChart.setVal(i, eff/sum, personal/sum, social/sum);
				}
		}
		else {
			weights = user_2.getWeights();
			
			if(weights.size() == 3)
				for(int i = 0; i< 10; i++) {

					float social = weights.elementAt(0)*socR[i];
					float eff = weights.elementAt(1)*effort[i];
					float personal = weights.elementAt(2)*perR_2[i];
					
					result[i] = eff + personal + social;
					
					eff = Math.abs(eff); personal = Math.abs(personal); social = Math.abs(social);
					float sum = eff+personal+social;
					secondBehaviorRatioChart.setVal(i, eff/sum, personal/sum, social/sum);
				}
		}
		return result;
	}

	private float getCurrentConversionValue() {
		return (float)SCO/13200f;
	}
	
	public void showGestureQuad(String gesture) {
		gestureQuad_Mat.setTexture("ColorMap", assetManager.loadTexture("Textures/gui/"+gesture));		
		appendGestureQuad = true;
	}
	
	public void updateSteeringMsg(String sid, String dir, String or, String nextHop) {
		SteeringMessage m;
		String id = sid.split(":")[0];
		
		if(id != null && id.equals(user_1.getId()))
			m = firstSteeringMessage;
		else if(id != null && id.equals(user_2.getId()))
			m = secondSteeringMessage;
		else return;
		
		m.setId(id);
		m.updateSteering(Float.parseFloat(dir.split(":")[0]));
		m.updateTarget(sid.split(":")[1]);
		m.updateNextHop(nextHop);
		m.updateDistance(Float.parseFloat(dir.split(":")[1]));
	}

	public void updateSpatialData(String or, String longitude, String latitude) {
		this.orientation = or;
		this.longitude = longitude;
		this.latitude = latitude;
		
		firstSteeringMessage.updateOrientation(Float.parseFloat(or));
		secondSteeringMessage.updateOrientation(Float.parseFloat(or));
	}
	
	public void setPersonalData(Vector<String> currentContent) {
		String id = currentContent.elementAt(1);
		String sci = currentContent.elementAt(2);
		Vector<Float> weights = new Vector<Float>();
		Vector<String> personal = new Vector<String>();
		Vector<String> foods;
		
		weights.add(Float.parseFloat(currentContent.elementAt(3))/100);
		weights.add(Float.parseFloat(currentContent.elementAt(4))/100);
		weights.add(Float.parseFloat(currentContent.elementAt(5))/100);
		
		String currentChannel = currentContent.elementAt(6);
		int contentIndex = Integer.parseInt(currentContent.elementAt(7));
		
		for(int i = 8; i<currentContent.size(); i++)
			personal.add(currentContent.elementAt(i));
		
		boolean setUser1 = false, setUser2 = false;
		
		if(user_1.getId().equals(id))
			setUser1 = true;
		else if(user_2.getId().equals(id))
			setUser2 = true;
		else if(!user_1.getInUse())
			setUser1 = true;
		else if(!user_2.getInUse())
			setUser2 = true;
	
		if(setUser1) {
			foods = user_1.getFoodPrefs();
			user_1.setData(id, weights, foods, personal, sci, currentChannel, contentIndex);
		}
		else if(setUser2) {
			foods = user_2.getFoodPrefs();
			user_2.setData(id, weights, foods, personal, sci, currentChannel, contentIndex);
		}
	}

	public void setLSASpace(String tmp) {
		lsas = tmp;
	}

	public void removePersonalData(String id) {
		if(user_1.getId().equals(id)) user_1.reset();
		if(user_2.getId().equals(id)) user_2.reset();
	}
	
	public void setCurrentZoomBucket(String distance, String direction) {
		int dis = Integer.parseInt(distance);
		int dir = Integer.parseInt(direction);
		
		zoomDis = dis;
		
		if((dir+2)%16 == centerDir) {
			centerDir--;
			if(centerDir < 0) centerDir += 16;
		}
		else {
			int tmpDir = (dir-2);
			if(tmpDir < 0) tmpDir += 16;
			if(tmpDir == centerDir) {
				centerDir=(centerDir+1)%16;
			}
		}
			
		if(zoomDir == -1) {
			zoomDir = 1; centerDir = dir;
		}
		else if(dir == 15 && centerDir == 0) zoomDir = 0;
		else if(dir == 0 && centerDir == 15) zoomDir = 2;
		else if(dir > centerDir) zoomDir = 2;
		else if(dir < centerDir) zoomDir = 0;
		else zoomDir = 1;
		
//		if(zoomDir>2) zoomDir = 2;
//		else if(zoomDir < 0) zoomDir = 0;
		
		newMotion = true;
		
		lastDis = dis;
	}

	public void removeSteeringMsg(String sid) {
		SteeringMessage m;
		String id = sid.split(":")[0];
		
		if(id != null && id.equals(user_1.getId()))
			m = firstSteeringMessage;
		else if(id != null && id.equals(user_2.getId()))
			m = secondSteeringMessage;
		else return;
		
		if(m.getTarget().equals(sid.split(":")[1])) {
			m.reset();
		}
	}

	public void parseKinectInput(String message) {
		if(message.equals("Left") || message.equals("Right")) 
			PresentationService.instance().moveCamera(message);
		else showGestureQuad(message);
	}
}
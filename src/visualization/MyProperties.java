/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * This class stores the properties of the application and loads them from an
 * XML file in the file system.
 * 
 * @author Thomas Schmittner
 * 
 */
public class MyProperties extends Properties {

	public static final String BTFOUND = "BTFOUND";
	public static final String BTLOST = "BTLOST";
	public static final String STORE = "STORE";
	public static final String GETID = "GETID ";
	public static final String SHOW_STEERING_MSG = "SHSTMSG";
	public static final String HIDE_STEERING_MSG = "HDSTMSG";
	public static final String CONTENT_TAG = "content";
	public static final String DESCRIPTION_TAG = "description";
	public static final String DISPLAY_TIME_TAG = "displayTime";
	public static final String ID_TAG = "id";
	public static final String KEYWORDS_TAG = "keywords";
	public static final String PART_TAG = "part";
	
	
	private static final long serialVersionUID = 1L;
	private static final String PROPERTIES_PATH = ".\\conf\\demonstrator.properties";
	private static final String NAME_PATH = "c:\\.sapere\\sapere.name";
	private static MyProperties instance;

	/**
	 * Private default constructor. Loads the application properties from a Java
	 * properties file located in the file system.
	 */
	private MyProperties() {
		try {
			load(new FileInputStream(PROPERTIES_PATH));
			load(new FileInputStream(NAME_PATH));
		} catch (FileNotFoundException e) {
			System.err
					.println("Properties file [demonstrator.properties] not found in expected location " + PROPERTIES_PATH);
			System.exit(-1);
		} catch (IOException e) {
			System.err
					.println("Properties file could not be read successfully. Please check file format and content.");
			System.exit(-1);
		}
	}

	/**
	 * Returns the instance of this singleton class and creates a new object one
	 * if none has been created yet.
	 * 
	 * @return The instance of this class.
	 */
	public static MyProperties instance() {
		if (instance == null) {
			return new MyProperties();
		} else {
			return instance;
		}
	}
}
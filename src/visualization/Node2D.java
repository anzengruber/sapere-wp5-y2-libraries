/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import com.jme3.scene.Node;
import com.jme3.math.Vector2f;
import com.jme3.math.FastMath;
import com.jme3.system.Timer;

public class Node2D extends Node {

	private boolean translationInProgress, scaleInProgress;
	private float translationStartTime, scaleStartTime;
	private float translationPeakTime, scalePeakTime;
	private float translationAcceleration, scaleAcceleration;
	private float translationDeceleration, scaleDeceleration;
	private float translationPeakDistance, scalePeakDistance;
	private float translationTotalDistance, scaleTotalDistance;
	private Vector2f translationStart, scaleStart;
	private Vector2f translationDest, scaleDest;
	private Vector2f translationDirection, scaleDirection;
	
	public Node2D() {
		reset();
	}

	public void update() {
		float _timeAbsolute = ((Timer) (PresentationVars.instance()
				.get(PresentationVars.TIMER))).getTimeInSeconds();

		if (translationInProgress == true
				&& _timeAbsolute >= translationStartTime) {
			float _distance = 0;

			if (_timeAbsolute >= translationPeakTime) {
				float _time = _timeAbsolute - translationPeakTime;
				float _vMax = (translationPeakTime - translationStartTime)
						* translationAcceleration;
				_distance = translationPeakDistance;
				_distance += _vMax * _time;
				_distance -= (float) (0.5 * translationDeceleration * (_time * _time));
			} else {
				float _time = _timeAbsolute - translationStartTime;
				_distance = (float) (0.5 * translationAcceleration * (_time * _time));
			}

			if (_distance >= translationTotalDistance || _distance < 0) {
				setLocalTranslation(translationDest.x, translationDest.y, getLocalTranslation().z);
				reset();
			} else {
				
				Vector2f _dest = translationStart.add(translationDirection.mult(_distance));
				setLocalTranslation(_dest.x, _dest.y, getLocalTranslation().z);
			}
		}
		
		_timeAbsolute = ((Timer) (PresentationVars.instance()
				.get(PresentationVars.TIMER))).getTimeInSeconds();
		
		if (scaleInProgress == true
				&& _timeAbsolute >= scaleStartTime) {
			float _distance = 0;

			if (_timeAbsolute >= scalePeakTime) {
				float _time = _timeAbsolute - scalePeakTime;
				float _vMax = (scalePeakTime - scaleStartTime)
						* scaleAcceleration;
				_distance = scalePeakDistance;
				_distance += _vMax * _time;
				_distance -= (float) (0.1 * scaleDeceleration * (_time * _time));
			} else {
				float _time = _timeAbsolute - scaleStartTime;
				_distance = (float) (0.1 * scaleAcceleration * (_time * _time));
			}

			if (_distance >= scaleTotalDistance || _distance < 0) {
				setLocalScale(scaleDest.x, scaleDest.y, getLocalScale().z);
				reset();
			} else {
				Vector2f _dest = scaleStart.add(scaleDirection.mult(_distance));
				setLocalScale(_dest.x, _dest.y, getLocalScale().z);
			}
		}
	}

	// evenly accelerated/decelerated movement
	public void moveAccelerated(int destX, int destY, float acceleration,
			float maxSpeedAtPercent) {

		float _time = ((Timer) (PresentationVars.instance().get(PresentationVars.TIMER)))
				.getTimeInSeconds();

		translationInProgress = true;
		translationStartTime = _time;
		translationAcceleration = acceleration;
		translationDeceleration = translationAcceleration * 0.99f; // makes sure
																	// that
																	// object
																	// will pass
																	// finish
																	// line
		translationStart = new Vector2f(getLocalTranslation().x,
				getLocalTranslation().y);
		translationDest = new Vector2f(destX, destY);
		translationDirection = translationDest.subtract(translationStart)
				.normalize();

		// compute distance, peak distance and time
		translationTotalDistance = translationDest.distance(translationStart);
		translationPeakDistance = translationTotalDistance * maxSpeedAtPercent;
		translationPeakTime = translationStartTime
				+ FastMath.sqrt(2 * translationPeakDistance
						/ translationAcceleration);

	}

	private void reset() {
		translationInProgress = false;
		translationStartTime = Float.MIN_VALUE;
		translationPeakTime = Float.MIN_VALUE;
		translationAcceleration = Float.MIN_VALUE;
		translationDeceleration = Float.MIN_VALUE;
		translationPeakDistance = Float.MIN_VALUE;
		translationTotalDistance = Float.MIN_VALUE;
		translationStart = new Vector2f(0, 0);
		translationDest = new Vector2f(0, 0);
		translationDirection = new Vector2f(0, 0);
		
		scaleInProgress = false;
		scaleStartTime = Float.MIN_VALUE;
		scalePeakTime = Float.MIN_VALUE;
		scaleAcceleration = Float.MIN_VALUE;
		scaleDeceleration = Float.MIN_VALUE;
		scalePeakDistance = Float.MIN_VALUE;
		scaleTotalDistance = Float.MIN_VALUE;
		scaleStart = new Vector2f(0, 0);
		scaleDest = new Vector2f(0, 0);
		scaleDirection = new Vector2f(0, 0);
	}

	public boolean isTranslationInProgress() {
		return translationInProgress;
	}
	
	public boolean isScaleInProgress() {
		return scaleInProgress;
	}

	public void scaleAccelerated(float destX, float destY, float acceleration, float maxSpeedAtPercent) {

		float _time = ((Timer) (PresentationVars.instance().get(PresentationVars.TIMER)))
				.getTimeInSeconds();

		scaleInProgress = true;
		scaleStartTime = _time;
		scaleAcceleration = acceleration;
		scaleDeceleration = scaleAcceleration * 0.99f; 	// makes sure
														// that
														// object
														// will pass
														// finish
														// line
		scaleStart = new Vector2f(getLocalScale().x,getLocalScale().y);
		scaleDest = new Vector2f(destX, destY);
		scaleDirection = scaleDest.subtract(scaleStart).normalize();

		// compute distance, peak distance and time
		scaleTotalDistance = scaleDest.distance(scaleStart);
		scalePeakDistance = scaleTotalDistance * maxSpeedAtPercent;
		scalePeakTime = scaleStartTime + FastMath.sqrt(2 * scalePeakDistance / scaleAcceleration);
	}
}

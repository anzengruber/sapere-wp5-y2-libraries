/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.util.Vector;

public class VisualizationService implements VisualizationListener {
		
	@Override
	public void fireContentsChanged(String agentName, Vector<String> currentContent) {
		updateVisualization(agentName, currentContent);
	}
	
	private void updateVisualization(String agentName, Vector<String> currentContent) {		
		if(agentName.equals("Spatial Agent")) {
			PresentationManager.instance().setZooming(currentContent);
		}
		else if(agentName.equals("Customer Information Agent")) {
			if(currentContent.elementAt(0).equals("Count")) {
				PresentationManager.instance().showCustomerCount(currentContent);
			}
			else if(currentContent.elementAt(0).equals("Personal")) {
				PresentationManager.instance().showPersonalData(currentContent);
			}
			else if(currentContent.elementAt(0).equals("Delete")) {
				PresentationManager.instance().hidePersonalData(currentContent);
			}
		}
		else if(agentName.equals("Steering Agent")) {
			if(currentContent.elementAt(1).equals("Show")) {
				PresentationManager.instance().showSteeringMsg(currentContent);
			}
			else if(currentContent.elementAt(1).equals("Data")) {
				PresentationManager.instance().setSpatialData(currentContent);
			}
			else if(currentContent.elementAt(1).equals("Hide")) {
				PresentationManager.instance().removeSteeringMessage(currentContent);
			}
		}
		else if(agentName.equals("Customer Count Agent")) {
			PresentationManager.instance().showCustomerCount(currentContent);
		}
	}
}

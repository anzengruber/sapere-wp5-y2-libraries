/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import visualization.Node2D;
import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;

public class SteeringMessage extends Geometry {

	public static final int MESSAGE_HEIGHT = 250;

	private Compass compass;
	private float steeringDirection;
	private float orientation;
	private String id, nextHop;
	private Node parent;
	private Node2D panel;
	private BitmapText steeringMessage;
	private float distance;
	private String target;
	
	public SteeringMessage(Node parent, String id) {
		this.setId(id);
		this.nextHop = "computing";
		this.parent = parent;
		orientation = 0;
		steeringDirection = 0;
		target = "computing";
		
		init();
	}
	
	public SteeringMessage(Node parent, String id, float dir, float or, String nextHop) {
		this.setId(id);
		this.nextHop = nextHop;
		this.parent = parent;
		orientation = or;
		steeringDirection = dir;
		
		init();
	}
	
	public void reset() {
		this.nextHop = "computing";
		steeringDirection = 0;
		target = "computing";
	}

	public void init() {
		panel = new Node2D();
		AssetManager _assetMgr = (AssetManager) PresentationVars.instance().get(PresentationVars.ASSET_MGR);
				
		Quad messageQuad = new Quad(PresentationVars.SCR_WIDTH/2, 250);
		Geometry messageGeom = new Geometry("messageQuad:"+id, messageQuad);
		Material messageMat = new Material(_assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
		messageMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		messageMat.setTexture("ColorMap", _assetMgr.loadTexture("Textures/gui/console_bg_dark.png"));
		messageGeom.setMaterial(messageMat);
		panel.attachChild(messageGeom);
		
		compass = new Compass(panel);
		
		steeringMessage = new BitmapText(_assetMgr.loadFont("Interface/Fonts/Default.fnt"), false);
		steeringMessage.setSize(_assetMgr.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize());
		steeringMessage.setColor(ColorRGBA.Green);
		steeringMessage.setText("Steering ID: "+id+". \r\nTarget: "+nextHop+".\r\nNext Destination: "+nextHop+". \r\nDistance: "+distance+"m"); // crosshairs
		steeringMessage.setLocalTranslation(panel.getLocalTranslation().x + 300f, panel.getLocalTranslation().y + MESSAGE_HEIGHT/2 + steeringMessage.getHeight()/2, panel.getLocalTranslation().z);
        panel.attachChild(steeringMessage);

		// attach to parent
		parent.attachChild(panel);
	}

	public void updateSteering(float dir) {
		steeringDirection = dir;
	}
	
	public void updateOrientation(float or) {
		orientation = or;
	}
	
	public void updateNextHop(String nextHop) {
		this.nextHop = nextHop;
	}
	
	public void update() {
		compass.updateInfo(steeringDirection, orientation);
		steeringDirection = 0.0f;
		orientation = 0.0f;

		steeringMessage.setText("Steering ID: "+id+". \r\nTarget: "+target+".\r\nNext Destination: "+nextHop+". \r\nDistance: "+distance+" m"); // crosshairs
		
		if (panel != null) {
			panel.update();
		}
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}
	
	public String getTarget() {
		return target;
	}

	public Node getChartNode() {
		return panel;
	}

	public void updateTarget(String target) {
		this.target = target;		
	}

	public void updateDistance(float distance) {
		this.distance = distance;
	}
}

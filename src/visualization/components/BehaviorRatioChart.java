/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Quad;

public class BehaviorRatioChart extends Geometry {
	private float HEIGHT;
	private int INTERVALL_WIDTH;
	private static final int DEFAULT_LINE_WIDTH = 3;
	private static final int behaviorCount = 10;
	
	private AssetManager assetMgr;
	private BitmapFont guiFont;
	
	private Node chartNode;
	private Line xAxis;
	
	private float[] effort = new float[10];
	private float[] social = new float[10];
	private float[] personal = new float[10];
	
	public BehaviorRatioChart(Node parent, int width, int height) {
		assetMgr = (AssetManager) PresentationVars.instance().get(
				PresentationVars.ASSET_MGR);

		HEIGHT = 3*height/4;
		INTERVALL_WIDTH = (7*width/8) /behaviorCount;
		
		guiFont = assetMgr.loadFont("Interface/Fonts/Default.fnt");
		chartNode = new Node();
		parent.attachChild(chartNode);

		// add x-axis
		xAxis = new Line(new Vector3f(0, 0, 0), new Vector3f(INTERVALL_WIDTH*behaviorCount+10, 0, 0));
		xAxis.setLineWidth(DEFAULT_LINE_WIDTH);
		putShape(chartNode, new Geometry("x-axis", xAxis), ColorRGBA.White,
				new Vector3f(-5, 0, 0));
		
		for (int i = 0; i <= behaviorCount; i++) {
			Line line = new Line(new Vector3f(i * INTERVALL_WIDTH, -1, 0),
					new Vector3f(i * INTERVALL_WIDTH, +1, 0));
			putShape(chartNode, new Geometry("line" + i, line),
					ColorRGBA.White, new Vector3f());
		}

		Line yAxis = new Line(new Vector3f(0, 0, 0), new Vector3f(0, HEIGHT+10, 0));
		yAxis.setLineWidth(DEFAULT_LINE_WIDTH);
		putShape(chartNode, new Geometry("y-axis", yAxis), ColorRGBA.White,
				new Vector3f(0, -5, 0));
		
		// add labels
		BitmapText label;
		label = new BitmapText(guiFont);
		label.setText("ratios");
		label.setColor(ColorRGBA.White);
		label.setLocalTranslation(INTERVALL_WIDTH*behaviorCount/2, HEIGHT+label.getLineHeight()+20, 0);
		chartNode.attachChild(label);
		
		setBars();
		setLabelValues();
		
		BitmapText eLabel = new BitmapText(guiFont);
		eLabel.setText("Effort");
		eLabel.setColor(ColorRGBA.White);
		eLabel.setLocalTranslation(INTERVALL_WIDTH*behaviorCount/4 - eLabel.getLineWidth()/2 , HEIGHT+eLabel.getLineHeight(), 0);
		chartNode.attachChild(eLabel);
		
		Quad e_quad = new Quad(INTERVALL_WIDTH/5, INTERVALL_WIDTH/5);
		Geometry e_geo = new Geometry("e_quad", e_quad);
		putShape(chartNode, e_geo, ColorRGBA.White, new Vector3f(
				INTERVALL_WIDTH*behaviorCount/4 - eLabel.getLineWidth()/2 - INTERVALL_WIDTH/5, 
				HEIGHT+eLabel.getLineHeight()/2-INTERVALL_WIDTH/10, 0));
		
		BitmapText pLabel = new BitmapText(guiFont);
		pLabel.setText("PerR");
		pLabel.setColor(ColorRGBA.White);
		pLabel.setLocalTranslation(2*INTERVALL_WIDTH*behaviorCount/4 - pLabel.getLineWidth()/2 , HEIGHT+pLabel.getLineHeight(), 0);
		chartNode.attachChild(pLabel);
		
		Quad p_quad = new Quad(INTERVALL_WIDTH/5, INTERVALL_WIDTH/5);
		Geometry p_geo = new Geometry("p_quad", p_quad);
		putShape(chartNode, p_geo, ColorRGBA.Yellow, new Vector3f(
				2*INTERVALL_WIDTH*behaviorCount/4 - pLabel.getLineWidth()/2 - INTERVALL_WIDTH/5, 
				HEIGHT+pLabel.getLineHeight()/2-INTERVALL_WIDTH/10, 0));
		
		BitmapText sLabel = new BitmapText(guiFont);
		sLabel.setText("SocR");
		sLabel.setColor(ColorRGBA.White);
		sLabel.setLocalTranslation(3*INTERVALL_WIDTH*behaviorCount/4 -label.getLineWidth()/2 , HEIGHT+label.getLineHeight(), 0);
		chartNode.attachChild(sLabel);
		
		Quad s_quad = new Quad(INTERVALL_WIDTH/5, INTERVALL_WIDTH/5);
		Geometry s_geo = new Geometry("s_quad", s_quad);
		putShape(chartNode, s_geo, ColorRGBA.Orange, new Vector3f(
				3*INTERVALL_WIDTH*behaviorCount/4 - sLabel.getLineWidth()/2 - INTERVALL_WIDTH/5, 
				HEIGHT+sLabel.getLineHeight()/2-INTERVALL_WIDTH/10, 0));
		
	}
	
	private Geometry putShape(Node parent, Geometry g, ColorRGBA color, Vector3f coords) {
		chartNode.detachChildNamed(g.getName());
		g.setLocalTranslation(coords);
		Material mat = new Material(assetMgr,
				"Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", color);
		g.setMaterial(mat);
		parent.attachChild(g);
		return g;
	}
	
	public void setVal(int index, float e, float p, float s) {
		effort[index] = e;
		personal[index] = p;
		social[index] = s;
	}
	
	public void setBars() {
		Quad quad;
		for (int i = 0; i < behaviorCount*3; i+=3) {
			float e = effort[i/3]*HEIGHT;
			float p = personal[i/3]*HEIGHT;
			float s = social[i/3]*HEIGHT;
			
			quad = new Quad(INTERVALL_WIDTH - 2, e);
			Geometry geo = new Geometry("bar" + i, quad);
			putShape(chartNode, geo, ColorRGBA.White, new Vector3f(
					(i/3) * INTERVALL_WIDTH + 1, 0, 0));
			
			quad = new Quad(INTERVALL_WIDTH - 2, p);
			geo = new Geometry("bar" + (i+1), quad);
			putShape(chartNode, geo, ColorRGBA.Yellow, new Vector3f(
					(i/3) * INTERVALL_WIDTH + 1, e, 0));
			
			quad = new Quad(INTERVALL_WIDTH - 2, s);
			geo = new Geometry("bar" + (i+2), quad);
			putShape(chartNode, geo, ColorRGBA.Orange, new Vector3f(
					(i/3) * INTERVALL_WIDTH + 1, p+e, 0));
		}
	}

	public void setLabelValues() {
		for (int i = 0; i <= behaviorCount-1; i++) {
			BitmapText label = new BitmapText(guiFont, false);
			
			String b = "b_"+i;
			chartNode.detachChildNamed(b);
			label.setName(b);
			label.setText(b);
			label.setLocalTranslation(INTERVALL_WIDTH/2 + i*INTERVALL_WIDTH - label.getLineWidth()/2, -1, 0);
			chartNode.attachChild(label);
		}
	}

	public Node getChartNode() {
		return chartNode;
	}
}

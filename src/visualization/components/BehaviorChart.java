/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.util.Vector;

import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Quad;
import com.jme3.util.BufferUtils;

public class BehaviorChart extends Geometry {

	private float HEIGHT;
	private int INTERVALL_WIDTH;
	private static final int DEFAULT_LINE_WIDTH = 3;
	private static final int SCALE = 8;
	private static final int GRID_STEP = 10*SCALE;
	private static final int behaviorCount = 10;
	
	private float[] distribution = new float[] { -12f, 3f, 11f, 5f, 45f, 15f, 32f, 1f, -7f, 7f };
	private int recIndex = 0;
	
	private AssetManager assetMgr;
	private Geometry[] bars;
	private BitmapFont guiFont;
	
	private Node chartNode;
	private Line xAxis;
	
	public BehaviorChart(Node parent, int width, int height) {
		assetMgr = (AssetManager) PresentationVars.instance().get(
				PresentationVars.ASSET_MGR);

		HEIGHT = 3 * height / 4;
		INTERVALL_WIDTH = (3* width/4) /behaviorCount;
		
		guiFont = assetMgr.loadFont("Interface/Fonts/Default.fnt");
		chartNode = new Node();
		parent.attachChild(chartNode);

		// add x-axis
		xAxis = new Line(new Vector3f(0, HEIGHT/2, 0), new Vector3f(behaviorCount
				* INTERVALL_WIDTH +5, HEIGHT/2, 0));
		xAxis.setLineWidth(DEFAULT_LINE_WIDTH);
		putShape(chartNode, new Geometry("x-axis", xAxis), ColorRGBA.White,
				new Vector3f(-1, 0, 0));
		
		for (int i = 0; i <= behaviorCount; i++) {
			Line line = new Line(new Vector3f(i * INTERVALL_WIDTH, HEIGHT/2-1, 0),
					new Vector3f(i * INTERVALL_WIDTH, HEIGHT/2+1, 0));
			putShape(chartNode, new Geometry("line" + i, line),
					ColorRGBA.White, new Vector3f());
		}

		Line yAxis = new Line(new Vector3f(0, 0, 0), new Vector3f(0, HEIGHT, 0));
		yAxis.setLineWidth(DEFAULT_LINE_WIDTH);
		putShape(chartNode, new Geometry("y-axis", yAxis), ColorRGBA.White,
				new Vector3f(-1, 0, 0));
		
		Triangle t = new Triangle();
		Geometry g = new Geometry("TriangleY-1", t);
		g.setLocalScale(6f, 4, 4);
		g.rotate(0, 0, (float) (Math.PI / 2));
		putShape(chartNode, g, ColorRGBA.White, new Vector3f(-1, HEIGHT - 2, 0));

		t = new Triangle();
		g = new Geometry("TriangleY-2", t);
		g.setLocalScale(6f, 4, 4);
		g.rotate(0, 0, -(float) (Math.PI / 2));
		putShape(chartNode, g, ColorRGBA.White, new Vector3f(-1, 0, 0));
		
		// add labels
		BitmapText label;
		label = new BitmapText(guiFont);
		label.setText("credits");
		label.setColor(ColorRGBA.White);
		label.setLocalTranslation(-label.getLineWidth()/2, HEIGHT+label.getLineHeight()+20, 0);
		chartNode.attachChild(label);
				
		// draw grid
		Line grid;
		for (int i = 0; i <= HEIGHT; i += GRID_STEP) {
			grid = new Line(new Vector3f(0, i, 0), new Vector3f(behaviorCount
					* INTERVALL_WIDTH, i, 0));
			grid.setLineWidth(1);
			putShape(chartNode, new Geometry("grid" + i, grid), ColorRGBA.Gray,
					new Vector3f());
		
			// add label
			label = new BitmapText(guiFont);
			label.setText(""+(int)(i-HEIGHT/2)/SCALE);
			label.setColor(ColorRGBA.White);
			label.setLocalTranslation(-label.getLineWidth() - INTERVALL_WIDTH
					/ 4, i + label.getLineHeight() / 2, 0);
			chartNode.attachChild(label);
		}

		
		setBars();
		setLabelValues();
		
		label = new BitmapText(guiFont);
		label.setText("behaviors");
		label.setColor(ColorRGBA.White);
		label.setLocalTranslation(3* width/8 -label.getLineWidth()/2, HEIGHT+label.getLineHeight()+20, 0);
		chartNode.attachChild(label);
	}

	public void setBars() {
		bars = new Geometry[behaviorCount];
		Quad quad;
		for (int i = 0; i < bars.length; i++) {
			quad = new Quad(INTERVALL_WIDTH - 2, distribution[i]*SCALE);
			bars[i] = new Geometry("bar" + i, quad);
			putShape(chartNode, bars[i], ColorRGBA.Yellow, new Vector3f(
					i * INTERVALL_WIDTH + 1, HEIGHT/2, 0));
		}
	}

	public void setLabelValues() {
		for (int i = 0; i <= behaviorCount-1; i++) {
			BitmapText label = new BitmapText(guiFont, false);
			
			String b = "b_"+i+":";
			chartNode.detachChildNamed(b);
			label.setName(b);
			label.setText(b);
			label.setLocalTranslation(INTERVALL_WIDTH/2 + i * INTERVALL_WIDTH - label.getLineWidth()/2, -2, 0);
			chartNode.attachChild(label);
			
			label = new BitmapText(guiFont, false);
			chartNode.detachChildNamed(b+"_"+i);
			label.setName(b+"_"+i);
			label.setSize(assetMgr.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize()/1.2f);
			label.setText(String.format("%.2f", distribution[i]));
			label.setLocalTranslation(INTERVALL_WIDTH/2 + i * INTERVALL_WIDTH - label.getLineWidth()/2, -5-label.getLineHeight(), 0);
			chartNode.attachChild(label);
		}
	}

	private Geometry putShape(Node parent, Geometry g, ColorRGBA color,
			Vector3f coords) {
		chartNode.detachChildNamed(g.getName());
		g.setLocalTranslation(coords);
		Material mat = new Material(assetMgr,
				"Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", color);
		g.setMaterial(mat);
		parent.attachChild(g);
		return g;
	}

	public void updateBehaviors(float[] newBehaviors) {
		float max = 0;
		recIndex = -1;
		for(int i = 0; i< behaviorCount; i++) {
			distribution[i] = newBehaviors[i];
			if(distribution[i]>max) {
				max = distribution[i];
				recIndex = i;
			}
		}
	}
	
	private class Triangle extends Mesh {
		private Vector3f[] vertices;
		private int[] indexes;

		public Triangle() {
			vertices = new Vector3f[3];
			vertices[0] = new Vector3f(3, 0, 0);
			vertices[1] = new Vector3f(0, 1, 0);
			vertices[2] = new Vector3f(0, -1, 0);
			indexes = new int[] { 2, 0, 1 };
			setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
			setBuffer(Type.Index, 1, BufferUtils.createIntBuffer(indexes));
			updateBound();
		}
	}

	public Geometry[] getBars() {
		return bars;
	}
	
	public Node getChartNode() {
		return chartNode;
	}

	public Vector<String> getRecommendation() {
		Vector<String> rec = new Vector<String>();
		if(recIndex == -1) {
			rec.add("computing");rec.add("computing");
		}
		else {	
			rec.add("b_"+recIndex);
			rec.add(String.format("%.2f",distribution[recIndex]));
		}
		return rec;
	}
}

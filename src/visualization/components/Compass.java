/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;
import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.debug.Arrow;
import com.jme3.scene.shape.Sphere;
import com.jme3.scene.shape.Torus;

public class Compass extends Geometry {
    private Node pivot, compass, needleRotate, labelRotate;
    private Torus ringTorus;
    private Sphere center;
    private Arrow needle;
    private float prior = 0;
    private float priorLabelRotate = 0;
    
    public Compass(Node parent) {
    	AssetManager _assetMgr = (AssetManager) PresentationVars.instance().get(PresentationVars.ASSET_MGR);
    	
    	pivot = new Node("Pivot");
        parent.attachChild(pivot); 
        
        compass = new Node("Compass");
        needleRotate = new Node("NeedleRotation");
        labelRotate = new Node("LabelRotation");
        
        Quaternion pitch_pi_3 = new Quaternion();
//        pitch_pi_3.fromAngleAxis(-1.7f*FastMath.PI/5, new Vector3f(1,0,0));
        pitch_pi_3.fromAngleAxis(0, new Vector3f(1,0,0));
        
        BitmapFont guiFont = _assetMgr.loadFont("Interface/Fonts/Default.fnt");
        setupCarDir(guiFont);
        
        ringTorus = new Torus(48, 2, 1f, 120.f);
        Geometry torusGeo = new Geometry("Compass Ring", ringTorus);
        Material torusMat = new Material(_assetMgr, "Common/MatDefs/Misc/Unshaded.j3md");
        torusMat.setColor("Color", ColorRGBA.White);
        torusGeo.setMaterial(torusMat);
        
        Torus hullRing = new Torus(48, 2, 1f, 90f);
        Geometry hullRingGeo = new Geometry("Hull Ring", hullRing);
        Material hullRingMat = new Material(_assetMgr, "Common/MatDefs/Misc/Unshaded.j3md");
        hullRingMat.setColor("Color", ColorRGBA.White);
        hullRingGeo.setMaterial(hullRingMat);
               
        center = new Sphere (48, 2, 3.0f);
        Geometry sphereGeo = new Geometry("Compass Center", center);
        Material sphereMat = new Material(_assetMgr, "Common/MatDefs/Misc/Unshaded.j3md");
        sphereMat.setColor("Color", ColorRGBA.White);
        sphereGeo.setMaterial(sphereMat);
        
        needle = new Arrow(new Vector3f(0.0f, 89f, 0.0f));
        needle.setLineWidth(3);
        Geometry needleGeo = new Geometry("Compass Needle", needle);
        Material needleMat = new Material(_assetMgr, "Common/MatDefs/Misc/Unshaded.j3md");
        needleMat.setColor("Color", ColorRGBA.Green);
        needleGeo.setMaterial(needleMat);
        needleRotate.attachChild(needleGeo);
        
        compass.attachChild(hullRingGeo);
        compass.attachChild(sphereGeo);
        compass.attachChild(torusGeo);
        compass.attachChild(needleRotate);
        compass.attachChild(labelRotate);
        
        pivot.attachChild(compass);
        pivot.rotate(pitch_pi_3);
        pivot.setLocalTranslation(new Vector3f(150f, 125f, 0));
    }
    
    public void updateInfo(float steeringDirection, float orientation) {
    	if(steeringDirection != 0.0) {
    		needleRotate.rotate(new Quaternion().fromAngleAxis(FastMath.PI * prior / 180, new Vector3f(0,0,1)));
    		needleRotate.rotate(new Quaternion().fromAngleAxis(-FastMath.PI * steeringDirection / 180, new Vector3f(0,0,1)));
    		prior = steeringDirection;
    	}
    	if(orientation != 0.0) {
    		labelRotate.rotate(new Quaternion().fromAngleAxis(-FastMath.PI * priorLabelRotate / 180, new Vector3f(0,0,1)));
    		labelRotate.rotate(new Quaternion().fromAngleAxis(FastMath.PI * orientation / 180, new Vector3f(0,0,1)));
    		priorLabelRotate = orientation;
    	}
    }
    
//    ------------------------------------------------------------------------------------------------
    
    private void setupCarDir(BitmapFont guiFont) {
    	
        BitmapText west = new BitmapText(guiFont, false);
        west.setSize(guiFont.getCharSet().getRenderedSize()*1.5f);
        west.setColor(ColorRGBA.Green);      
        west.setText("W"); // crosshairs
        west.setLocalTranslation(-105f - west.getLineWidth() / 2, west.getLineHeight() / 2, 0);

        labelRotate.attachChild(west);
        
        BitmapText east = new BitmapText(guiFont, false);
        east.setSize(guiFont.getCharSet().getRenderedSize()*1.5f);
        east.setColor(ColorRGBA.Green);   
        east.setText("E"); // crosshairs
        east.setLocalTranslation(105f - east.getLineWidth() / 2, east.getLineHeight() / 2, 0);
        labelRotate.attachChild(east);
        
        BitmapText south = new BitmapText(guiFont, false);
        south.setSize(guiFont.getCharSet().getRenderedSize()*1.5f);
        south.setColor(ColorRGBA.Green);   
        south.setText("S"); // crosshairs
        south.setLocalTranslation(-south.getLineWidth() / 2, -105f + south.getLineHeight() / 2, 0);
        labelRotate.attachChild(south);

        BitmapText north = new BitmapText(guiFont, false);
        north.setSize(guiFont.getCharSet().getRenderedSize()*1.5f);
        north.setColor(ColorRGBA.Green);   
        north.setText("N"); // crosshairs
        north.setLocalTranslation(-north.getLineWidth() / 2, 105f + north.getLineHeight() / 2, 0);
        labelRotate.attachChild(north);
        
//   -------------------------------------------------------------------------------------------
        
        BitmapText nw = new BitmapText(guiFont, false);
        nw.setSize(guiFont.getCharSet().getRenderedSize()*1.1f);
        nw.setColor(ColorRGBA.Green);   
        nw.setText("NW"); // crosshairs
        nw.setLocalTranslation(-75f - nw.getLineWidth() / 2, 75f + nw.getLineHeight() / 2, 0);
        labelRotate.attachChild(nw);
        
        BitmapText ne = new BitmapText(guiFont, false);
        ne.setSize(guiFont.getCharSet().getRenderedSize()*1.1f);
        ne.setColor(ColorRGBA.Green);   
        ne.setText("NE"); // crosshairs
        ne.setLocalTranslation(75f - ne.getLineWidth() / 2, 75f + ne.getLineHeight() / 2, 0);
        labelRotate.attachChild(ne);
        
        BitmapText sw = new BitmapText(guiFont, false);
        sw.setSize(guiFont.getCharSet().getRenderedSize()*1.1f);
        sw.setColor(ColorRGBA.Green);   
        sw.setText("SW"); // crosshairs
        sw.setLocalTranslation(-75f - sw.getLineWidth() / 2, -75f + sw.getLineHeight() / 2, 0);
        labelRotate.attachChild(sw);
        
        BitmapText se = new BitmapText(guiFont, false);
        se.setSize(guiFont.getCharSet().getRenderedSize()*1.1f);
        se.setColor(ColorRGBA.Green);   
        se.setText("SE"); // crosshairs
        se.setLocalTranslation(75f - se.getLineWidth() / 2, -75f + se.getLineHeight() / 2, 0);
        labelRotate.attachChild(se);
    }
}
/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Quad;

public class ZoomingImage extends Geometry {
	private AssetManager assetMgr;
	
	public ZoomingImage(Node parent, int width, int height) {
		assetMgr = (AssetManager) PresentationVars.instance().get(
				PresentationVars.ASSET_MGR);
		
		Quad zoomQuad = new Quad(2*width, height);
		Geometry zoomGeom = new Geometry("zoomGeom", zoomQuad);
		Material zoomMat = new Material(assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
		zoomMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		zoomMat.setTexture("ColorMap", assetMgr.loadTexture("Textures/gui/img3.png"));
		zoomGeom.setMaterial(zoomMat);
		parent.attachChild(zoomGeom);
	}
}

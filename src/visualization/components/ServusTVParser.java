/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.imageio.ImageIO;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

public class ServusTVParser {
	
	public class ContentObject {
		private String text;
		private Texture tex;
		private String videoURL;
		private String channel;
		
		public ContentObject(String text, Texture tex, String videoURL, String channel) {
			this.text = text;
			this.tex = tex;
			this.videoURL = videoURL;
			this.channel = channel;
		}
		
		public void setVideoURL(String videoURL) {
			this.videoURL = videoURL;
		}
		
		public String getVideoURL() {
			return videoURL;
		}
		
		public void setChannel(String channel) {
			this.channel = channel;
		}
		
		public String getChannel() {
			return channel;
		}
		
		public void setText(String text) {
			this.text = text;
		}
		
		public String getText() {
			return text;
		}

		public void setTexture(Texture tex) {
			this.tex = tex;
		}
		
		public Texture getTexture() {
			return tex;
		}
	}
	
	class GetContentTask implements Callable<Boolean> {
		private String channel;
		private int index;
		
		public GetContentTask (String channel, int index) {
			this.channel = channel;
			this.index = index;
		}
		
	    @Override
	    public Boolean call() {
	    	Vector<Node> channelNodes = nextChannelContent.get(channel);
			
	    	if(channelNodes == null || channelNodes.isEmpty()) {
	    		System.out.println("Channel: "+channel+" empty;");
	    		return false;
	    	}
			
	    	if(index == -1) index = (int)(Math.floor(Math.random()*(channelNodes.size()-0.01)));
	    	
	    	if(index > channelNodes.size()) { 
	    		System.out.println("Requested node not found");
	    		return false;
	    	}
	    	
	    	Node nextContentEntry = channelNodes.elementAt(index);
	    	
			String contentText = "";
			URL contentImage = null;
			String videoURL = "";
			
			NodeList contentElems = nextContentEntry.getChildNodes();
			for(int i = 0; i< contentElems.getLength(); i++) {
				Node elem = contentElems.item(i);
				
				if(elem.getLocalName().equals("copytext")) {
					contentText = elem.getChildNodes().item(0).getNodeValue();
				}
				else if(elem.getLocalName().equals("images")) {
					try {
						contentImage = new URL(elem.getChildNodes().item(0).getAttributes().getNamedItem("url").getNodeValue());
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (DOMException e) {
						e.printStackTrace();
					}
				}
				else if(elem.getLocalName().equals("videos")) {
					try {
						if(elem.getChildNodes().getLength()>1)
							videoURL = new URL(elem.getChildNodes().item(1).getAttributes().getNamedItem("url").getNodeValue()).toString();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (DOMException e) {
						e.printStackTrace();
					}
				}
			}
			
			Texture2D tex;
			int attempts = 10;
			boolean done = false;
			while(!done) {
				loader = new AWTLoader();
				try {
					done = true;
					if(contentImage == null) System.out.println("Image == null");
					BufferedImage img = ImageIO.read(contentImage);
					tex = new Texture2D();
					tex.setImage(loader.load(img, true));
					
					Vector<ContentObject> prior = contentObjects.get(channel);
					if(prior == null) prior = new Vector<ContentObject>();
					prior.add(new ContentObject(contentText, tex, videoURL, channel));
					contentObjects.put(channel, prior);
				} catch (IOException e) {
					System.out.println("Failed at Texture Creation");
					done = false;
					
					attempts--;
					if(attempts == 0) return false;
				}
	    	}
			return true;
	    }
	}
	
	private Document document;
	private Map<String, Vector<Node>> nextChannelContent;
	private Map<String, Vector<ContentObject>> contentObjects;
	private AWTLoader loader;
	private ExecutorService executor = Executors.newCachedThreadPool();
	
	public ServusTVParser() {
		boolean documentParsed = false;
		loader = new AWTLoader();
		nextChannelContent = new HashMap<String, Vector<Node>>();
		contentObjects = new HashMap<String, Vector<ContentObject>>();
		
		Tidy tidy = new Tidy();
		tidy.setXmlTags(true);
		tidy.setAsciiChars(false);
		tidy.setInputEncoding("UTF-8");
		tidy.setOutputEncoding("UTF-8");
		tidy.setWraplen(Integer.MAX_VALUE);
		tidy.setXmlOut(true);
		
		document = null;
		URL tmp_url;
		while(!documentParsed) {
			try {
				tmp_url = new URL("http://ios.servustv.com/testflight-vod2vju-xml");
				BufferedReader in = new BufferedReader(new InputStreamReader(tmp_url.openConnection().getInputStream(), "UTF-8"));
				document = tidy.parseDOM(in, null);
				document.setXmlVersion("1.0");
				
				NodeList nl = document.getElementsByTagName("channel");
				for(int i = 0; i<nl.getLength(); i++) {
					NamedNodeMap attr = nl.item(i).getAttributes();
					for(int j = 0; j<attr.getLength(); j++) {
						Vector<Node> entries = getEntries(nl.item(i));
						
						nextChannelContent.put(attr.item(j).getNodeValue(), entries);
					}
				}
				
				documentParsed = true;
			} catch (MalformedURLException e) {
//				e.printStackTrace();
			} catch (IOException e) {
//				e.printStackTrace();
			}
		}
	}
	
	private Vector<Node> getEntries(Node channelNode) {
		Vector<Node> entries = new Vector<Node>();
	
		NodeList formats = channelNode.getChildNodes();
		for(int i = 0; i<formats.getLength(); i++) {
			NodeList tmp_entries = formats.item(i).getChildNodes();
			for(int j = 0; j<tmp_entries.getLength(); j++) {
				entries.add(tmp_entries.item(j));
			}
		}
		
		return entries;
	}
	
	public Vector<String> getChannels() {
		return new Vector<String>(nextChannelContent.keySet());
	}
	
	public boolean getNextContentTexture(String channel, int index) {
		boolean validChannel = false;
		for(String str:nextChannelContent.keySet())
			if(str.equals(channel)) validChannel = true;
			
		if(validChannel) {
//			System.out.println("Channel: "+ channel +"; Index: "+index);
			try {
				Future<Boolean> future = executor.submit(new GetContentTask(channel, index));
				return future.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
				return false;
			} catch (ExecutionException e) {
				e.printStackTrace();
				return false;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		else return false;
	}

	public ContentObject getContentObject(String channel) {
		Vector<ContentObject> objs =  contentObjects.get(channel);
		if(objs == null || objs.isEmpty()) return null;
		else {
			ContentObject o = objs.remove(0);
			return o;
		}
	}
	
	public boolean clean() {
		executor.shutdownNow();
		contentObjects = null;
		document = null;
		nextChannelContent = null;
		loader = null;
		return true;
	}
	
	public int channelSize(String channel) {
		Vector<Node> channelNodes = nextChannelContent.get(channel);
		if(channelNodes != null) return channelNodes.size();
		else return -1;
	}
}

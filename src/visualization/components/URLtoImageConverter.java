/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.awt.Dimension;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.swing.Java2DRenderer;

import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;

public class URLtoImageConverter extends Thread {
	private Tidy tidy = new Tidy();
	private volatile Vector<URLRequest> requests = new Vector<URLRequest>();
	private boolean run = true;
	private Map<Key,Object> renderingHints = new HashMap<Key,Object>();
	private BufferedImage renderedImage;
	private Document document;
	private Dimension renderDim;
	private URLRequest req;
	private AWTLoader loader = new AWTLoader();
	
	public URLtoImageConverter() {		
		tidy.setXHTML(true);
		tidy.setDocType("strict");
		tidy.setInputEncoding("UTF-8");
	    tidy.setWraplen(Integer.MAX_VALUE);
	    tidy.setSmartIndent(true);
		tidy.setMakeClean(true);
		tidy.setShowWarnings(false);
		tidy.setPrintBodyOnly(true);
		tidy.setQuiet(true);
		tidy.setDropEmptyParas(true);
		tidy.setMakeBare(true);
         
		renderingHints.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		renderingHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		renderingHints.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
		renderingHints.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
	}
	
	public void run() {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		while(run) {
			try {
				Future<Boolean> future = executor.submit(new RenderImageTask());
				future.get(15, TimeUnit.SECONDS);
			} catch (TimeoutException e) {
				document = null;
				renderedImage = null;
		        System.out.println("Time");
		        executor.shutdownNow();
		        executor = Executors.newSingleThreadExecutor();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		executor.shutdownNow();
	}

	class RenderImageTask implements Callable<Boolean> {
				
	    @Override
	    public Boolean call() {
	    	if(requests.size() > 0) {
	    		req = requests.remove(0);
				renderDim = req.getDim();
				document = null;
				URL tmp_url;
				try {
					tmp_url = new URL(req.getPath());
					BufferedReader in = new BufferedReader(new InputStreamReader(tmp_url.openConnection().getInputStream()));
					document = tidy.parseDOM(in, null);
					in.close();
					
					renderDim.width = renderDim.width/2;
					
					if(req.getType().equals("weather") || req.getType().equals("local news")) {
							
						document.removeChild(document.getFirstChild());
						NodeList nl = document.getChildNodes();
						for (int i = 0; i < nl.getLength(); i++) {
							removeWeatherNodes(nl.item(i));
						}
						
						renderDim.height = renderDim.height/2;
					}
						
					Java2DRenderer renderer = new Java2DRenderer(document, renderDim.width, 2*renderDim.height/3);
					renderer.setBufferedImageType(BufferedImage.SCALE_SMOOTH);
					renderer.setRenderingHints(renderingHints);
					renderedImage = renderer.getImage();
						
					Texture tex = awtImageToTexture();
					Material customerDataMat = ((Geometry)req.getGeom()).getMaterial();
					customerDataMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
					customerDataMat.setTexture("ColorMap", tex);
					
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}				
	    	}
			return true;
	    }
	}
	
	private boolean removeWeatherNodes(Node rootNode) {
		if(rootNode.getNodeName().equals("ul") || 
				rootNode.getNodeValue().equals("Share this") || 
				rootNode.getNodeValue().equals("Top") || 
				rootNode.getNodeValue().equals("Menu") || 
				rootNode.getNodeValue().equals("Related BBC sites"))
			return true;
		
		NodeList nl = rootNode.getChildNodes();
	    for (int i = 0; i < nl.getLength(); i++) {
	    	if(removeWeatherNodes(nl.item(i)))
    			rootNode.removeChild(nl.item(i));
	    }
		return false;
	}
	
	private Texture awtImageToTexture() {
	  Texture tex = new Texture2D();
	 
	  tex.setImage(loader.load(renderedImage, true));
	  return tex;
	}
	
	public void addUrl(String path, Dimension dimension, Spatial geom, String type) {
		if(requests.size()>10) return;
		requests.add(new URLRequest(path, dimension, geom, type));
	}
	
	public void setRun(boolean run) {
		this.run = run;
	}
}

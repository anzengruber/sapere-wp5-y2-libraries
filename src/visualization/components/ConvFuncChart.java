/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.util.Date;
import java.util.Vector;

import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Spline;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.scene.shape.Curve;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Sphere;
import com.jme3.util.BufferUtils;

public class ConvFuncChart extends Geometry {
	private float HEIGHT;
	private int INTERVAL_WIDTH;
	private float TIME_INTERVAL;
	private static final int DEFAULT_LINE_WIDTH = 3;
	private static final int GRID_STEP = 40;
	private static final int timeSlots = 9;

	private AssetManager assetManager;
	private Node chartNode;
	
	private Vector<Float> past = new Vector<Float>();
	
	private Geometry currentUtility;
	
	private Spline spline;
	private Sphere sphere;
	private Line xAxis;
	
	public ConvFuncChart(Node parent, int width, int height, long timeInterval) {
		
		assetManager = (AssetManager) PresentationVars.instance().get(
				PresentationVars.ASSET_MGR);
		
		past.add(32f);past.add(30f);past.add(26f);past.add(29f);
		past.add(35f);past.add(37f);past.add(39f);past.add(35f);
		past.add(31f);
		
		HEIGHT = 3 * height / 4;
		INTERVAL_WIDTH = (3* width/4) /timeSlots;
		
		float interval = (float)timeInterval/60000f;
		if(interval > 60) TIME_INTERVAL = 60;
		else TIME_INTERVAL = interval;
		
		BitmapFont guiFont = assetManager
				.loadFont("Interface/Fonts/Default.fnt");
		chartNode = new Node();
		parent.attachChild(chartNode);

		// add x-axis
		xAxis = new Line(new Vector3f(0, 0, 0), new Vector3f(timeSlots
				* INTERVAL_WIDTH + 5, 0, 0));
		xAxis.setLineWidth(DEFAULT_LINE_WIDTH);
		putShape(chartNode, new Geometry("x-axis", xAxis), ColorRGBA.White,
				new Vector3f(-1, 0, 0));
		Triangle t = new Triangle(); // add arrowhead to x-axis
		Geometry g = new Geometry("triangleX", t);
		g.setLocalScale(6f, 4, 4);
		putShape(chartNode, g, ColorRGBA.White, new Vector3f(timeSlots
				* INTERVAL_WIDTH + 3, 0, 0));
		chartNode.attachChild(g);
		
		for (int i = 0; i <= timeSlots; i++) { // add tick marks
			Line line = new Line(new Vector3f(i * INTERVAL_WIDTH, -1, 0),
					new Vector3f(i * INTERVAL_WIDTH, 1, 0));
			putShape(chartNode, new Geometry("line" + i, line),
					ColorRGBA.White, new Vector3f());
		}

		// add y-axis
		Line yAxis = new Line(new Vector3f(0, 0, 0), new Vector3f(0, HEIGHT, 0));
		yAxis.setLineWidth(DEFAULT_LINE_WIDTH);
		putShape(chartNode, new Geometry("y-axis", yAxis), ColorRGBA.White,
				new Vector3f(-1, 0, 0));
		t = new Triangle(); // add arrowhead to y-axis
		g = new Geometry("triangleY", t);
		g.setLocalScale(6f, 4, 4);
		g.rotate(0, 0, (float) (Math.PI / 2));
		putShape(chartNode, g, ColorRGBA.White, new Vector3f(-1, HEIGHT - 2, 0));
		chartNode.attachChild(g);

		// draw grid
		Line grid;
		// add labels
		BitmapText label;
		label = new BitmapText(guiFont);
		label.setText("credits");
		label.setColor(ColorRGBA.White);
		label.setLocalTranslation(-label.getLineWidth()/2, HEIGHT+label.getLineHeight()+20, 0);
		chartNode.attachChild(label);
				
		for (int i = GRID_STEP; i < HEIGHT; i += GRID_STEP) {
			grid = new Line(new Vector3f(0, i, 0), new Vector3f(timeSlots
					* INTERVAL_WIDTH, i, 0));
			grid.setLineWidth(1);
			putShape(chartNode, new Geometry("grid" + i, grid), ColorRGBA.Gray,
					new Vector3f());
			// add label
			label = new BitmapText(guiFont);
			label.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize()/1.2f);
			label.setText(String.valueOf(i/4));
			label.setColor(ColorRGBA.White);
			label.setLocalTranslation(-label.getLineWidth() - INTERVAL_WIDTH
					/ 4, i + label.getLineHeight() / 2, 0);
			chartNode.attachChild(label);
		}

		setSpline();
		setTimes();
		
		label = new BitmapText(guiFont);
		label.setText("time");
		label.setColor(ColorRGBA.White);
		label.setLocalTranslation(3* width/8 -label.getLineWidth()/2 , -5-label.getLineHeight(), 0);
		chartNode.attachChild(label);
		
		// add time line
		Line line = new Line(new Vector3f((timeSlots-1) * INTERVAL_WIDTH, HEIGHT+1, 0),
				new Vector3f((timeSlots-1) * INTERVAL_WIDTH, -1, 0));
		line.setLineWidth(1);
		currentUtility = new Geometry("utilityLine", line);
		putShape(chartNode, currentUtility, ColorRGBA.Green, new Vector3f());
				
		PresentationVars.instance();
	}

	public void setTimes() {
		Date d = new Date(System.currentTimeMillis());
		@SuppressWarnings("deprecation")
		int min = d.getMinutes();
		@SuppressWarnings("deprecation")
		int hour = d.getHours();
		
		int minsBackward = (int)TIME_INTERVAL*(timeSlots-1);
		min = min-(minsBackward%60);
		if(min<0) min += 60;
		hour -= minsBackward/60;
		
		for (int i = 0; i <= timeSlots; i++) {
			chartNode.detachChildNamed("TimeLabel"+i);
			BitmapText label = new BitmapText(
					assetManager.loadFont("Interface/Fonts/Default.fnt"), false);
			label.setSize(assetManager.loadFont("Interface/Fonts/Default.fnt").getCharSet().getRenderedSize()/1.2f);
			label.setName("TimeLabel"+i);
			
			String mins = ""+min;
			String hours = ""+hour;
			if(mins.length()==1) mins = "0"+mins;
			if(hours.length()==1) hours = "0"+hours;
			
			label.setText(hours + ":" + mins);
			
			label.setLocalTranslation(i * INTERVAL_WIDTH - label.getLineWidth()/2, -2, 0);
			chartNode.attachChild(label);
		
			min += TIME_INTERVAL;
			if(min >= 60) {
				min = min%60;
				hour++;
			}
		}
	}

	public void setSpline() {
		// add graph		
		spline = new Spline();
		
		for (int i = 0; i < timeSlots; i++) {
			spline.addControlPoint(new Vector3f(i * INTERVAL_WIDTH, past.elementAt(i)*4, 0));
		}
		
		for (int i = 0; i < timeSlots; i++) { // add visual data points
			sphere = new Sphere(100, 100, 4f);
			chartNode.detachChildNamed("sphere" + i);
			Geometry g = new Geometry("sphere" + i, sphere);
			putShape(chartNode, g, ColorRGBA.White, new Vector3f(i
					* INTERVAL_WIDTH, past.elementAt(i)*4, 0));
		}
		spline.setCurveTension(0.6f);
		Curve curve = new Curve(spline, timeSlots);
		curve.setLineWidth(DEFAULT_LINE_WIDTH);
		chartNode.detachChildNamed("curve");
		Geometry circle = new Geometry("curve", curve);
		putShape(chartNode, circle, ColorRGBA.Yellow, new Vector3f());
	}

	private Geometry putShape(Node n, Geometry g, ColorRGBA color,
			Vector3f coords) {
		g.setLocalTranslation(coords);
		Material mat = new Material(assetManager,
				"Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", color);
		g.setMaterial(mat);
		n.attachChild(g);
		return g;
	}

	private class Triangle extends Mesh {
		private Vector3f[] vertices;
		private int[] indexes;

		public Triangle() {
			vertices = new Vector3f[3];
			vertices[0] = new Vector3f(3, 0, 0);
			vertices[1] = new Vector3f(0, 1, 0);
			vertices[2] = new Vector3f(0, -1, 0);
			indexes = new int[] { 2, 0, 1 };
			setBuffer(Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
			setBuffer(Type.Index, 1, BufferUtils.createIntBuffer(indexes));
			updateBound();
		}
	}
	
	public void update(float val) {
		past.add(val);
		past.remove(0);		
	}
	
	public float getConversion() {
		return past.lastElement();
	}

	public Node getChartNode() {
		return chartNode;
	}
}
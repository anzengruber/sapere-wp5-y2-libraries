/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;

public class Border extends Node {
	private Material m;
	public Border(float x, float y, float width, float height, Material m, int lw) {
		this.m = m;
		
		Vector3f p1 = new Vector3f(x, y, 1);
		Vector3f p1_lw = new Vector3f(x-lw/2, y, 1);
		Vector3f p2 = new Vector3f(x, y+height, 1);
		Vector3f p2_lw = new Vector3f(x-lw/2, y+height, 1);
		Vector3f p3 = new Vector3f(x+width, y, 1);
		Vector3f p3_lw = new Vector3f(x+width+lw/2, y, 1);
		Vector3f p4 = new Vector3f(x+width, y+height, 1);
		Vector3f p4_lw = new Vector3f(x+width+lw/2, y+height, 1);
		
		Line l1 = new Line(p1, p2);
		l1.setLineWidth(lw);
		attachSpatial(new Geometry("l1", l1));
		
		Line l2 = new Line(p2_lw, p4_lw);
		l2.setLineWidth(lw);
		attachSpatial(new Geometry("l2", l2));
		
		Line l3 = new Line(p4, p3);
		l3.setLineWidth(lw);
		attachSpatial(new Geometry("l3", l3));
		
		Line l4 = new Line(p3_lw, p1_lw);
		l4.setLineWidth(lw);
		attachSpatial(new Geometry("l4", l4));
	}
	
	private void attachSpatial(Geometry g) {
		g.setMaterial(m.clone());
		attachChild(g);
	}
}

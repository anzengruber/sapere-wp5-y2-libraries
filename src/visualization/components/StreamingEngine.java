/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import uk.co.caprica.vlcj.binding.internal.libvlc_log_level_e;
import uk.co.caprica.vlcj.log.NativeLog;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.direct.DirectMediaPlayer;
import uk.co.caprica.vlcj.player.direct.RenderCallbackAdapter;
import uk.co.caprica.vlcj.test.VlcjTest;

public class StreamingEngine extends VlcjTest {

	private final class ImagePane extends JPanel {
		private static final long serialVersionUID = 1L;
		private final BufferedImage image;

	    public ImagePane(BufferedImage image) {
	      this.image = image;
	    }

	    @Override
	    public void paint(Graphics g) {
	      Graphics2D g2 = (Graphics2D)g;
	      g2.drawImage(image, null, 0, 0);
	      // You could draw on top of the image here...
	      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	      g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
	      g2.setComposite(AlphaComposite.SrcOver.derive(0.3f));
	      g2.fillRoundRect(100, 100, 100, 80, 32, 32);
	      g2.setComposite(AlphaComposite.SrcOver);
	    }
	}

	private final class TestRenderCallback extends RenderCallbackAdapter {

	    public TestRenderCallback() {
	      super(new int[width * height]);
	    }

	    @Override
	    public void onDisplay(DirectMediaPlayer p, int[] data) {
	      // The image data could be manipulated here...
	      image.setRGB(0, 0, width, height, data, 0, width);
	      imagePane.repaint();
	    }
	    
	    
	}

	private final class MediaPlayerEvents extends MediaPlayerEventAdapter {

		@Override
		public void finished(MediaPlayer mediaPlayer) {
			running = false;
			super.finished(mediaPlayer);
		}
		
        @Override
        public void error(MediaPlayer mediaPlayer) {
        	mediaPlayer.stop();
        }
	}
	
	private class StreamingThread extends Thread {		
		@SuppressWarnings("deprecation")
		public void run() {
			running = true;
			mediaPlayer = factory.newDirectMediaPlayer(width, height, new TestRenderCallback());
			mediaPlayer.addMediaPlayerEventListener(new MediaPlayerEvents());
			
			if(mediaPlayer.startMedia(fileName))
				;
			else {
				if(mediaPlayer != null) {
					mediaPlayer.stop();
					mediaPlayer.release();
					mediaPlayer = null;
					running = false;
				}
			}
		}
	}
	
	private String fileName;
	private StreamingThread streamingThread;
	
	private final int width = 810;
	private final int height = 480;
	private String[] args = {"--no-video-title-show"};
	
	private BufferedImage image = null;
	private ImagePane imagePane;

	private final MediaPlayerFactory factory;
	private DirectMediaPlayer mediaPlayer;

	private boolean running = false;
	private NativeLog log;
	
	public StreamingEngine() {
		System.setProperty("jna.dump_memory", "false");
		System.setProperty("vlcj.log", "NONE");

		image = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(width, height);
	    image.setAccelerationPriority(1.0f);
	    imagePane = new ImagePane(image);
	    factory = new MediaPlayerFactory(args);
	    
	    log = factory.newLog();
	    if(log != null) log.setLevel(libvlc_log_level_e.ERROR);
	}
	
	public boolean displayVideo(String url) {
		this.fileName = url;
		
		if(!running) {
			streamingThread = new StreamingThread();
			streamingThread.start();
			return true;
		}
		else return false;
	}
	
	public BufferedImage getFrame() {
		if(!running) return null;
		return image;
	}

	public void resume() {
		if(running && mediaPlayer!=null && !mediaPlayer.isPlaying()) mediaPlayer.play();
	}
	public void pause() {
		if(running && mediaPlayer!=null && mediaPlayer.isPlaying()) mediaPlayer.pause();
	}
	public void toggleMute() {
		if(running && mediaPlayer!=null && mediaPlayer.isPlaying()) mediaPlayer.mute(!mediaPlayer.isMute());
	}

	public void stop() {
		if(mediaPlayer!=null) {
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
			running = false;
		}
	}
}

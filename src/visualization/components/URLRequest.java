/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.awt.Dimension;

import com.jme3.scene.Spatial;

public class URLRequest {
	private String path;
	private Dimension dim;
	private Spatial geom;
	private String type;
	
	public URLRequest(String path, Dimension dimension, Spatial geom, String type) {
		this. path = path;
		dim = dimension;
		this.geom = geom;
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public Dimension getDim() {
		return dim;
	}

	public Spatial getGeom() {
		return geom;
	}

	public String getType() {
		return type;
	}
}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.util.HashMap;
import java.util.Map;

import visualization.Node2D;
import visualization.PresentationVars;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.font.LineWrapMode;
import com.jme3.font.Rectangle;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Quad;
import com.jme3.renderer.queue.RenderQueue;

public class ContentChart extends Node2D {
	private AssetManager assetManager;
	private Map<String, Float> textStartTranslations;
	private float elementHeight;
	private float elementWidth;
	private BitmapFont myFont_20;
	private Material mat;
	private ColorRGBA thomasesSkinColor = new ColorRGBA(253f/255f, 222f/255f, 151f/255f, 1f);
	private ColorRGBA azure = new ColorRGBA(240f/255f, 255f/255f, 255f/255f, 1f);
	
	
	public ContentChart(Node2D customerDataPanel, int width, float height) {
		assetManager = (AssetManager) PresentationVars.instance().get(
				PresentationVars.ASSET_MGR);
		
		myFont_20 = assetManager.loadFont("Interface/Fonts/myFont_20.fnt");
		elementWidth = width;
		
		textStartTranslations = new HashMap<String, Float>();
		
		Material borderMat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		borderMat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		borderMat.setColor("Color", ColorRGBA.Black);
		
		mat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");
		mat.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		
		elementHeight =  (3*width/4)*0.563f;
		elementHeight =  height/4;
		
//		------- Init Video Content Image -----------------------------------------------	
		customerDataPanel.attachChild(createQuadGeom(true, true, "videoGeom", new Vector3f(elementWidth/4, 3*elementHeight, 1)));
		
		Border scB = new Border(0, 0, 3*elementWidth/4 - 3, elementHeight - 3, borderMat, 6);
		scB.setLocalTranslation(elementWidth/4, 3*elementHeight, 1);
		customerDataPanel.attachChild(scB);
		
		//------- Init Video Content Text -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(false, true, "videoTGeom", new Vector3f(0, 3*elementHeight, -4)));
		
		Border miscB = new Border(0, 0, elementWidth/4 -3, elementHeight - 3, borderMat, 6);
		miscB.setLocalTranslation(3, 3*elementHeight, -4);
		customerDataPanel.attachChild(miscB);
				
		BitmapText videoText = createText(new Vector3f(10, 4*elementHeight - 10, -3));
		videoText.setName("videoText");
		textStartTranslations.put(videoText.getName(), 10f);
	    customerDataPanel.attachChild(videoText);
	    
//		------- Init First Content Image -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(true, false, "firstImageGeom", new Vector3f(0, 2*elementHeight, 1)));
		
		Border firstImageB = new Border(0, 0, 3*elementWidth/4 -3, elementHeight, borderMat, 6);
		firstImageB.setLocalTranslation(3, 2*elementHeight, 1);
		customerDataPanel.attachChild(firstImageB);
		
		//------- Init First Content Text -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(false, false, "firstTextGeom", new Vector3f(3*elementWidth/4, 2*elementHeight, -4)));
		
		Border firstTextB = new Border(0, 0, elementWidth/4 - 3, elementHeight, borderMat, 6);
		firstTextB.setLocalTranslation(3*elementWidth/4, 2*elementHeight, -4);
		customerDataPanel.attachChild(firstTextB);
		
		BitmapText firstText = createText(new Vector3f(3*elementWidth/4 + 10, 3*elementHeight - 10, -3));
		firstText.setName("firstText");
		textStartTranslations.put(firstText.getName(), 3*elementWidth/4 + 10);
		customerDataPanel.attachChild(firstText);
		
//		------- Init Second Content Image -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(true, true, "secondImageGeom", new Vector3f(elementWidth/4, elementHeight, 1)));
		
		Border secondImageB = new Border(0, 0, 3*elementWidth/4 -3, elementHeight, borderMat, 6);
		secondImageB.setLocalTranslation(elementWidth/4, elementHeight, 1);
		customerDataPanel.attachChild(secondImageB);
		
		//------- Init Second Content Text -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(false, true, "secondTextGeom", new Vector3f(0, elementHeight, -4)));
				
		Border secondTextB = new Border(0, 0, elementWidth/4 -3, elementHeight, borderMat, 6);
		secondTextB.setLocalTranslation(3, elementHeight, -4);
		customerDataPanel.attachChild(secondTextB);
		
		BitmapText secondText = createText(new Vector3f(10, 2*elementHeight - 10,  -3));
		secondText.setName("secondText");
		textStartTranslations.put(secondText.getName(), 10f);
		customerDataPanel.attachChild(secondText);
		
//		------- Init Third Content Image -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(true, false, "thirdImageGeom", new Vector3f(0, 0, 1)));
		
		Border thirdImageB = new Border(0, 0, 3*elementWidth/4 -3, elementHeight - 3, borderMat, 6);
		thirdImageB.setLocalTranslation(3, 3, 1);
		customerDataPanel.attachChild(thirdImageB);
		
		//------- Init Third Content Text -----------------------------------------------
		customerDataPanel.attachChild(createQuadGeom(false, false, "thirdTextGeom", new Vector3f(3*elementWidth/4, 0, -4)));
				
		Border thirdTextB = new Border(0, 0, elementWidth/4 -3, elementHeight - 3, borderMat, 6);
		thirdTextB.setLocalTranslation(3*elementWidth/4, 3, -4);
		customerDataPanel.attachChild(thirdTextB);
		
		BitmapText thirdText = createText(new Vector3f(3*elementWidth/4 + 10, elementHeight - 10,  -3));
		thirdText.setName("thirdText");
		textStartTranslations.put(thirdText.getName(), 3*elementWidth/4 + 10);
		customerDataPanel.attachChild(thirdText);
	}
	
	private Geometry createQuadGeom(boolean isImage, boolean isUneven, String name, Vector3f translation) {
		Geometry newGeom;
		if(isImage) {
			Quad newQuad = new Quad(3*elementWidth/4, elementHeight);
			newGeom = new Geometry(name, newQuad);
			Material newM = mat.clone();
			newM.setColor("Color", ColorRGBA.White);
			newGeom.setMaterial(newM);
			newGeom.setLocalTranslation(translation);
		}
		else {
			Quad newQuad = new Quad(elementWidth/4, elementHeight);
			newGeom = new Geometry(name, newQuad);
			Material newM = mat.clone();
			if(isUneven) newM.setColor("Color", azure);
			else newM.setColor("Color", thomasesSkinColor);
			newGeom.setMaterial(newM);
			newGeom.setLocalTranslation(translation);
		}
		return newGeom;
	}

	private BitmapText createText(Vector3f translation) {
		BitmapText text = new BitmapText(myFont_20, false);
		text.setBox(new Rectangle(0, 0, elementWidth/4 - 20, elementHeight - 20));
		text.setLineWrapMode(LineWrapMode.Character);
		text.setSize(myFont_20.getCharSet().getRenderedSize());
		text.setColor(ColorRGBA.Black);
		text.setQueueBucket(RenderQueue.Bucket.Translucent);
		text.setText("");
		text.setLocalTranslation(translation);
		return text;
	}

	public float getTextOffset(String text) {
		return textStartTranslations.get(text);
	}
	
	public float getElementHeight() {
		return elementHeight;
	}
}
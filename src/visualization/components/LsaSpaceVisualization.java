/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization.components;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.vecmath.Point2f;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.RenderState.BlendMode;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;
import com.jme3.scene.shape.Quad;

import visualization.Node2D;
import visualization.PresentationVars;

public class LsaSpaceVisualization {
	private Vector<Material> materials = new Vector<Material>();
	private Map<String, Vector<String>> bondMap = new HashMap<String, Vector<String>>();
	private Map<String, Vector<Point2f>> bondLineSources = new HashMap<String, Vector<Point2f>>();
	private Map<String, Point2f> bondLineTargets = new HashMap<String, Point2f>();
	private AssetManager assetMgr;
	private BitmapFont guiFont;
	
	private int width, height;
	private Node2D my_parent;
	private Node lsaNode;
	
	public LsaSpaceVisualization(Node2D parent, int scrWidth, int scrHeight) {
		assetMgr = (AssetManager) PresentationVars.instance().get(
				PresentationVars.ASSET_MGR);
		guiFont = assetMgr.loadFont("Interface/Fonts/Default.fnt");
		
		width = scrWidth;
		height = scrHeight;
		my_parent = parent;
		
		lsaNode = new Node();
		my_parent.attachChild(lsaNode);
		
		Material m = new Material(assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
		m.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		m.setColor("Color", ColorRGBA.Red);
		materials.add(m);
		
		m = new Material(assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
		m.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		m.setColor("Color", ColorRGBA.White);
		materials.add(m);
		
		m = new Material(assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
		m.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		m.setColor("Color", ColorRGBA.Orange);
		materials.add(m);
		
		m = new Material(assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
		m.getAdditionalRenderState().setBlendMode(BlendMode.Alpha);
		m.setColor("Color", ColorRGBA.Yellow);
		materials.add(m);
	}
	
	public void update(String current_lsas) {
		if(current_lsas.length()==0) return;
		lsaNode.detachAllChildren();
		bondMap.clear();
		bondLineSources.clear();
		bondLineTargets.clear();
		
		Map<String, Map<Integer, Vector<Integer>>> parsedLsas = parseLsaString(current_lsas);
		
		int nrOfLSA = 0;
		for(String key : parsedLsas.keySet()) {
			Map<Integer, Vector<Integer>> tmp_PropertyCount = parsedLsas.get(key);
			
			BitmapText lsaName = new BitmapText(assetMgr.loadFont("Interface/Fonts/Default.fnt"), false);
			lsaName.setSize(guiFont.getCharSet().getRenderedSize()/1.2f);
			lsaName.setColor(ColorRGBA.Green);   
			lsaName.setText(key+"#"+tmp_PropertyCount.get(4).elementAt(0));
			lsaName.setLocalTranslation(width/50, 
					height/2 + nrOfLSA*(height/80+width/60) + lsaName.getLineHeight(), 0);
			lsaNode.attachChild(lsaName);
			
			bondLineTargets.put(key, new Point2f(width/50 + lsaName.getLineWidth(), height/2 + nrOfLSA*(height/80+width/60) + lsaName.getLineHeight()/2 ));
			
			Vector<Integer> standardProp = tmp_PropertyCount.get(0);
			Vector<Integer> subProperties = tmp_PropertyCount.get(1);
			Vector<Integer> bonds = tmp_PropertyCount.get(2);
			Vector<Integer> syntheticProp = tmp_PropertyCount.get(3);
			
			int offset = 0;
			for(int i : standardProp) {
				createQuads(i, lsaName.getLineWidth(), nrOfLSA, offset, 0, key);
				offset += i;
			}
			for(int i : subProperties) {
				createQuads(i+1, lsaName.getLineWidth(), nrOfLSA, offset, 1, key);
				offset += i+1;
			}
			for(int i : bonds) {
				createQuads(i, lsaName.getLineWidth(), nrOfLSA, offset, 2, key);
				offset += i;
			}
			for(int i : syntheticProp) {
				createQuads(i, lsaName.getLineWidth(), nrOfLSA, offset, 3, key);
				offset += i;
			}
			
			nrOfLSA++;
		}
		
		drawBondLines();
	}

	private void drawBondLines() {
		for(String key : bondMap.keySet()) {
			Vector<String> targets = bondMap.get(key);
			Vector<Point2f> sources = bondLineSources.get(key);
			if(sources == null) {// || targets.size()!=sources.size()) {
				System.out.println("Source null");
				break;
			}
			
			for(int i = 0; i< targets.size(); i++) {
				String target = targets.elementAt(i);
				Point2f source = sources.elementAt(i);
				Point2f lineTarget = bondLineTargets.get(target);
				if(lineTarget == null) {
					System.out.println("BondLineTarget LSA not found");
					break;
				}
				
				Line bond = new Line(new Vector3f(source.x,source.y,1), new Vector3f(lineTarget.x, lineTarget.y, 1));
				bond.setLineWidth(3);
				Geometry bondGeo = new Geometry("bond", bond);
				Material bondMat = new Material(assetMgr,"Common/MatDefs/Misc/Unshaded.j3md");
				bondMat.setColor("Color", ColorRGBA.Orange);
				bondGeo.setMaterial(bondMat);
				lsaNode.attachChild(bondGeo);
			}
		}
		
	}

	private void createQuads(int quadCount, float linewidth, int nrOfLSA, int offset, int type, String key) {
		float quad_height = width/60, quad_width = width/60;
		float height_offset = 0, width_offset = 0;
		
		for(int quadIndex = 0; quadIndex < quadCount; quadIndex++) {
			if(type == 1 && quadIndex > 0) {
				quad_height = width/120;
				quad_width = width/120;
				height_offset = width/240;
				width_offset = width/240;
			}
			Quad newQ = new Quad(quad_width, quad_height);
			Geometry newQG = new Geometry("newQ"+quadIndex, newQ);
			
			switch(type) {
				case 0: newQG.setMaterial(materials.get(0)); break;
				case 1: newQG.setMaterial(materials.get(1)); break;
				case 2: newQG.setMaterial(materials.get(2)); break;
				case 3: newQG.setMaterial(materials.get(3)); break;
				default: newQG.setMaterial(materials.get(0)); break;
			}
						
			newQG.setLocalTranslation(width/50 + 10 + linewidth + (offset*(width/60+10)) + (quadIndex*(width/60+10)) + width_offset, 
					height/2 + nrOfLSA*(height/80+width/60) + height_offset, 2);
			lsaNode.attachChild(newQG);
			
			if(type == 2) {
				Vector<Point2f> current_Sources = bondLineSources.get(key);
				if(current_Sources == null) current_Sources = new Vector<Point2f>();
				current_Sources.add(
						new Point2f(
								width/50 + 10 + linewidth + (offset*(width/60+10)) + (quadIndex*(width/60+10)) + width/120,
								height/2 + nrOfLSA*(height/80+width/60) + width/120));				
				bondLineSources.put(key, current_Sources);
			}
		}
	}

	private Map<String, Map<Integer, Vector<Integer>>> parseLsaString(String lsa_space) {
		Map<String, Map<Integer, Vector<Integer>>> result = new HashMap<String, Map<Integer, Vector<Integer>>>();
		String[] lsas = lsa_space.split("\r\n");
		
		for(String lsa : lsas) {
			String key = lsa.substring(1, lsa.indexOf(","));
			lsa = lsa.substring(lsa.indexOf(",")+1);
			String[] properties = lsa.split(",");
			
			int standardProp = 0;
			int subProperties = 0;
			Vector<Integer> subDescriptions = new Vector<Integer>();
			int bonds = 0;
			int syntheticProp = 0;
			
			boolean parseStandardProperty = true;
			boolean parseSubProperty = false;
			boolean parseMultipleBond = false;
			
			for(String prop:properties) {
				prop = prop.trim();
				
				if(parseStandardProperty) {
					if(prop.indexOf("<") != -1) {
						parseSubProperty = true;
						parseStandardProperty = false;
					}
					else if(prop.indexOf("#") != -1) {
						syntheticProp++;
					}
					else if(prop.indexOf("[") != -1) {
						standardProp++;
					}
				}
				else if(parseSubProperty) {
					if(prop.indexOf("#") != -1 || parseMultipleBond) {
						bonds++;
						
						// Single Bond
						if(!parseMultipleBond && prop.indexOf("]") != -1) {
							//Enter this bond to draw line
							String[] target_arr = prop.split("[=>]");
							String target = "";
							if(target_arr.length>=2) {
								target = target_arr[1];
								target = target.substring(1, target.length()-1);
								Vector<String> current_bonds = bondMap.get(key);
								if(current_bonds == null) current_bonds = new Vector<String>();
								current_bonds.add(target);
								bondMap.put(key, current_bonds);
							}							
						}
						//Multiple Bonds
						else {
							if(!parseMultipleBond) {
								//first Bond of Many
								String[] target_arr = prop.split("[=]");
								String target = "";
								if(target_arr.length>=2) {
									target = target_arr[1].substring(1);
									Vector<String> current_bonds = bondMap.get(key);
									if(current_bonds == null) current_bonds = new Vector<String>();
									current_bonds.add(target);
									bondMap.put(key, current_bonds);
								}
								
								parseMultipleBond = true;
							}
							else if(prop.indexOf("]") != -1) {
								//last Bond of Many
								String[] target_arr = prop.split("[]]");
								String target = target_arr[0];
								Vector<String> current_bonds = bondMap.get(key);
								if(current_bonds == null) current_bonds = new Vector<String>();
								current_bonds.add(target);
								bondMap.put(key, current_bonds);
								
								parseMultipleBond = false;
							}
							else {
								//one Bond of Many
								Vector<String> current_bonds = bondMap.get(key);
								if(current_bonds == null) current_bonds = new Vector<String>();
								current_bonds.add(prop);
								bondMap.put(key, current_bonds);
							}
						}
					}
					if(prop.indexOf(">") != -1) {
						parseSubProperty = false;
						parseStandardProperty = true;
						subDescriptions.add(subProperties);
						subProperties = 0;
					}
					else if(!parseMultipleBond && prop.indexOf("[") != -1) {
						subProperties++;
					}
				}
			}
			
			Vector<Integer> stPropV = new Vector<Integer>();
			stPropV.add(standardProp);
			
			Vector<Integer> bondV = new Vector<Integer>();
			bondV.add(bonds);
			
			Vector<Integer> syntheticPropV = new Vector<Integer>();
			syntheticPropV.add(syntheticProp);
			
			Map<Integer, Vector<Integer>> v = new HashMap<Integer, Vector<Integer>>();
			v.put(0, stPropV);v.put(1, subDescriptions);v.put(2, bondV);v.put(3, syntheticPropV);
			
			Vector<Integer> hashV = new Vector<Integer>();
			hashV.add(key.hashCode());
			v.put(4, hashV);
			
			result.put(key, v);
		}

		return result;
	}
}

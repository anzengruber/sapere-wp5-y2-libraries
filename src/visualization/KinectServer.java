/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class KinectServer extends Thread {
	private ServerSocket server;
	private Socket client;
	private boolean run;
	
	public KinectServer() {
		run = true;
		
		try {
			server = new ServerSocket(9999);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		while(run) {
			try {
				client = server.accept();
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			ObjectInputStream ois; 
	        try {
	        	ois = new ObjectInputStream(client.getInputStream());
	        	
	        	DataFrame receivedData = (DataFrame) ois.readObject();

	        	PresentationService.instance().parseKinectInput(receivedData.getMessage());
	        	
	        	client.close();
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    	} catch (ClassNotFoundException e) {
	    		e.printStackTrace();
	    	}
		}
		
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void quit() {
		run = false;
	}
}

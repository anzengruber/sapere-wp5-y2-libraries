/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package visualization;

import java.util.Map;
import java.util.HashMap;

/**
 * 
 * @author maschek
 */
public class PresentationVars {

	public static final String ASSET_MGR = "ASSET_MGR";
	public static final String TIMER = "TIMER";
	public static final String STEERING_MGR = "STEERING_MGR";
	public static final String SAPERE_GUI = "SAPERE_GUI";
	
	public static final String ASSET_PATH = MyProperties.instance().getProperty("asset.path");
	public static final int SCR_WIDTH = new Integer(MyProperties.instance().getProperty("scr.width")).intValue();
	public static final int SCR_HEIGHT = new Integer(MyProperties.instance().getProperty("scr.height")).intValue();
	public static final float F_SCR_WIDTH = (float)SCR_WIDTH;
	public static final float F_SCR_HEIGHT = (float)SCR_HEIGHT;
	public static final boolean FULLSCREEN = new Boolean(MyProperties.instance().getProperty("fullscreen")).booleanValue();
	public static final int NUM_VERTICAL_SCREENS = new Integer(MyProperties.instance().getProperty("num.vertical.screens")).intValue();
	public static final int SINGLE_SCR_HEIGHT = SCR_HEIGHT / NUM_VERTICAL_SCREENS;
	public static final float F_SINGLE_SCR_HEIGHT = F_SCR_HEIGHT / NUM_VERTICAL_SCREENS;

	public static final float PAGE_TURN_TIME = new Float(MyProperties.instance().getProperty("page.turn.time")).floatValue();
	public static final boolean DISABLE_GUI = new Boolean(MyProperties.instance().getProperty("disable.gui")).booleanValue();

	public static final int RUN_TIME_S = new Integer(MyProperties.instance().getProperty("runtime")).intValue();
	public static final long INIT_TIME = System.currentTimeMillis();
	
	private static PresentationVars instance = null;
	private Map<String, Object> map;
	
	private PresentationVars() {
		map = new HashMap<String, Object>();
	}

	public static PresentationVars instance() {
		if (instance == null) {
			instance = new PresentationVars();
		}
		return instance;
	}
	
	public void set(String key, Object val) {
		map.put(key, val);
	}

	public Object get(String key) {
		return map.get(key);
	}

	public int getInt(String key) {
		return (Integer) map.get(key);
	}
	
	public void deInit() {
		map.clear();
	}
}

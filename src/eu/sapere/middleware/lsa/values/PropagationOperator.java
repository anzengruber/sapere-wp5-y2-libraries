/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.lsa.values;

/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
	
public enum PropagationOperator {
		
		DIFFUSION_OP("diffusion_op"),
		
		FIELD_VALUE("field_value"),
		
		SOURCE("souce"),
		
		PREVIOUS ("previous"),
		
		DESTINATION ("destination"),
		
		AGGREGATION_OP("aggregation_op"),
		
		DIRECT("direct");
		
		  private String text;

		  PropagationOperator (String text) {
		    this.text = text;
		  }

		  public String getText() {
		    return this.text;
		  }

		  public static PropagationOperator fromString(String text) {
		    if (text != null) {
		      for (PropagationOperator b : PropagationOperator.values()) {
		        if (text.equalsIgnoreCase(b.text)) {
		          return b;
		        }
		      }
		    }
		    return null;
		  }

		
		
		
		public String getOperator (String op){
			return op.substring(op.indexOf("_"));
			
		//	return aOp;
		}
		
		public int getHop(String op){
			return new Integer(op.substring(op.indexOf("_"))).intValue();
		}
		
		
	}



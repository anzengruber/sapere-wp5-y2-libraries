/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.lsa.values;

import java.util.Date;
import java.util.Vector;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticProperty;
import eu.sapere.middleware.lsa.exception.UnresolvedPropertyNameException;



public enum AggregationOperators {
		
		MAX ("max"),
		
		MIN ("min"), 
		
		AVG ("avg");
		
		private String text;

		AggregationOperators (String text) {
		    this.text = text;
		  }

		  public String getText() {
		    return this.text;
		  }
		
		public static Id max (Vector<Lsa> allLsa){
			
			Id ret = null;
			
			if(allLsa.isEmpty())
				return null;
			
			int max = new Integer(allLsa.elementAt(0).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
			ret = allLsa.elementAt(0).getId();
			
			for(int i=0; i<allLsa.size(); i++){
				if (new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue() > max){
					max = new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
					ret =  allLsa.elementAt(i).getId();
				}
			}
			
			return ret;
			
		}
		
		public static Id min (Vector<Lsa> allLsa){
			
			Id ret = null;
			
			if(allLsa.isEmpty())
				return null;
			
			int min = new Integer(allLsa.elementAt(0).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
			ret = allLsa.elementAt(0).getId();
			
			for(int i=0; i<allLsa.size(); i++){
				if (new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue() < min){
					min = new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
					ret =  allLsa.elementAt(i).getId();
				}
			}
			
			return ret;
			
		}
		
		public static Lsa mean (Vector<Lsa> allLsa){
			
			Lsa ret = null;
			
			if(allLsa.isEmpty())
				return null;
			
			int val = 0;
			
			for(int i=0; i<allLsa.size(); i++){
					val += new Integer(allLsa.elementAt(i).getProperty(allLsa.elementAt(0).getFieldValue()).getValue().elementAt(0)).intValue();
			}
			
			
			val = val / allLsa.size();
			
			ret = allLsa.elementAt(0).getCopy();
			try {
				ret.setProperty(new Property(allLsa.elementAt(0).getFieldValue(), ""+val));
			} catch (UnresolvedPropertyNameException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
				ret.addProperty(new SyntheticProperty(SyntheticPropertyName.LAST_MODIFIED, new String(""+new Date().getTime())));
			
			System.out.println("Lsa res "+ret.toString());
			
			return ret;
			
		}

}

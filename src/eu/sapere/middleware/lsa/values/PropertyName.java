/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.lsa.values;

/**
 * Well-known Properties
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public enum PropertyName {
	
	DIFFUSION_OP("diffusion_op"), 
	
	AGGREGATION_OP ("aggregation_op"), 
	
	FIELD_VALUE ("field_value"),
	
	SOURCE ("source"), 

	PREVIOUS ("previous"), 
	
	DESTINATION ("destination"),
	
	ACTIVE_PROPAGATION("active_propagation"),
	
	// Used for the DECAY ECO-LAW, workes as a TIME-TO-LIVE
	DECAY ("decay"); 
	
	private PropertyName (final String text) {
        this.text = text;
    }

    private final String text;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return text;
    }

	
	public static boolean isDecay(String s){
		return s.toLowerCase().equals(DECAY.toString());
	};
	
	private static final int decayDecrement = 1;
	
	public static boolean isDecayValue (String s) {
        
        try {

            Integer.parseInt(s);
        
        } catch (NumberFormatException ex) {
            return false;
        }
        
        return true;
    }
	
	public static Integer getDecayValue(String s){
		
		Integer n = null;
		
		try {

            n = Integer.parseInt(s);
        
        } catch (NumberFormatException ex) {
            
        }
		
		return n;
	}
	
	public static Integer decrement (Integer n){		
		return n - decayDecrement;
	}
	
	public static boolean isExpired(Integer n){
		return n.intValue() == 0;
	}
	
}

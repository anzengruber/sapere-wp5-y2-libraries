/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.lsa.autoupdate;

import java.util.Iterator;
import java.util.Vector;

// Event Generator
public abstract class EventGenerator {
	
	 private Vector<PropertyValueListener> listeners = new Vector<PropertyValueListener>();
	 
	 private String propertyName = null;
	 
	 private boolean onAppend = false;
	 
	 public EventGenerator(){
	 }

	 public EventGenerator(boolean onAppend){
		 
		 this.onAppend = onAppend;
	 }
	 
	 public void setPropertyName(String propertyName){
		 this.propertyName = propertyName;
	 }
    
	 public final void autoUpdate(String s) {
		 firePropertyValueEvent(s);
	 }

	 public final synchronized void addPropertyValueListener( PropertyValueListener l ) {
		 listeners.add( l );
	 }

	 public final synchronized void removePropertyValueListener( PropertyValueListener l ) {
		 listeners.remove( l );
	 }

	 private final synchronized void firePropertyValueEvent(String s) {

        PropertyValueEvent newEvent = new PropertyValueEvent( this, propertyName, s );
        
        Iterator<PropertyValueListener> pvListeners = listeners.iterator();
        while( pvListeners.hasNext() ) {
        	
        	if (onAppend == true)
        		( (PropertyValueListener) pvListeners.next() ).PropertyValueGenerated( newEvent );
        	else 
        		( (PropertyValueListener) pvListeners.next() ).PropertyValueAppendGenerated( newEvent );
        }
    }
}

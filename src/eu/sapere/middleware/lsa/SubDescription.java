/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.lsa;


import java.util.Iterator;
import java.util.Map;

import eu.sapere.middleware.lsa.values.SyntheticPropertyName;

/**
 * 
 * Represents a SubDescription 
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class SubDescription extends Description{
	
	protected String name = null;

	public SubDescription(String name, Id id, Property p) {
		super(id, p);
		this.name = name;
	}

	public SubDescription(String name, Id id, Property[] properties) {
		super(id, properties);
		this.name = name;
	}
	
	protected SubDescription(String name, Id id, Map<String, Property> Properties, Map<SyntheticPropertyName, Property> SyntheticProperties) {
		super(id, Properties, SyntheticProperties);
		this.name = name;
	}

	private static final long serialVersionUID = 7319728270982187084L;

	@Override
	public SubDescription getCopy(){
		Description d = super.getCopy();
		SubDescription sd = new SubDescription(name, d.getId().getCopy(), d.Properties, d.SyntheticProperties);
		return sd;
	}

	/**
	 * Returns the specified Property
	 */
	public Property getProperty (String name){
		return Properties.get(name);
	}
	
	public Iterator<Property> getProperties() {
		Iterator<Property> ret = null;
		
		ret = this.Properties.values().iterator();
		
		return ret;
	}
	
	public String getName() {
		return name;
	}
	
	
	/**
	 * Returns true if the SD has a Bond with the specified LSA id. Both LSA and its SubDescriptions
	 * @param LsaId the LSA id
	 * @return true if the LSA has a Bond with the specified LSA id, false otherwise
	 */

	public boolean hasBondWith(Id LsaId){
		return this.hasBond(LsaId.toString());
	}
}

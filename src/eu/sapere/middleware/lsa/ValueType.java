/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.lsa;

/**
 * Value Types for SAPERE Properties
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public enum ValueType {
	
	FORMAL_ONE ("?"),
	FORMAL_MANY("*"),
	POTENTIAL("!"),
	ACTUAL(null);
	
	private ValueType (final String text) {
        this.text = text;
    }

    private final String text;

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return text;
    }
	
	//public static String FORMAL_ONE = "?";
	//public static String FORMAL_MANY = "*";
	//public static String POTENTIAL =  "!";
	

	public static boolean isActual(String s){
		boolean ret = true;
		
		if (s.equals(FORMAL_ONE.toString()) || s.equals(FORMAL_MANY.toString()) || s.equals(POTENTIAL.toString()))
			ret = false;
		return ret;
	}
	
	public static boolean isPotential(String s){
		boolean ret = false;
		
		if (s.equals(POTENTIAL.toString()))
			ret = true;
		return ret;
	}
	
	public static boolean isFormal (String s){
		boolean ret = false;		
		if (isFormalOne (s) || isFormalMany (s))
			ret = true;
		return ret;
	}
	
	public static boolean isFormalOne (String s){
		boolean ret = false;
		
		if (s.equals(FORMAL_ONE.toString()) )
			ret = true;
		return ret;
	}
	
	public static boolean isFormalMany (String s){
		boolean ret = false;
		
		if (s.equals(FORMAL_MANY.toString()) )
			ret = true;
		return ret;
	}
	
	
	
	public static boolean matches (String s1, String s2){

		if (isActual(s1) && isActual(s2) )
			return s1.equals(s2);
		
		if (isActual(s1) && isPotential(s2) )
			return false;
		
		if (isActual(s1) && isFormal(s2) )
			return true;
		
		if ( isPotential(s1) && isFormalOne(s2))
			return true;
		
		if ( isPotential(s1) && ! isFormalOne(s2))
			return false;
		
		if (isFormalOne(s1) && isActual(s2) )
			return true;
		
		if (isFormalOne(s1) && isPotential(s2) )
			return true;
		
		if (isFormalOne(s1) && isFormal(s2) )
			return false;
		
		if (isFormalMany(s1) && isActual(s2) )
			return true;
		
		if (isFormalOne(s1) && ! isActual(s2) )
			return false;
		
		return false;
	}
	
	
	
	};

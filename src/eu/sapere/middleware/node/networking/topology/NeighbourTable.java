/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;


import eu.sapere.middleware.agent.BasicSapereAgent;
import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.networking.topology.util.TimeLimitedCacheMap;

/**
 * @author Gabriella Castelli (UNIBO)
 *
 */
public class NeighbourTable {
	
	
	private TimeLimitedCacheMap table = null;
	private HashMap<String, Id> lsaTable = null;
	
	private BasicSapereAgent agent = null;
//	private NodeManager sMng= null;

	/**
	 * 
	 */
	public NeighbourTable(NodeManager sMng, long initialDelay, long evictionDelay, long expiryTime, TimeUnit unit,  NeighbourListener listener) {
		
//		this.sMng = sMng;
		table = new TimeLimitedCacheMap (initialDelay, evictionDelay, expiryTime, unit, listener);
		lsaTable = new HashMap<String, Id>();
	}
	
	public void put(String key, HashMap<String, String> data){
		table.put(key, data);
		
		Lsa mylsa = new Lsa();
		
		Iterator<Entry<String, String>> i = data.entrySet().iterator();
		while(i.hasNext()){
			Entry<String, String> e = i.next();
			mylsa.addProperty(new Property((String)e.getKey(), (String)e.getValue()) );
		}	
			
		agent = new BasicSapereAgent("NetworkTopologyManager");
		
		if (lsaTable.containsKey(key)){
//			mylsa.removeProperty("btMac");
			agent.updateOperation(mylsa, lsaTable.get(key));
		}
		else{
//			mylsa.removeProperty("btMac");
			Id n = agent.injectOperation(mylsa);
			lsaTable.put(key, n);
		}
		
	}

	public void remove(String key) {
		if (lsaTable.containsKey(key)) {
			table.remove(key, false);
			lsaTable.remove(key);
		}
	}
}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology.physical;

import eu.sapere.middleware.node.networking.physical.bluetooth.BluetoothDeviceListener;
import eu.sapere.middleware.node.networking.physical.bluetooth.BluetoothServiceDiscovery;
import eu.sapere.middleware.node.networking.physical.bluetooth.RegistrationService;

public class PhysicalNetworkAnalyzer extends Thread implements
		BluetoothDeviceListener {

	private PhysicalNetworkListener listener = null;

	public PhysicalNetworkAnalyzer(PhysicalNetworkListener listener) {
		this.listener = listener;
	}

	public void run() {
		BluetoothServiceDiscovery discovery = BluetoothServiceDiscovery.instance();
		discovery.addBluetoothDeviceListener(this);
		if(!discovery.wasStarted()) discovery.start();
		RegistrationService.instance().addPhysicalNetworkListener(this);
	}

	@Override
	public void onBtDeviceDetected(PhysicalNeighbour neighbor) {
		listener.onPhysicalNeighbourFound(neighbor); // FIXME
	}

	@Override
	public void onBtDeviceLost(String btMac) {
		listener.onPhysicalNeighbourLost(btMac); // FIXME
	}
}
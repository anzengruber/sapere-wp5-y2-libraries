/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology.logical;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import visualization.MyProperties;
import eu.sapere.middleware.node.NodeManager;

/**
 * @author Gabriella Castelli
 * 
 */
public class LogicalNetworkAnalyzer {

	private LogicalNetworkListener listener = null;

	/**
	 * @param networkTopologyManager
	 * 
	 */
	public LogicalNetworkAnalyzer(LogicalNetworkListener listener) {

		this.listener = listener;
	}

	public void staticNeighboursList() {
		LogicalNeighbour ln;
		try {
			String fileName = MyProperties.instance().getProperty(
					"neighborsPath");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName)));
			String line;
			String spaceName = NodeManager.getSpaceName();
			while ((line = reader.readLine()) != null) {
				if (line.startsWith(spaceName)) {
					String[] neighbors = line.split(";");
					for (int i = 1; i < neighbors.length; i++) {
						String ip = MyProperties.instance().getProperty(
								neighbors[i]);
						ln = new LogicalNeighbour("", ip, neighbors[i], "pc");
						listener.onLogicalNeighbourFound(ln);
					}
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.topology;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import eu.sapere.middleware.agent.BasicSapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.networking.topology.logical.LogicalNeighbour;
import eu.sapere.middleware.node.networking.topology.logical.LogicalNetworkAnalyzer;
import eu.sapere.middleware.node.networking.topology.logical.LogicalNetworkListener;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNetworkAnalyzer;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNetworkListener;
import eu.sapere.middleware.node.networking.topology.util.TimeLimitedCacheMap;

/**
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class NetworkTopologyManager implements PhysicalNetworkListener,
		LogicalNetworkListener, NeighbourListener {

	private NodeManager sMng = null;

	private TimeLimitedCacheMap physicalNeighbours = null;
	private TimeLimitedCacheMap logicalNeighbours = null;
	private NeighbourTable neighbours = null;

	private PhysicalNetworkAnalyzer pNetAnalyzer = null;
	private LogicalNetworkAnalyzer lNetAnalyzer = null;
	private NeighbourAnalyzer neighbourAnalyzer = null;

	// private boolean started = false;

	public NetworkTopologyManager(NodeManager sMng) {
		this.sMng = sMng;
		initializeNetworkTopologyManager();
	}

	public void initializeNetworkTopologyManager() {
		physicalNeighbours = new TimeLimitedCacheMap(1, 10, 15, TimeUnit.DAYS,
				this);
		logicalNeighbours = new TimeLimitedCacheMap(1, 10, 15, TimeUnit.DAYS,
				this);

		// logicalNeighbours = new TimeLimitedCacheMap(10, 10, 10,
		// TimeUnit.DAYS,
		// null);

		neighbours = new NeighbourTable(sMng, 1, 10, 15, TimeUnit.DAYS, this);

		pNetAnalyzer = new PhysicalNetworkAnalyzer(this);
		pNetAnalyzer.start();
		lNetAnalyzer = new LogicalNetworkAnalyzer(this);
		lNetAnalyzer.staticNeighboursList();
		neighbourAnalyzer = new NeighbourAnalyzer(physicalNeighbours,
				logicalNeighbours, this);

		new Thread(neighbourAnalyzer).start();
	}

	public void onPhysicalNeighbourFound(PhysicalNeighbour neighbour) {
		physicalNeighbours.put(neighbour.getNodeName(), neighbour.getData());
	}

	@Override
	public void onPhysicalNeighbourLost(String btMacAddress) {
		physicalNeighbours.remove(btMacAddress, true);
	}

	@Override
	public void onLogicalNeighbourFound(LogicalNeighbour neighbour) {
		// TODO Auto-generated method stub
		logicalNeighbours.put(neighbour.getIpAddress(), neighbour.getData());
	}

	@Override
	public void onLogicalNeighbourLost(String ipAddress) {
		// TODO Auto-generated method stub
		logicalNeighbours.remove(ipAddress, true);
	}

	public void onNeighbourFound(String key, HashMap<String, String> data) {
		neighbours.put(key, data);
	}

	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		if (data != null) {
			Lsa mylsa = new Lsa();
			Iterator<Entry<String, String>> i = data.entrySet().iterator();
			while (i.hasNext()) {
				Entry<String, String> e = i.next();
				mylsa.addProperty(new Property((String) e.getKey(), (String) e
						.getValue()));

			}

			Lsa[] allLsas = sMng.getSpace().getAllLsa();
			for (int j = 0; j < allLsas.length; j++) {
				Lsa tmpLsa = allLsas[j];

				Property nProp = tmpLsa.getProperty("neighbour");
				if (nProp != null && nProp.getValue().elementAt(0).equals(key)) {
					mylsa.setId(tmpLsa.getId());
				}
			}
			BasicSapereAgent agent = new BasicSapereAgent(
					"NetworkTopologyManager");
			agent.removeLsa(mylsa);
			neighbours.remove(key);
		}
	}
}
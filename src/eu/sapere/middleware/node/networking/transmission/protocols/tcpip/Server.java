/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import eu.sapere.middleware.lsa.Lsa;

/*
 * Receives remote LSA injection
 */
public class Server extends Thread implements LsaReceived {
	
	private ServerSocket server;
	private ExecutorService executor;
	private Socket socket;
	
	private boolean listening = true;
	private LsaReceived listener;
	
	
	public Server(int port, LsaReceived listener){
		this.listener = listener;
		
		try {
			server = new ServerSocket(port);
			executor = Executors.newCachedThreadPool();
			start();
		
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e1) {
			System.out.println(e1.getMessage());
			e1.printStackTrace();
		}
	}
	
	public void run(){
		while(listening){
			try {
				socket = server.accept();
				
				Future<?> future = executor.submit(new ServerThread (socket, this));
				future.get(5, TimeUnit.SECONDS);
				
			} catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
				executor.shutdownNow();
		        executor = Executors.newCachedThreadPool();
			}
		}
	}
	
	public void quit(){
		
		listening = false;
		
		try {
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public void onLsaReceived(Lsa lsaReceived) {
		//System.out.println("Server: event onLsaReceived");
		//System.out.println(lsaReceived);
		listener.onLsaReceived(lsaReceived);
	}
	
	

}

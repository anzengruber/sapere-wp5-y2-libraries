/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;

/**
 * @author Thomas Schmittner
 * 
 */
public class CommunicationThread extends Thread {

	private static final int COMMUNICATION_PORT = 55510;
	private PhysicalNeighbour neighbor;
	private String message;

	public CommunicationThread(PhysicalNeighbour neighbor, String message) {
		this.neighbor = neighbor;
		this.message = message;
	}

	@Override
	public void run() {
		try {
			Socket socket = new Socket(neighbor.getIpAddress(),
					COMMUNICATION_PORT);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream()));
			writer.write(message);
			writer.flush();
			socket.close();
		} catch (IOException e) {
			// e.printStackTrace();
			System.err.println("Could not establish connection with "
					+ neighbor.getIpAddress());
			// This exception occurs when the Bluetooth device is still in range
			// but the SAPERE app has been shut down already.
		}
	}
}
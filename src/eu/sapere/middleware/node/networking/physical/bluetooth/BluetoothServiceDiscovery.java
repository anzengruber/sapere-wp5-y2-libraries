/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;

import displays.BtLsaPropagationAgent;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;

/**
 * Minimal Device Discovery example.
 */
public class BluetoothServiceDiscovery extends Thread {

	private static final String NODE_TYPE_PC = "pc";
	private static final String PREFIX = "192.168.";
	private Set<String> currentlyDiscovered;
	private Set<String> recentlyDiscovered;
	private ArrayList<BluetoothDeviceListener> listeners;
	private volatile boolean running = true;
	final Object inquiryCompletedEvent = new Object();
	private static BluetoothServiceDiscovery singleton;
	private boolean started = false;
	
	/**
	 * @param physicalNetworkAnalyzer
	 */
	public BluetoothServiceDiscovery() {
		currentlyDiscovered = new HashSet<String>();
		recentlyDiscovered = new HashSet<String>();
		listeners = new ArrayList<BluetoothDeviceListener>();
	}

	public void addBluetoothDeviceListener(BluetoothDeviceListener l) {
		listeners.add(l);
	}
	
	public static BluetoothServiceDiscovery instance() {
		if (singleton == null)
			singleton = new BluetoothServiceDiscovery();
		
		return singleton;
	}

	@Override
	public void run() {
		started = true;
		currentlyDiscovered.clear();
		
		new BtLsaPropagationAgent("BtLsaPropagationAgent").setInitialLSA();
		DiscoveryListener listener = new DiscoveryListener() {

			@Override
			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
				currentlyDiscovered.add(btDevice.getBluetoothAddress());
			}

			@Override
			public void inquiryCompleted(int discType) {
				System.out.println("Device Inquiry completed!");
				synchronized (inquiryCompletedEvent) {
					inquiryCompletedEvent.notifyAll();
				}
			}

			@Override
			public void serviceSearchCompleted(int transID, int respCode) {
				// TODO Auto-generated method stub
			}

			@Override
			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {
				// TODO Auto-generated method stub
			}
		};

		while (running) {
			LocalDevice localDevice;
			try {
				synchronized (inquiryCompletedEvent) {
					localDevice = LocalDevice.getLocalDevice();
					boolean started = localDevice.getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, listener);
					System.out.println(started);
					if (started) {
						System.out.println("Waiting for device inquiry to complete...");
						inquiryCompletedEvent.wait();
						Map<String, PhysicalNeighbour> registeredDevices = RegistrationService.instance().getRegisteredDevices();
						Map<String, PhysicalNeighbour> devices = new HashMap<String, PhysicalNeighbour>();
						synchronized (registeredDevices) {
							devices.putAll(registeredDevices);
						}

						System.out.println(currentlyDiscovered.size() + " device(s) discovered - " + currentlyDiscovered);

						PhysicalNeighbour neighbor;
						Set<String> toRemove = new HashSet<String>();
						for (String key : currentlyDiscovered) {
							if (devices.containsKey(key)) { // is a registered
															// SAPERE device
								if (!recentlyDiscovered.contains(key)) {
									if ((neighbor = devices.get(key)) != null) {
										System.out
												.println("!! Registered device "
														+ key + " found");
										for (BluetoothDeviceListener l : listeners) {
											l.onBtDeviceDetected(neighbor);
										}
										System.out.println("Send found message to device "+ key + " using IP " + devices.get(key).getIpAddress());
										new CommunicationThread(neighbor,getMessage(localDevice,DeviceStatus.FOUND)).start();
									}
								}
							} else {
								toRemove.add(key);
							}
						}
						currentlyDiscovered.removeAll(toRemove);
						for (String key : recentlyDiscovered) {
							if ((neighbor = devices.get(key)) != null) {
								if (!currentlyDiscovered.contains(key)) {
									System.out.println("!! Registered device "+ neighbor.getNodeName() + " lost");
									for (BluetoothDeviceListener l : listeners) {
										l.onBtDeviceLost(neighbor.getNodeName());
									}
									new CommunicationThread(neighbor,getMessage(localDevice,DeviceStatus.LOST)).start();
								} else {
									System.out.println(".. Device " + key + " is still present... do nothing...");
									new CommunicationThread(neighbor,getMessage(localDevice,DeviceStatus.STILL_PRESENT)).start();
								}
							}
						}
						recentlyDiscovered.clear();
						recentlyDiscovered.addAll(currentlyDiscovered);
						currentlyDiscovered.clear();
					}
				}
				
				
			} catch (BluetoothStateException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			try {
				sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private String getMessage(LocalDevice localDevice, DeviceStatus status) {
		StringBuilder builder = new StringBuilder();
		builder.append(status.toString());
		builder.append(";");
		builder.append(getLocalIpAddress(PREFIX));
		builder.append(";");
		builder.append(localDevice.getBluetoothAddress());
		builder.append(";");
		builder.append(NodeManager.getSpaceName());
		builder.append(";");
		builder.append(NODE_TYPE_PC);
		builder.append("\n\r");
		return builder.toString();
	}

	/**
	 * Returns the local IP address starting with the specified prefix.
	 * 
	 * @param prefix
	 *            The prefix of the IP address to return.
	 * @return The IP address if available or null otherwise.
	 */
	private String getLocalIpAddress(String prefix) {
		InetAddress[] addresses = null;
		try {
			addresses = InetAddress.getAllByName(InetAddress.getLocalHost()
					.getHostName());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < addresses.length; i++) {
			String address = addresses[i].getHostAddress();
			if (address.startsWith(prefix)) {
				return address;
			}
		}
		return null;
	}

	public boolean wasStarted() {
		return started;
	}
}
/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.networking.physical.bluetooth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;

/**
 * @author Thomas Schmittner
 * 
 */
public class RegistrationService extends Thread {

	private static final int PORT = 55440;
	private static final String QUIT = "quit";
	private static RegistrationService instance;
	private volatile Map<String, PhysicalNeighbour> devices;

	private ArrayList<BluetoothDeviceListener> bluetoothListeners;
	private ArrayList<ListChangedListener> registrationListeners;

	private RegistrationService() {
		devices = new HashMap<String, PhysicalNeighbour>();
		bluetoothListeners = new ArrayList<BluetoothDeviceListener>();
		registrationListeners = new ArrayList<ListChangedListener>();
	}

	public static RegistrationService instance() {
		if (instance == null) {
			instance = new RegistrationService();
		}
		return instance;
	}

	public void addPhysicalNetworkListener(BluetoothDeviceListener l) {
		bluetoothListeners.add(l);
	}

	public Map<String, PhysicalNeighbour> getRegisteredDevices() {
		return devices;
	}

	public void run() {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(PORT);
			Socket socket;
			while (true) {
				System.out.println("Listening for incoming connection...");
				socket = serverSocket.accept();
				System.out.println("Connection accepted...");

				String ipAddress = null;
				String[] args = null;
				try {
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(socket.getInputStream()));
					args = reader.readLine().split(";");
					ipAddress = socket.getRemoteSocketAddress().toString();
					ipAddress = ipAddress.substring(1, ipAddress.indexOf(':'));
					reader.close();
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					socket.close();
				}
				PhysicalNeighbour neighbor;
				if (args != null) {
					// System.out.println("Here");
					synchronized (devices) {
						if (args[0].equals(QUIT)) {
							System.out.println("Device " + args[1]
									+ " deregistered!");
							String btMac = args[1].replaceAll(":", "");
							devices.remove(btMac);
							for (BluetoothDeviceListener l : bluetoothListeners) {
								l.onBtDeviceLost(args[2]);
							}
							System.out.println("--- " + devices);
						} else {
							neighbor = new PhysicalNeighbour(args[0],
									ipAddress, args[1], args[2]);
							String btMac = args[0].replaceAll(":", "");
							devices.put(btMac, neighbor);
							System.out.println("New device registered:");
							System.out.println("\t" + args[0]);
							System.out.println("\t" + ipAddress);
							System.out.println("\t" + args[1]);
							System.out.println("\t" + args[2]);
							System.out.println("----------------------");
						}
						for (ListChangedListener l : registrationListeners) {
							l.listChanged();
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (serverSocket != null) {
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void addListChangedListener(ListChangedListener l) {
		registrationListeners.add(l);
	}
}
/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.console;

import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import visualization.PresentationService;

import eu.sapere.middleware.node.console.SpaceConsole;
import eu.sapere.middleware.lsa.Lsa;

/**
 * Class that represent a monitor for the Space content
 * 
 * @author Marco Santarelli (UNIBO)
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class SpaceConsoleImpl implements SpaceConsole{
	
	JTextArea lsastext,reactstext;
	private String nodeName;

	
	/**
	 * Creates a new SpaceMonitor
	 */
	public SpaceConsoleImpl(String nodeName){
		this.nodeName = nodeName;
	}
	
	public SpaceConsoleImpl(){
	}
	
	public void startConsole(String nodeName){
		this.nodeName = nodeName;
		initConsole();
	}
	
	private void initConsole(){
		JFrame frame = new JFrame("Node: "+nodeName);
		
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		
		lsastext= new JTextArea();
		lsastext.setEditable(false);
		reactstext= new JTextArea();
		reactstext.setEditable(false);
		JLabel lsaslabel = new JLabel("LSA Space content");
		
		JScrollPane lsaspane = new JScrollPane(lsastext);
//		JScrollPane reactspane = new JScrollPane(reactstext);
		
		main.add(lsaslabel);
		
		main.add(lsaspane);
		
		frame.setLayout(new GridLayout(1,1));
		frame.getContentPane().add(main);
		frame.pack();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(650, 550);
		frame.setResizable(true);
		frame.setVisible(false);
	}
	
	/**
	 * Updates the Monitor, printing the given list of LSA
	 * 
	 * @param list the list of LSA to show
	 */
	public void update(final Lsa[] list){
		SwingUtilities.invokeLater(new Runnable(){
			public void run() {
				String tmp="";
				for(Lsa e:list){
					tmp+=e.toString()+"\r\n";
				}
				lsastext.setText(tmp);
				PresentationService.instance().setLSASpace(tmp);
			}
			
		});
	}

	public void updateGUI(Lsa[] list) {
		// TODO Auto-generated method stub

	}
	


}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace.ecolaws;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.exception.UnresolvedPropertyNameException;
import eu.sapere.middleware.lsa.values.NeighbourLsa;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.lsa.values.SyntheticPropertyName;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;
import eu.sapere.middleware.node.networking.delivery.INetworkDeliveryManager;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Propagation {
	
	public Propagation(){
		
	}
	
	
	public static void execPropagation(Lsa[] allLsa, Space s, OperationManager opMng, INetworkDeliveryManager networkDeliveryManager){
		
		for(int i=0; i<allLsa.length; i++){
		
			if(allLsa[i].isPropagate()){
		
				Lsa copy = allLsa[i].getCopy();
				copy.removeSyntheticProperty(SyntheticPropertyName.BONDS);
				copy.removeSyntheticProperty(SyntheticPropertyName.CREATION_TIME);
				copy.removeSyntheticProperty(SyntheticPropertyName.CREATOR_ID);
				copy.removeSyntheticProperty(SyntheticPropertyName.LAST_MODIFIED);
				copy.removeSyntheticProperty(SyntheticPropertyName.LOCATION);
				
				copy.setId(null);
				
				if (copy.hasPropagatotionOp(PropertyName.PREVIOUS.toString()))
					try {
						copy.setProperty(new Property(PropertyName.PREVIOUS.toString(), s.getName()));
					} catch (UnresolvedPropertyNameException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				else
					copy.addProperty(new Property(PropertyName.PREVIOUS.toString(), s.getName()));
					
					if(isDirectPropagation (allLsa[i])){
						
						//System.out.println("trying direct propagation of "+allLsa[i]);
						
						copy.removeProperty(PropertyName.DESTINATION.toString());
						copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
						
										
						copy.removeBonds();
						
						Lsa llsa[] = s.getAllLsa();
						
						if (allLsa[i].getProperty(PropertyName.DESTINATION.toString()).getValue().elementAt(0).equals("all")){
							for(int j=0; j<llsa.length; j++){
								if (llsa[j].isNeighbour()){
									networkDeliveryManager.doSpread(copy, llsa[j]);
								// Propagates forever, the LSA is not removed from the local LSA space
								// s.remove(allLsa[i].getId(), EcoLawsEngine.ECO_LAWS_PROPAGATION);
								}
								PropagationEvent event = new PropagationEvent(allLsa[i]);
								Notifier.instance().publish(event);
							}
							
						}
						else
							for(int j=0; j<llsa.length; j++){
								//System.out.println("llsa[j]");
								if(llsa[j].isNeighbour()){
									//System.out.println("llsa[j].isNeighbour()"+llsa[j].isNeighbour());
									if(allLsa[i].getProperty(PropertyName.DESTINATION.toString()).getValue().elementAt(0).equals(llsa[j].getProperty(NeighbourLsa.NEIGHBOUR.toString()).getValue().elementAt(0))){									
										networkDeliveryManager.doSpread(copy, llsa[j]);
										
										// Propagates forever, the LSA is not removed from the local LSA space
										// s.remove(allLsa[i].getId(), EcoLawsEngine.ECO_LAWS_PROPAGATION);
										
										PropagationEvent event = new PropagationEvent(allLsa[i]);
										Notifier.instance().publish(event);										
										break;
									}
								}
							}
						
						
					}// end direct
					else
					if(isGradientPropagation(allLsa[i])){
						if (getNextHop(allLsa[i].getProperty(PropertyName.DIFFUSION_OP.toString()).getValue().elementAt(0)) >= 1){
							
//							System.out.println("next Hop calculated");
							
							try {
								copy.setProperty(new Property(PropertyName.DIFFUSION_OP.toString(), getOperator(allLsa[i].getProperty(PropertyName.DIFFUSION_OP.toString()).getValue().elementAt(0))+"_" +(getNextHop(allLsa[i].getProperty(PropertyName.DIFFUSION_OP.toString()).getValue().elementAt(0)))));
								copy.setProperty(new Property(allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0), ""+getNextHop(allLsa[i].getProperty(PropertyName.DIFFUSION_OP.toString()).getValue().elementAt(0)))  );
							} catch (UnresolvedPropertyNameException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
						else 
						{
							//diffuse the LSA, but the diffuse LSA won't be propagated further
							copy.removeProperty(PropertyName.DIFFUSION_OP.toString());
							copy.removeProperty(PropertyName.DESTINATION.toString());
							try {
								copy.setProperty(new Property(allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0), "0" ));
							} catch (UnresolvedPropertyNameException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							
						}
						
						//propagate to all neighbours
//						Lsa Lsas[] = s.getAllLsa(); //check Lsas
						for(int j=0; j<allLsa.length; j++){
							
							if(allLsa[j].isNeighbour()){
						//		System.out.println("spread to"+allLsa[j]);
								networkDeliveryManager.doSpread(copy, allLsa[j]);
							}
						}
						
						PropagationEvent event = new PropagationEvent(allLsa[i]);
						Notifier.instance().publish(event);						
					}//end GradientPropagation
			}
		}
	}
	
	private static boolean isDirectPropagation(Lsa lsa){
		return lsa.getProperty(PropertyName.DIFFUSION_OP.toString()).getValue().elementAt(0).equals("direct");
	}
	
	private static boolean isGradientPropagation(Lsa lsa){
		return lsa.getProperty(PropertyName.DIFFUSION_OP.toString()).getValue().elementAt(0).contains("GRADIENT");
	}

	public static int getNextHop(String op){
		return (new Integer(op.substring(op.indexOf("_")+1)).intValue()-1);
	}
	
	public static String getOperator (String op){
		return op.substring(0, op.indexOf("_"));
	}
}

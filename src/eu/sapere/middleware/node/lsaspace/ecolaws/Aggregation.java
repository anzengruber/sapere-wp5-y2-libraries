/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node.lsaspace.ecolaws;



import java.util.Vector;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.exception.UnresolvedPropertyNameException;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.lsaspace.EcoLawsEngine;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Aggregation {
	
	/* LSA that wants to be propagated MUST have the following fields defined in PropertyValue:
	 * aggregation_op: Aggregation Operators
	 * field_value: Field to which the Aggregation Operator will be applied
	 * source: an id to identify the LSAs to be aggregated together
	 */
	
	public Aggregation(){
		
	}
	
	private static void selfAggregation(Lsa[] allLsa, Space s, OperationManager opMng){
		Vector<String> sourceProcessed = new Vector<String>();
		Vector<Id> toRemove = new Vector<Id>();
		
		for(int i=0; i<allLsa.length; i++){
			
			
			if(allLsa[i].explicitAggregationApplies() && !sourceProcessed.contains(allLsa[i].getAggregationSource().toString())){
				
				sourceProcessed.add(allLsa[i].getAggregationSource().toString());
				
				Vector<Lsa> compatible = getAggregationCompatibleLsa(allLsa[i], allLsa);
				
				if (allLsa[i].getAggregationOp().equals(AggregationOperators.MAX.toString())){
					Id l = null;
					l = AggregationOperators.max(compatible);
					
					if (l != null){
						if(! allLsa[i].getId().toString().equals(l.toString()))
							toRemove.add(allLsa[i].getId());
						
						for(int k=0; k<compatible.size(); k++)
							if(! compatible.elementAt(k).getId().toString().equals(l.toString()))
								toRemove.add(compatible.elementAt(k).getId());
					}
					
				}
				
				if (allLsa[i].getAggregationOp().equals(AggregationOperators.MIN.toString())){
					Id l = null;
					l = AggregationOperators.min(compatible);
					
					if (l != null){
						if(! allLsa[i].getId().toString().equals(l.toString()))
							toRemove.add(allLsa[i].getId());
						
						for(int k=0; k<compatible.size(); k++)
							if(! compatible.elementAt(k).getId().toString().equals(l.toString()))
								toRemove.add(compatible.elementAt(k).getId());
					}
					
				}
				
				if (allLsa[i].getAggregationOp().equals(AggregationOperators.AVG.toString())){
					compatible.add(allLsa[i]);
					
					System.out.println("compatible"+compatible.size());
					
					Lsa l = null;
					l = AggregationOperators.avg(compatible).getCopy();
					
					for(int k=0; k<compatible.size(); k++)
							toRemove.add(compatible.elementAt(k).getId());
					
					System.out.println("\t"+l);
					
					Operation inject = new Operation().injectOperation(l, EcoLawsEngine.ECO_LAWS_AGGREGATION, null);
					opMng.execOperation(inject);
				}
				
				
			}
			
		}
		
		for(int j=0; j<toRemove.size();j++){
			s.remove(toRemove.elementAt(j), EcoLawsEngine.ECO_LAWS_AGGREGATION);
		}
	}
	
	
	private static void otherAggregation(Lsa[] allLsa, Space s, OperationManager opMng){
		
		for(int i=0; i<allLsa.length; i++){
			
			
			if(allLsa[i].requestedAggregationApplies()){
				
				System.out.println(allLsa[i].toString()+" "+allLsa[i].requestedAggregationApplies());
				
				Vector<Lsa> compatible = getRequestAggregationCompatibleLsa(allLsa[i], allLsa);
				
				//for(int j=0; j<compatible.size(); j++ )
					//System.out.println("* "+compatible.elementAt(j).toString());
				//System.out.println(compatible.size());
				
				if (allLsa[i].getAggregationOp().equals(AggregationOperators.MAX.toString())){
					
				/*	l = AggregationOperators.max(compatible, allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					l.addProperty(new Property("aggregated_by", AggregationOperators.MAX.toString()));
					
					System.out.println(allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));*/
					
					String pName= allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					String value = AggregationOperators.maxValue(compatible, allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					if(allLsa[i].hasProperty(pName))
						try {
							allLsa[i].setProperty(new Property(pName, value));
						} catch (UnresolvedPropertyNameException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
						allLsa[i].addProperty(new Property(pName, value));
						
				}
				
				if (allLsa[i].getAggregationOp().equals(AggregationOperators.MIN.toString())){
					
			//		l = AggregationOperators.min(compatible, allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
			//		l.addProperty(new Property("aggregated_by", AggregationOperators.MIN.toString()));	
					
					String pName= allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					String value = AggregationOperators.minValue(compatible, allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					if(allLsa[i].hasProperty(pName))
						try {
							allLsa[i].setProperty(new Property(pName, value));
						} catch (UnresolvedPropertyNameException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
						allLsa[i].addProperty(new Property(pName, value));
				}
				
				if (allLsa[i].getAggregationOp().equals(AggregationOperators.AVG.toString())){
					
				//	 l = AggregationOperators.mean(compatible, allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0)).getCopy();
				//	 l.addProperty(new Property("aggregated_by", AggregationOperators.AVG.toString()));
					String pName= allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0);
					String value = AggregationOperators.avgValue(compatible, allLsa[i].getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0));
					if(allLsa[i].hasProperty(pName))
						try {
							allLsa[i].setProperty(new Property(pName, value));
						} catch (UnresolvedPropertyNameException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					else
						allLsa[i].addProperty(new Property(pName, value));
				}
				
		//			Operation inject = new Operation().injectOperation(l, EcoLawsEngine.ECO_LAWS_AGGREGATION, null);
		//			opMng.execOperation(inject);
			
				
				
			}
			
		}

	}
	
	public static void execAggregation(Lsa[] allLsa, Space s, OperationManager opMng){
		
		selfAggregation(allLsa, s, opMng);
		otherAggregation(allLsa, s, opMng);
		
	}
	
	public static Vector<Lsa> getRequestAggregationCompatibleLsa(Lsa lsa, Lsa[] allLsa){
		Vector<Lsa> ret = new Vector<Lsa>();
		
		for(int i=0; i<allLsa.length; i++){			
			
			if(!allLsa[i].requestedAggregationApplies() && !allLsa[i].explicitAggregationApplies() && ! allLsa[i].hasProperty("aggregated_by") && allLsa[i].hasProperty(lsa.getProperty(PropertyName.FIELD_VALUE.toString()).getValue().elementAt(0)))
				ret.add(allLsa[i].getCopy());
		}
		return ret;
	}
	
	public static Vector<Lsa> getAggregationCompatibleLsa(Lsa lsa, Lsa[] allLsa){
		Vector<Lsa> ret = new Vector<Lsa>();
		for(int i=0; i<allLsa.length; i++){
			
			if(allLsa[i].aggregationApplies() && !allLsa[i].getId().toString().equals(lsa.getId().toString()) && allLsa[i].getAggregationSource().equals(lsa.getAggregationSource()))
				ret.add(allLsa[i].getCopy());
		}
		ret.add(lsa.getCopy());
		return ret;
	}

	//lsa included
	public static Vector<Lsa> getAggregationCompatibleLsaI(Lsa lsa, Lsa[] allLsa){
		Vector<Lsa> ret = new Vector<Lsa>();
		for(int i=0; i<allLsa.length; i++){
			
			if(allLsa[i].aggregationApplies() && !allLsa[i].getId().toString().equals(lsa.getId().toString()) && allLsa[i].getAggregationSource().equals(lsa.getAggregationSource()))
				ret.add(allLsa[i].getCopy());
		}
		
		ret.add(lsa);
		return ret;
	}
}

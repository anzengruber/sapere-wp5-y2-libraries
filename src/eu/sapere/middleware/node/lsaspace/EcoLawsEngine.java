/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace;

import java.util.logging.Logger;

import eu.sapere.middleware.node.networking.delivery.INetworkDeliveryManager;
import eu.sapere.middleware.node.lsaspace.ecolaws.Aggregation;
import eu.sapere.middleware.node.lsaspace.ecolaws.Bonding;
import eu.sapere.middleware.node.lsaspace.ecolaws.Decay;
import eu.sapere.middleware.node.lsaspace.ecolaws.Propagation;
import eu.sapere.middleware.util.SystemConfiguration;

/**
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class EcoLawsEngine implements Runnable {

	public static final String ECO_LAWS_AGENT = "EcoLawsAgent";
	public static final String ECO_LAWS_DECAY = "EcoLawsDecay";
	public static final String ECO_LAWS_AGGREGATION = "EcoLawsAggregation";
	public static final String ECO_LAWS_PROPAGATION = "EcoLawsPropagation";

	private Space space = null;
	private OperationManager opManager = null;
	private INetworkDeliveryManager networkDeliveryManager = null;

	Logger log = null;

	public EcoLawsEngine(Space space) {
		log = SystemConfiguration.getInstance().getLog();

		this.space = space;
	}

	public void setOperationManager(OperationManager opManager) {
		this.opManager = opManager;
	}

	public void setNetworkDeliveryManager(INetworkDeliveryManager networkDeliveryManager) {
		this.networkDeliveryManager = networkDeliveryManager;
	}

	public void run() {

		while (true) {

			// Attendo di essere svegliato
			synchronized (this) {
				try {
					wait(2000);
//					wait();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

			log.finest("Starting EcoLawsManager...");
			Decay.execDecay(space.getAllLsa(), space, opManager);
			log.finer("Decay Eco-law executed");

			Bonding.execBondingEcoLaws(space.getAllLsa(), space, opManager);
			log.finer("Bonding Eco-law executed");

			Aggregation.execAggregation(space.getAllLsa(), space, opManager);
			log.finer("Aggregation Eco-law executed");

			Propagation.execPropagation(space.getAllLsa(), space, opManager, networkDeliveryManager);
			log.finer("Propagation Eco-law executed");

			log.finest("Stopping EcoLawsManager...");

			synchronized (opManager) {
				opManager.notify();
			}
		}
	}
}
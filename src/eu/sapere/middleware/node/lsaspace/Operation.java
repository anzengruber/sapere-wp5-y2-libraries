/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace;

import java.io.Serializable;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class Operation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5118578211958268938L;
	private Lsa lsa = null;
	private Id lsaId = null;
	private OperationType opType = null;
	private String requestingId = null;
	
	private Agent requestingAgent = null;
	
	

	
	/**
	 * @return the lsa
	 */
	public Lsa getLsa() {
		return lsa;
	}

	/**
	 * @param lsa the lsa to set
	 */
	public void setLsa(Lsa lsa) {
		this.lsa = lsa;
	}

	/**
	 * @return the lsaId
	 */
	public Id getLsaId() {
		return lsaId;
	}

	/**
	 * @param lsaId the lsaId to set
	 */
	public void setLsaId(Id lsaId) {
		this.lsaId = lsaId;
		this.lsa.setId(lsaId);
	}

	/**
	 * @return the opType
	 */
	public OperationType getOpType() {
		return opType;
	}

	/**
	 * @param opType the opType to set
	 */
	public void setOpType(OperationType opType) {
		this.opType = opType;
	}

	/**
	 * @return the requestingId
	 */
	public String getRequestingId() {
		return requestingId;
	}
	
	public Agent getRequestingAgent(){
		return requestingAgent;
	}

	/**
	 * @param requestingId the requestingId to set
	 */
	public void setRequestingId(String requestingId) {
		this.requestingId = requestingId;
	}

	public Operation injectOperation(Lsa lsa, String requestingId, Agent requestingAgent){
		
		this.opType = OperationType.Inject;
		this.lsaId = null; // To be decided where should be fixed
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		this.requestingAgent = requestingAgent;
		
		return this;
		}
	
	public Operation updateOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.Update;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation updateBondOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.BondUpdate;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation updateParametrizedOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.UpdateParametrized;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation updateBondParametrizedOperation(Lsa lsa, Id lsaId, String requestingId){
		this.opType = OperationType.BondUpdateParametrized;
		this.lsaId = new Id(lsaId.toString());
		this.lsa = lsa.getCopy();
		this.requestingId = new String(requestingId);
		
		return this;
	}
	
	public Operation removeOperation(Id lsaId, String requestingId){
		
		this.opType = OperationType.Remove;
		this.lsaId = new Id(lsaId.toString());
		this.requestingId = new String(requestingId);

		return this;
	}
	
	public Operation readOperation(Id lsaId, String requestingId, Agent requestingAgent){
		
		this.opType = OperationType.Read;
		this.lsaId = new Id(lsaId.toString());
		this.requestingId = new String(requestingId);
		this.requestingAgent = requestingAgent;
		
		return this;
	}
	
	
	
	public String toString(){
		String res = "operation[type="+opType;
		if(this.lsaId != null)
			res+=",lsaId="+this.lsaId;
		if(lsa != null)
			res+=",lsa="+lsa;
		if (requestingId != null)
			res+=",requestedBy="+requestingId;
		return res+="]";
	}
	
	

}

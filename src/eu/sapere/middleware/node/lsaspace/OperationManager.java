/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.lsaspace;

import java.util.Date;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.node.notifier.Subscription;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.DecayEvent;
import eu.sapere.middleware.node.notifier.event.LsaExpiredEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.ReadEvent;
import eu.sapere.middleware.node.notifier.filter.BondAddedFilter;
import eu.sapere.middleware.node.notifier.filter.BondRemovedFilter;
import eu.sapere.middleware.node.notifier.filter.DecayFilter;
import eu.sapere.middleware.node.notifier.filter.LsaExpiredFilter;
import eu.sapere.middleware.node.notifier.filter.PropagationFilter;
import eu.sapere.middleware.node.notifier.filter.ReadFilter;
import eu.sapere.middleware.util.SystemConfiguration;





/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class OperationManager implements Runnable{
	
	private Queue<Operation> operationsQueue = null; // FIFO Queues operation requested by SapereAgents
	
	private Space space = null;
	private Notifier notifier = null;
	private EcoLawsEngine ecoLawsEngine = null;
	private String spaceName = null;
		
	private long opTime = 400;
	private long sleepTime = 1;
	
	Logger log = null;
	
	public String getSpaceName(){
		return spaceName;
	}
 	
	public OperationManager(Space space, Notifier notifier, String spaceName){
		log = SystemConfiguration.getInstance().getLog();
		
		this.spaceName = spaceName;
		this.operationsQueue = new ConcurrentLinkedQueue<Operation>(); // This FIFO queue is synchronized AND NON BLOCKING
		this.space = space;
		this.notifier = notifier;
		
	}

	public OperationManager(Space space, Notifier notifier, String spaceName, long opTime, long sleepTime){
		log = SystemConfiguration.getInstance().getLog();
		
		this.spaceName = spaceName;
		this.operationsQueue = new ConcurrentLinkedQueue<Operation>(); // This FIFO queue is synchronized AND NON BLOCKING
		this.space = space;
		this.notifier = notifier;
		this.opTime = opTime;
		this.sleepTime = sleepTime;		
	}
	
	public void setEcoLawsEngine(EcoLawsEngine ecoLawsEngine){
		this.ecoLawsEngine = ecoLawsEngine;
		}
	
	public Id execOperation (Operation operation){
		Id  id = null;
		
		if (operation.getOpType() == OperationType.Inject){
			id = space.getFreshId();
			operation.setLsaId(id);
			
			if( operation.getRequestingAgent() != null){
				BondAddedEvent event1 = new BondAddedEvent(null, null, null);
				BondAddedFilter filter1 = new BondAddedFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s1 = new Subscription(event1.getClass(), filter1, operation.getRequestingAgent(),operation.getRequestingId());
				notifier.subscribe(s1);}
			
			if (operation.getRequestingAgent() != null){
			BondRemovedEvent event3 = new BondRemovedEvent(null, null);
			BondRemovedFilter filter3 = new BondRemovedFilter(operation.getLsaId(), operation.getRequestingId());
			Subscription s3 = new Subscription(event3.getClass(), filter3, operation.getRequestingAgent(),operation.getRequestingId());
			notifier.subscribe(s3);}
			
			if (operation.getLsa().hasDecayProperty() && operation.getRequestingAgent() != null){
				LsaExpiredEvent event2 = new LsaExpiredEvent(null);
				LsaExpiredFilter filter2 = new LsaExpiredFilter(operation.getLsaId(), operation.getRequestingId());
				
				Subscription s2 = new Subscription(event2.getClass(), filter2, operation.getRequestingAgent(),operation.getRequestingId());
				notifier.subscribe(s2);}
			
			}
		this.execOp(operation);
		
		return id;
	}
	
	public Id queueOperation(Operation operation){
		Id  id = null;
		
		if (operation.getOpType() == OperationType.Inject){
			id = space.getFreshId();
			operation.setLsaId(id);
			
			if(operation.getLsa().hasDecayProperty()){
				
				DecayEvent event = new DecayEvent(null);
				DecayFilter filter = new DecayFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s = new Subscription(event.getClass(), filter, operation.getRequestingAgent(),operation.getRequestingId());
				notifier.subscribe(s);
			
				LsaExpiredEvent event1 = new LsaExpiredEvent(null);
				LsaExpiredFilter filter1 = new LsaExpiredFilter(operation.getLsaId(), operation.getRequestingId());
				Subscription s1 = new Subscription(event1.getClass(), filter1, operation.getRequestingAgent(),operation.getRequestingId());
				notifier.subscribe(s1);
			
			}
		
			BondAddedEvent event1 = new BondAddedEvent(null, null, null);
			BondAddedFilter filter1 = new BondAddedFilter(operation.getLsaId(), operation.getRequestingId());
			Subscription s1 = new Subscription(event1.getClass(), filter1, operation.getRequestingAgent(),operation.getRequestingId());
			notifier.subscribe(s1);
			
			BondRemovedEvent event3 = new BondRemovedEvent(null, null);
			BondRemovedFilter filter3 = new BondRemovedFilter(operation.getLsaId(), operation.getRequestingId());
			Subscription s3 = new Subscription(event3.getClass(), filter3, operation.getRequestingAgent(),operation.getRequestingId());
			notifier.subscribe(s3);
			
			//Subscription to PropagationEvent
			//System.out.println("PropagationEvent subscription");
			PropagationEvent event11 = new PropagationEvent(null);
			PropagationFilter filter11 = new PropagationFilter(operation.getLsaId());
			Subscription s11 = new Subscription(event11.getClass(), filter11, operation.getRequestingAgent(), operation.getRequestingId() );
			notifier.subscribe(s11);
		}

		// Adds the operation to the queue
		operationsQueue.add(operation);

		
		return id;
	}

	public void run(){
		while(true){
			log.finer("Starting Operation Manager... ");
			long startTime = new Date().getTime();

			do{
				execNextOp();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}	while(! (new Date().getTime()-startTime > opTime));
			
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			log.finer("Stopping Operation Manager... ");

			// Notify the EcoLaws Engine
			synchronized(ecoLawsEngine) {
				ecoLawsEngine.notify();
		    }
			
			synchronized(this) {
				try {
					wait(2000);
				} catch(InterruptedException e) {
					throw new RuntimeException(e);
				}
	    	}
		}// end while(true)
	}
	
//	public void run(){
//		while(true){
//			log.finer("Starting Operation Manager... ");
//			long startTime = new Date().getTime();
//			
//			do{
//				execNextOp();
//					
//				try {
//					Thread.sleep(1);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			} while(! (new Date().getTime()-startTime > opTime));
//			
//			try {
//				Thread.sleep(sleepTime);
//			} catch (InterruptedException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//			
//			log.finer("Stopping Operation Manager... ");
//
//			// Notify the EcoLaws Engine
//			synchronized(ecoLawsEngine) {
//				ecoLawsEngine.notify();
//			}
//			
//			synchronized(this) {
//				try {
//					wait();
//				} catch(InterruptedException e) {
//					throw new RuntimeException(e);
//				}
//			}
//		}
//	}
	
	private void execOp(Operation nextOp){
		
		log.fine(nextOp.toString());
		
		if (nextOp.getOpType() == OperationType.Inject){
			space.inject(nextOp.getLsa(), nextOp.getRequestingId());
		}
		if (nextOp.getOpType() == OperationType.Remove)
			space.remove (nextOp.getLsaId(), nextOp.getRequestingId());
		if (nextOp.getOpType() == OperationType.Read){
			
			ReadEvent event = new ReadEvent(null);
			ReadFilter filter = new ReadFilter(nextOp.getLsaId(), nextOp.getRequestingId());
			Subscription s = new Subscription(event.getClass(), filter, nextOp.getRequestingAgent(), nextOp.getRequestingId());
			
			notifier.subscribe(s);
			
			space.read (nextOp.getLsaId(), nextOp.getRequestingId()); // N:B: Qui ci vuole un ritorno
			
			notifier.unsubscribe(s);
		}
		
		if(nextOp.getOpType() == OperationType.Update)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId());
		if(nextOp.getOpType() == OperationType.BondUpdate)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId(), true, false);
		if(nextOp.getOpType() == OperationType.UpdateParametrized)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId(), false, true); 
		if(nextOp.getOpType() == OperationType.BondUpdateParametrized)
			space.update(nextOp.getLsaId(), nextOp.getLsa(), nextOp.getRequestingId(), false, false);
		
	}
	
	private boolean execNextOp(){
		Operation nextOp = null;
		boolean ret = false;
		
			
			Iterator<Operation> iterator = operationsQueue.iterator();
			
			if (iterator.hasNext()){
				nextOp = (Operation) iterator.next();
				operationsQueue.poll();
				execOp(nextOp);
				ret = true;
			}
			else
				return false;
	
			return ret;
	}
}

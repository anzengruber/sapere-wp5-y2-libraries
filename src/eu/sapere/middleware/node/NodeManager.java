/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.node;

import eu.sapere.middleware.agent.RemoteConnectionManager;
import visualization.MyProperties;
import visualization.PresentationVars;
import eu.sapere.middleware.node.console.SpaceConsole;
import eu.sapere.middleware.node.console.SpaceConsoleImpl;
import eu.sapere.middleware.node.lsaspace.EcoLawsEngine;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.lsaspace.Space;
import eu.sapere.middleware.node.networking.delivery.INetworkDeliveryManager;
import eu.sapere.middleware.node.networking.delivery.NetworkDeliveryManager;
import eu.sapere.middleware.node.networking.receiver.INetworkReceiverManager;
import eu.sapere.middleware.node.networking.receiver.NetworkReceiverManager;
import eu.sapere.middleware.node.networking.topology.NetworkTopologyManager;
import eu.sapere.middleware.node.notifier.INotifier;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.util.SystemConfiguration;

/**
 * Initializes the Local Sapere Node
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class NodeManager {

	/** The Operation Manager */
	private OperationManager operationManager = null;

	/** The eco-laws engine */
	private EcoLawsEngine ecoLawsEngine = null;

	/** The Network Delivery Manager */
	private INetworkDeliveryManager networkDeliveryManager = null;

	/** The Network Receiver Manager */
	@SuppressWarnings("unused")
	private INetworkReceiverManager networkReceiverManager = null;

	/** The local LSA space */
	private Space space = null;

	/** The local Notifier */
	private Notifier notifier = null;

	/** The name of the local Node */
	private static String spaceName = null;

	/** The Console of the LSA space */
	private SpaceConsole console = null;

	/**
	 * This is a reference to the one and only instance of this singleton
	 * object.
	 */
	static private NodeManager singleton = null;

	/**
	 * Returns the name of this Node
	 * 
	 * @return The Name of this Node
	 */
	public static String getSpaceName() {
		return spaceName;
	}

	/**
	 * This is the only way to access the singleton instance.
	 * 
	 * @return A reference to the singleton.
	 */
	// Provides well-known access point to singleton EventService
	public static synchronized NodeManager instance() {
		if (singleton == null)
			singleton = new NodeManager();
		return singleton;
	}
	
	/**
	 * Starts the Node Manager
	 */
	// Prevents direct instantiation of the event service
	private NodeManager() {
		spaceName = MyProperties.instance().getProperty("name");
		
		SystemConfiguration.getInstance();
		
//		// Set Node Name
//		if (SystemConfiguration.getInstance().props != null)
//			spaceName = SystemConfiguration.getInstance().props.getProperty("nodeName");
//		else
//			spaceName = "Node" + Math.random();
		
		this.notifier = Notifier.instance();
		this.console = new SpaceConsoleImpl();
		console.startConsole(spaceName);

		space = new Space(spaceName, notifier, console);
		this.networkDeliveryManager = new NetworkDeliveryManager();
		this.networkReceiverManager = new NetworkReceiverManager();

		if (SystemConfiguration.getInstance().props != null)
			operationManager = new OperationManager(space, notifier, spaceName, 
				new Long(SystemConfiguration.getInstance().props.getProperty("opTime")).longValue(),
				new Long (SystemConfiguration.getInstance().props.getProperty("sleepTime")).longValue());
		else
			operationManager = new OperationManager(space, notifier, spaceName);
		
		ecoLawsEngine = new EcoLawsEngine(space);

		operationManager.setEcoLawsEngine(ecoLawsEngine);
		ecoLawsEngine.setOperationManager(operationManager);
		ecoLawsEngine.setNetworkDeliveryManager(networkDeliveryManager);

		new Thread(operationManager).start();
		new Thread(ecoLawsEngine).start();

		new NetworkTopologyManager(this);

		PresentationVars.instance().set(PresentationVars.SAPERE_GUI, this);
		
		@SuppressWarnings("unused")
		RemoteConnectionManager remoteMng = new RemoteConnectionManager();
	}

	/**
	 * Retrieves the local Operation Manager
	 * 
	 * @return the local Operation Manager
	 */
	public OperationManager getOperationManager() {
		return operationManager;
	}

	/**
	 * Retireves the local Notifier
	 * 
	 * @return the local Notifier
	 */
	public Notifier getNotifier() {
		return notifier;
	}

	public Space getSpace() {
		return space;
	}
}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.notifier.event;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.interfaces.ILsa;


/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public class BondAddedEvent extends AbstractSapereEvent{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 899040460408932860L;
	private Lsa lsa = null;
	private Lsa bondedLsa = null;
	private String bondId = null; // the the Id of the LSa to which I have just been bonded

	public BondAddedEvent(Lsa lsa, String bondId, Lsa bondedLsa){
		this.lsa = lsa;
		this.bondId = bondId;
		this.bondedLsa = bondedLsa;
	}
	
	public ILsa getLsa(){
		return lsa;
	}
	
	public String getBondId(){
		return bondId;
	}

	public ILsa getBondedLsa() {
		return bondedLsa;
	}

	public void setBondedLsa(Lsa bondedLsa) {
		this.bondedLsa = bondedLsa;
	}


}

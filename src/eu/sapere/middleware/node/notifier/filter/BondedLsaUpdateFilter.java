/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

/**
 * 
 */
package eu.sapere.middleware.node.notifier.filter;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.IEvent;



/**
 * @author Gabriella Castelli (UNIMORE)
 *
 */


public class BondedLsaUpdateFilter implements Filter{
	
	private Id targetLsaId = null;
	public Id getTargetLsaId() {
		return targetLsaId;
	}
	
	public void setTargetLsaId(Id targetLsaId) {
		this.targetLsaId = targetLsaId;
	}


	public String getRequestingId() {
		return requestingId;
	}


	public void setRequestingId(String requestingId) {
		this.requestingId = requestingId;
	}


	private String requestingId = null;
	
	public BondedLsaUpdateFilter(Id lsaId, String requestingId){
		this.targetLsaId = lsaId; // changedId
		this.requestingId = requestingId; //requesting Id
	}
	
	public boolean apply(IEvent event, String agentName) {
		
		boolean ret = false;
		
		BondedLsaUpdateEvent bondedLsaUpdateEvent = (BondedLsaUpdateEvent) event;
		
		 if (((Lsa)bondedLsaUpdateEvent.getLsa()).getId().toString().equals(targetLsaId.toString()) )
				 ret = true;
		
		return ret;
	}


	public boolean apply(IEvent event) {
		// TODO Auto-generated method stub
		return false;
	}


}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.autoupdate.PropertyValueListener;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.lsaspace.Operation;

/**
 * The abstract class that realize an agent that manages an LSA. The Agent
 * is represented by an implicit LSA, each operation on the LSA is automatically
 * and transparently propagated to the LSA space. 
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public abstract class SapereAgentRNM extends SapereAgent implements PropertyValueListener{

	private boolean proxyModeOn = false; 

	private ProxySapereAgent proxy = null;
	
	private String ip = null;
	
	public SapereAgentRNM(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public void setIp(String ip){
		this.ip = ip;
	}
	
	public String getNodeName(){
		return opMng.getSpaceName();
	}
	
	@Override
	//This is the entry for the operations to be exectued
		protected void submitOperation (){
		
		//System.out.println("\n* submit op:");
			
			if (lsa.getProperty(PropertyName.ACTIVE_PROPAGATION.toString()) != null){
				
				//System.out.println("\n*** requires ACTIVE_PROPAGATION");
				//System.out.println(this.lsa);
				
				if(this.proxyModeOn == false){
					this.proxyModeOn = true;
					
					setIp(lsa.getProperty(PropertyName.ACTIVE_PROPAGATION.toString()).getValue().elementAt(0));
			
					proxy = new ProxySapereAgent(this.getAgentName()+"Proxy", ip, this);
	
				}
				
				Lsa remoteCopy = ((Lsa)lsa).getCopy();
				remoteCopy.removeProperty(PropertyName.ACTIVE_PROPAGATION.toString());
				
				//System.out.println(remoteCopy);
				
				proxy.setLsa((Lsa)remoteCopy);
				proxy.forward();
				
				if (lsaId != null)
					removeLsa(); // remove the LSA from the local node
				
						}
			else{
				
				//System.out.println("\n*** DO NOT requires ACTIVE_PROPAGATION");
				//System.out.println("%%% "+lsa);
				
				if (this.proxyModeOn == true){
					proxy.closeConnection();
					this.proxyModeOn = false;
					// rimuovere la copia remota 
					lsaId = injectOperation(); //inject so synthetic properties are overwritten
				}
				
				else{
			if(lsaId == null )
				lsaId = injectOperation();
			else
				updateOperation();
				}	
			
				
			}
		}
	
	
	@Override
	public void removeLsa(){
		//System.out.println("Op di remove lsa");
		Operation op = new Operation().removeOperation(lsaId, getAgentName());
	//	this.lsa = null;
		// rimuovere le notifiche per gli eventi
		opMng.queueOperation(op);
	}
	

}

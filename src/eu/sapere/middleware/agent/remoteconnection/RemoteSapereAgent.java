/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.remoteconnection;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.lsaspace.Agent;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.notifier.INotifier;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.node.notifier.Subscription;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.IEvent;
import eu.sapere.middleware.node.notifier.filter.BondedLsaUpdateFilter;
import eu.sapere.middleware.node.notifier.filter.Filter;


/**
 * The Remote Sapere Agent is in charge of managing local operations for an LSA
 * that has been actively propagated to the local node. Local events are forwarded
 * to the remote Agent. 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public  class RemoteSapereAgent extends Agent{
		
	/** The LSA managed */
	private ILsa lsa = null;
	
	/** The id of the LSA managed */
	protected Id lsaId = null;
	
	/** The reference to the local Operation Manager */
	private OperationManager opMng = null;
	
	/** The reference to the local Notifier */
	protected INotifier notifier = null;

	/** The associated thread server*/
	private ThreadServer thread = null;
	
	/** 
	 * Instantiates the Remote Sapere Agent
	 * 
	 * @param agentName The name of the Agent
	 * @param OpMng The reference to the local Operation Manager
	 * @param notifier The reference to the local Notifier
	 * @param thread The associated thread server
	 */
	public RemoteSapereAgent(String agentName, ThreadServer thread){
		
		super(agentName);
		this.opMng = NodeManager.instance().getOperationManager();
		this.notifier = Notifier.instance();
		this.thread = thread;
		this.lsa = new Lsa();
		
	}
	
	/**
	 * Sets the Agent LSA
	 * @param lsa The LSA managed by the Agent
	 */
	public void setLsa(Lsa lsa){
		this.lsa = lsa;
	}


	/**
	 * Injects the managed LSA in the local LSA space
	 * @return the id of the injected LSA
	 */
	protected Id injectOperation (){
		Id lsaId = null;

		Operation op = new Operation().injectOperation((Lsa)this.lsa, getAgentName(),  this);
		
			lsaId = opMng.queueOperation(op);
		//	proxy.sendString("Injected with LsaId "+lsaId);
			this.lsaId =lsaId;
		return lsaId;
	}

	/**
	 * Updates the managed LSA in the local LSA space
	 * @return
	 */
	protected void updateOperation (){
		
		Operation op = new Operation().updateOperation((Lsa)this.lsa, lsaId, getAgentName());

			opMng.queueOperation(op);
	}	
	


	final public void onNotification(IEvent event){
			
		if (event.getClass().isAssignableFrom(BondAddedEvent.class)){
			BondAddedEvent bondAddedEvent = (BondAddedEvent) event;
			 onBondAddedNotification(bondAddedEvent);
		}
		if (event.getClass().isAssignableFrom(BondedLsaUpdateEvent.class)){
			BondedLsaUpdateEvent bondedLsaUpdateEvent = (BondedLsaUpdateEvent) event;
			 onBondedLsaUpdateEventNotification(bondedLsaUpdateEvent);
		}
		if (event.getClass().isAssignableFrom(BondRemovedEvent.class)){
			BondRemovedEvent bondRemovedEvent = (BondRemovedEvent) event;
			 onBondRemovedNotification(bondRemovedEvent);
		}		
		
	}

	  
	  public void onBondAddedNotification(BondAddedEvent event) {
  		  		 
		  String checkId = event.getBondId();
		  
		    String tentativo = event.getBondId().substring(0, event.getBondId().lastIndexOf("#"));
		    
		    if (tentativo.contains("#"))
		    	checkId = tentativo;
		   
		    BondedLsaUpdateEvent bondedLsaUpdateEvent = new BondedLsaUpdateEvent(null);
			Filter filter = new BondedLsaUpdateFilter(new Id( checkId), this.getAgentName());
			Subscription s = new Subscription(bondedLsaUpdateEvent.getClass(), filter, this, this.getAgentName());

			notifier.subscribe(s); // 
			thread.forwardEvent(event);
			 
	  }
	  
	  public void onBondRemovedNotification(BondRemovedEvent event) {
	  		 
		    thread.forwardEvent(event);
		    }
	  
	  public void onBondedLsaUpdateEventNotification (BondedLsaUpdateEvent event){
		  
				thread.forwardEvent(event);
	  }
	  	
}

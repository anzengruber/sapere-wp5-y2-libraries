/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent.remoteconnection.internal;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.agent.ProxySapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;


public class ResolveIpAddress extends SapereAgent{
	
	private ProxySapereAgent proxy = null;

	public ResolveIpAddress(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	
	public ResolveIpAddress(String name, ProxySapereAgent proxy) {
		super(name);
		this.proxy = proxy;
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		String destinationNode = null;
	    destinationNode = event.getBondedLsa().getProperty("ipAddress").getValue().elementAt(0);
	    this.removeLsa();
	    proxy.setIp(destinationNode);
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInitialLSA() {
		// TODO Auto-generated method stub
		
	}
	
	public void setInitialLSA(String nodeName) {
		addProperty(new Property("neighbour", nodeName), new Property("ipAddress", "?"));
		
	}
}

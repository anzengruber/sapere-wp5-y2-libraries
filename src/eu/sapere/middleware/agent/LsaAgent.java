/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package eu.sapere.middleware.agent;

import eu.sapere.middleware.lsa.Id;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.lsaspace.Agent;
import eu.sapere.middleware.node.lsaspace.Operation;
import eu.sapere.middleware.node.lsaspace.OperationManager;
import eu.sapere.middleware.node.notifier.INotifier;
import eu.sapere.middleware.node.notifier.Notifier;
import eu.sapere.middleware.node.notifier.Subscription;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.IEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.filter.BondedLsaUpdateFilter;

/**
 * Internal class that implements actions for Agents that manages LSA
 * 
 * @author Gabriella Castelli (UNIMORE)
 *
 */
public abstract class LsaAgent extends Agent implements ISapereAgent{
	
	/** The managed LSA */
	protected ILsa lsa = null;
	
	/** The id of the maanged LSA */
	protected Id lsaId = null;
	
	/** The OperationManager of the local SAPERE node */
	protected OperationManager opMng = null;
	
	/** The Notifier of the local SAPERE node */
	protected INotifier notifier = null;
	
	/**
	    * Instantiates this LsaAgent
	    * @param agentName The name of this Agent
	    */
	   public LsaAgent(String agentName, OperationManager an_opMng, Notifier notifier) {
	      super(agentName);

	      this.lsa = new Lsa();
	      this.opMng = an_opMng;
	      this.notifier = notifier;
	   }
	   
	/**
	 * Instantiates this LsaAgent
	 * 
	 * @param agentName The name of this Agent
	 */
	public LsaAgent(String agentName){
		super(agentName);
		
		this.lsa = new Lsa();
		this.opMng = NodeManager.instance().getOperationManager();
		this.notifier = Notifier.instance();
	}
	
	/*
	private void submitOperation(Operation op){
		if(op.getOpType() == OperationType.Inject)
			injectOperation(op);
		if(op.getOpType() == OperationType.Update)
			updateOperation(op);
	}*/
	
	/**
	 * Removes the managed LSA from the local LSA space
	 */
	public void removeLsa(){
		this.lsa = null;
		Operation op = new Operation().removeOperation(lsaId, getAgentName());
		opMng.queueOperation(op);
	}
	
	
	protected void submitOperation (){
		if(lsaId == null )
			lsaId = injectOperation();
		else
			updateOperation();
	}

	
	protected Id injectOperation (){
		Id lsaId = null;

		Operation op = new Operation().injectOperation((Lsa)this.lsa, getAgentName(),  this);
		
			lsaId = opMng.queueOperation(op);
			return lsaId;
	}
	
	/*
	private Id injectOperation(Operation op){
		lsaId = opMng.queueOperation(op);
		return lsaId;
	}*/


	protected void updateOperation (){
		// recupero lsaId gi� stato settato
		Operation op = new Operation().updateOperation((Lsa)this.lsa, lsaId, getAgentName());
		
		opMng.queueOperation(op);
	}	
	
	/*
	private void updateOperation(Operation op){
		opMng.queueOperation(op);
	}*/
	
	@Override
	public void onNotification(IEvent event){
		
	/*	if (event.getClass().isAssignableFrom(ReadEvent.class)){
			 ReadEvent readEvent = (ReadEvent) event;
			 onReadNotification(readEvent);
		}
		if (event.getClass().isAssignableFrom(UpdateEvent.class)){
			 UpdateEvent updateEvent = (UpdateEvent) event;
			 onUpdateNotification(updateEvent);
		}
		if (event.getClass().isAssignableFrom(LsaExpiredEvent.class)){
			LsaExpiredEvent lsaExpiredEvent = (LsaExpiredEvent) event;
			 onLsaExpiredNotification(lsaExpiredEvent);
		}		*/
		if (event.getClass().isAssignableFrom(BondAddedEvent.class)){
			BondAddedEvent bondAddedEvent = (BondAddedEvent) event;
			
		    BondedLsaUpdateEvent bondedLsaUpdateEvent = new BondedLsaUpdateEvent(null);
			BondedLsaUpdateFilter filter = new BondedLsaUpdateFilter(new Id( bondAddedEvent.getBondId()), lsaId.toString());
			Subscription s = new Subscription(bondedLsaUpdateEvent.getClass(), filter, this, this.getAgentName());

			notifier.subscribe(s); // sistemare
			
			onBondAddedNotification(bondAddedEvent);
		}
		if (event.getClass().isAssignableFrom(BondedLsaUpdateEvent.class)){
			BondedLsaUpdateEvent bondedLsaUpdateEvent = (BondedLsaUpdateEvent) event;
			 onBondedLsaUpdateEventNotification(bondedLsaUpdateEvent);
		}
		if (event.getClass().isAssignableFrom(BondRemovedEvent.class)){
			BondRemovedEvent bondRemovedEvent = (BondRemovedEvent) event;
			 onBondRemovedNotification(bondRemovedEvent);
		
			 BondedLsaUpdateFilter filter = new BondedLsaUpdateFilter(new Id(bondRemovedEvent.getBondId()), lsaId.toString());
			 notifier.unsubscribe(filter);
		}
	/*	if (event.getClass().isAssignableFrom(DecayEvent.class)){
			DecayEvent decayEvent = (DecayEvent) event;
			 onDecayNotification(decayEvent);
		}
		if (event.getClass().isAssignableFrom(AggregationRemovedEvent.class)){
			AggregationRemovedEvent aggregationRemovedEvent = (AggregationRemovedEvent) event;
			 onAggregationRemovedNotification(aggregationRemovedEvent);
		}*/
		
		if(event.getClass().isAssignableFrom(PropagationEvent.class)){
			PropagationEvent propagationEvent = (PropagationEvent) event;
			onPropagationEvent(propagationEvent);
		}
	}
	
	
	@Override
	public String toString(){
		return lsa.toString();
	}

}

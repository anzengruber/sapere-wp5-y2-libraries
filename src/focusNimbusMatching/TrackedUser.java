/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package focusNimbusMatching;

import spatialAnalyzer.CompoundStaticState;
import spatialAnalyzer.SpatiotemporalState;

public class TrackedUser {
	
	private int id;
	private CompoundStaticState staticState;
	private SpatiotemporalState spTempState;
	private int attentionLevel;
	
	
	public TrackedUser(int i, CompoundStaticState cSstate, SpatiotemporalState sTstate) {
		id = i;
		staticState = cSstate;
		spTempState = sTstate;
		attentionLevel = 0;
	}

	private void updateAttentionLevel() {
		attentionLevel = (int)(100 - (Math.abs(180 - staticState.getOrientation()*22.5))/1.8);
	}
	
//	----------Getters Setters---------------------------------------
	
	public CompoundStaticState getStaticState() {
		return staticState;
	}
	public void setStaticState(CompoundStaticState staticState) {
		this.staticState = staticState;
	}
	public SpatiotemporalState getSpTempState() {
		return spTempState;
	}
	public void setSpTempState(SpatiotemporalState spTempState) {
		this.spTempState = spTempState;
	}

	public int getAttention() {
		updateAttentionLevel();
		return attentionLevel;
	}
	
	public int getID() {
		return id;
	}
	
//	-----------------------------------------------------------------
	
	public String toString() {
		String staticString; String spString;
		
		if(staticState==null) staticString="null";
		else staticString=staticState.toString();
		
		if(spTempState==null) spString="null";
		else spString=spTempState.toString();
			
		return id+": "+staticString+"; "+spString;
	}
}

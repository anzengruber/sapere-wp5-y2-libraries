/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package focusNimbusMatching;

import java.util.HashMap;
import java.util.Map;

import spatialAnalyzer.CompoundStaticState;
import spatialAnalyzer.SpatiotemporalState;

public class FNMatcher {

	private Map<Integer, TrackedUser> trackedUsers;
	
	public FNMatcher() {
		trackedUsers = new HashMap<Integer, TrackedUser>();
	}
	
	public void updateUser(int i, CompoundStaticState state) {
		TrackedUser user = trackedUsers.get(i);
		if(user!=null) user.setStaticState(state);
		else {
			user = new TrackedUser(i, state, null);
			trackedUsers.put(i, user);
		}
	}
	
	public void updateUser(int i, SpatiotemporalState state) {
		TrackedUser user = trackedUsers.get(i);
		if(user!=null) user.setSpTempState(state);
		else {
			user = new TrackedUser(i, null, state);
			trackedUsers.put(i, user);
		}
	}

	public TrackedUser getUser(int id) {
		return trackedUsers.get(id);
	}
}

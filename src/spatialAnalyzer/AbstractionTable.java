/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

import java.util.HashMap;
import java.util.Map;

public class AbstractionTable {
	
	private static AbstractionTable instance;
	private Map<Integer, String> distanceAbstractions = new HashMap<Integer, String>();
	private Map<Integer, String> cardinalDirectionAbstractions = new HashMap<Integer, String>();
	
	private Map<Integer, String> semanticMotions = new HashMap<Integer, String>();
//	private Map<Integer, String> directionalMotions = new HashMap<Integer, String>();
	private Map<Integer, String> velocitiesOfMotion = new HashMap<Integer, String>();
	
	public static AbstractionTable instance() {
		if(instance == null)  {
			instance = new AbstractionTable();
		}
		return instance;
	}
	
	public AbstractionTable() {
		insertDistances();
		insertCardinalDirections();
		
		insertSemanticMotion();
//		insertDirectionalMotion();
		insertVelocitiesOfMotion();
	}

//	---------------------------------Static States-----------------------------------------
	
	private void insertCardinalDirections() {
		cardinalDirectionAbstractions.put(0, "N");
		cardinalDirectionAbstractions.put(1, "NNE");
		cardinalDirectionAbstractions.put(2, "NE");
		cardinalDirectionAbstractions.put(3, "ENE");
		cardinalDirectionAbstractions.put(4, "E");
		cardinalDirectionAbstractions.put(5, "ESE");
		cardinalDirectionAbstractions.put(6, "SE");
		cardinalDirectionAbstractions.put(7, "SSE");
		cardinalDirectionAbstractions.put(8, "S");
		cardinalDirectionAbstractions.put(9, "SSW");
		cardinalDirectionAbstractions.put(10, "SW");
		cardinalDirectionAbstractions.put(11, "WSW");
		cardinalDirectionAbstractions.put(12, "W");
		cardinalDirectionAbstractions.put(13, "WNW");
		cardinalDirectionAbstractions.put(14, "NW");
		cardinalDirectionAbstractions.put(15, "NNW");
	}

	private void insertDistances() {
		distanceAbstractions.put(0, "Intimate");
		distanceAbstractions.put(1, "Personal");
		distanceAbstractions.put(2, "Social");
		distanceAbstractions.put(3, "Public");
	}
	
//	---------------------------------Spatiotemporal States-----------------------------------------
	
	private void insertVelocitiesOfMotion() {
		velocitiesOfMotion.put(-1, "Error"); 			//Error
		velocitiesOfMotion.put(0, "New");				//N
		velocitiesOfMotion.put(1, "Slow");				//S
		velocitiesOfMotion.put(2, "Medium");			//M
		velocitiesOfMotion.put(3, "Fast");				//F
	}

//	private void insertDirectionalMotion() {
//		directionalMotions.put(-1, "Error"); 			//Error
//		directionalMotions.put(0, "New");				//N
//		directionalMotions.put(1, "Center");			//C
//		directionalMotions.put(2, "Left");				//L
//		directionalMotions.put(3, "Right");				//R
//		directionalMotions.put(4, "LeftToRight");		//LR
//		directionalMotions.put(5, "RightToLeft");		//RL
//	}

	private void insertSemanticMotion() {
		semanticMotions.put(-1, "Error"); 				//Error
		semanticMotions.put(0, "New"); 					//N
		semanticMotions.put(1, "ClosingIn"); 			//CI
		semanticMotions.put(2, "StoppedClosingIn"); 	//SCI
		semanticMotions.put(3, "MovingAway"); 			//MA
		semanticMotions.put(4, "StoppedMovingAway"); 	//SMA
		semanticMotions.put(5, "CrossingPath"); 		//CP
		semanticMotions.put(6, "StoppedCrossingPath"); 	//SCP
		semanticMotions.put(7, "PassedBy"); 			//PB
		semanticMotions.put(8, "Overtook"); 			//OT
		semanticMotions.put(9, "TurnedTowards"); 		//TT
		semanticMotions.put(10, "TurnedAway"); 			//TA
		semanticMotions.put(11, "Still"); 				//S
		semanticMotions.put(12, "Gone"); 				//G
	}

//	---------------------------------Getters-------------------------------------------------
	
	public String getQualDirection(int key) {
		return cardinalDirectionAbstractions.get(key);
	}
	
	public String getQualDistance(int key) {
		return distanceAbstractions.get(key);
	}

	public String getSemanticMotion(int key) {
		return semanticMotions.get(key);
	}
	
//	public String getDirectionalMotion(int key) {
//		return directionalMotions.get(key);
//	}
	
	public String getVelocityOfMotion(int key) {
		return velocitiesOfMotion.get(key);
	}
	
}

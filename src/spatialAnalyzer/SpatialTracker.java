/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JFrame;

import org.OpenNI.StatusException;


public class SpatialTracker extends Thread {

    /**
	 * 
	 */
	public UserTracker viewer;
	private StaticAbstractionEngine staticAbstractionEngine;
	private SpatiotemporalAbstractionEngine spatiotemporalAbstractionEngine;
	
	private boolean shouldRun = true;
	private float displayOrientation;
	
	private JFrame frame;
	private Vector<SpatialListener> listeners;
	
	private Map<Integer, Float> orientations;
	private Map<Integer, Float> distances;
	private Map<Integer, Float> directions;

    private Map<Integer, Integer> qual_orientations;
    private Map<Integer, Integer> qual_distances;
    private Map<Integer, Integer> qual_directions;
    
    public SpatialTracker(boolean visible)
    {
//    	frame = new JFrame("OpenNI User Tracker");
//    	frame.addWindowListener(new WindowAdapter() {
//            public void windowClosing(WindowEvent e) {System.exit(0);}
//        });
//        
//    	frame.addKeyListener(new KeyListener()
//		{
//			@Override
//			public void keyTyped(KeyEvent arg0) {}
//			@Override
//			public void keyReleased(KeyEvent arg0) {}
//			@Override
//			public void keyPressed(KeyEvent arg0) {
//				if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE)
//				{
//					shouldRun = false;
//				}
//			}
//		});
//    	
//    	displayOrientation = 0;
    	listeners = new Vector<SpatialListener>();
    	viewer = new UserTracker(visible);
    	staticAbstractionEngine = new StaticAbstractionEngine();
    	spatiotemporalAbstractionEngine = new SpatiotemporalAbstractionEngine();
//    	frame.add("Center", viewer);
//    	frame.pack();
//    	frame.setVisible(visible);
    	
    	start();
    }

    public void updateDisplayOrientation(float or) {
    	displayOrientation = or;
    }
    
    public void run() {
        while(shouldRun) {
        	viewer.update();
            viewer.repaint();
            
			try {
				orientations = viewer.computeOrientation();
				distances = viewer.computeDistance();
				directions = viewer.computeDirection();
								
				changeFrameOfReference();
				
				qual_orientations = staticAbstractionEngine.abstractOrientation(orientations);
				qual_distances = staticAbstractionEngine.abstractDistance(distances);
				qual_directions = staticAbstractionEngine.abstractDirection(directions);
				
				Map<Integer, CompoundStaticState> compoundStaticStates = generateStaticStates(qual_orientations,qual_distances,qual_directions);
				
				Map<Integer, SpatiotemporalState> spatioTempStates = spatiotemporalAbstractionEngine.computeCurrentStates(compoundStaticStates);
				for(SpatialListener agent:listeners) {
					agent.fireChanged(compoundStaticStates, spatioTempStates);
				}
			} catch (StatusException e1) {
				e1.printStackTrace();
			}
            
            try {
				sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }
        frame.dispose();
    }

	private void changeFrameOfReference() {
		for(int i:orientations.keySet()) {
			orientations.put(i, (orientations.get(i)+displayOrientation)%360);
		}
		for(int i:directions.keySet()) {
			directions.put(i, (directions.get(i)+displayOrientation)%360);
		}
	}

	private Map<Integer, CompoundStaticState> generateStaticStates(Map<Integer, Integer> orientations, Map<Integer, Integer> distances,
			Map<Integer, Integer> directions) {
		
		Map<Integer, CompoundStaticState> res = new HashMap<Integer, CompoundStaticState>();
		
		for(int key : orientations.keySet()) {
			Integer tmp_orientation = orientations.get(key);
			Integer tmp_distance = distances.get(key);
			Integer tmp_direction = directions.get(key);
			
			if(tmp_orientation!=null && tmp_distance!=null && tmp_direction!=null) {
				res.put(key, new CompoundStaticState(key, tmp_orientation, tmp_distance, tmp_direction));
			}
		}
		return res;
	}

	public void register(SpatialAgent spatialAgent) {
		listeners.add(spatialAgent);
		viewer.registerListener(spatialAgent);
	}
	
	
}

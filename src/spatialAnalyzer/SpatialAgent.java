/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

import java.util.Map;
import java.util.Vector;

import visualization.VisualizationListener;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import focusNimbusMatching.FNMatcher;

public class SpatialAgent extends SapereAgent implements SpatialListener, UserLostListener {

	private class LastPair {
		int id;
		CompoundStaticState state;
		
		public LastPair(int i, CompoundStaticState p) {
			id = i;
			state = p;
		}
		
		public void setID(int id) {
			this.id = id;
		}
		
		public void setState(CompoundStaticState s) {
			this.state = s;
		}
		
		public int getID() {
			return id;
		}
		
		public CompoundStaticState getState() {
			return state;
		}
	}
	
	private SpatialTracker sTracker;
	private FNMatcher fnMatcher;
	
	private Vector<String> currentContent = new Vector<String>();
	private Vector<VisualizationListener> listeners = new Vector<VisualizationListener>();
	
	private LastPair previous = null;
	private int timeoutIndex = 0;
	
	public SpatialAgent(String name, boolean visible) {
		super(name);
		
		boolean initialized = false;
		while(!initialized) {
			try{
				sTracker = new SpatialTracker(visible);
				initialized = true;
			} catch (Exception ex) {};
		}
			
		sTracker.register(this);
		fnMatcher = new FNMatcher();
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {

	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		Property orP = event.getLsa().getProperty("orientation");
		if(orP!=null)
			sTracker.updateDisplayOrientation(Float.parseFloat(orP.getValue().elementAt(0)));	
	}

	@Override
	public void fireChanged(
			Map<Integer, CompoundStaticState> compoundStaticStates,
			Map<Integer, SpatiotemporalState> states) {
		
		parseStaticStates(compoundStaticStates);
		parseSTStates(states);
	}	
	
	public void addVisualizationListener(VisualizationListener list) {
		listeners.add(list);
	}
	
	public void notifyContentChanged() {
		for (VisualizationListener list : listeners) {
			list.fireContentsChanged(this.agentName, currentContent);
		}
	}
	
	private void processSpatialContent(int direction, int distance) {
		 currentContent.clear();
		 currentContent.add(""+distance);
		 currentContent.add(""+direction);
		 notifyContentChanged();
	}
	
	private void parseSTStates(Map<Integer, SpatiotemporalState> states) {
		for(int i:states.keySet()) {
			addProperty(new Property("SpatioTState"+i, states.get(i).toString()));
			fnMatcher.updateUser(i, states.get(i));
		}
	}

	private void parseStaticStates(Map<Integer, CompoundStaticState> compoundStaticStates) {
		boolean found = false, active = false, last = false;
		int elemIndex = 0;
		for(int i:compoundStaticStates.keySet()) {
			elemIndex++;
			if(!found) {
				CompoundStaticState curr = compoundStaticStates.get(i);
				
				if(previous == null) {
					previous = new LastPair(i, curr);
					active = true;
					timeoutIndex = 0;
				} else if(i == previous.getID()) {
					CompoundStaticState prev = previous.getState();					
					if(curr.getDirection() != prev.getDirection()) active = true;
					else if(curr.getDistance() != prev.getDistance()) active = true;
					else if(curr.getOrientation() != prev.getOrientation()) active = true;
					
					if(!active) {
						if(timeoutIndex < 10) {
							timeoutIndex++;
							active = true;
						}
					}
					else timeoutIndex = 0;
				}
				
				if(elemIndex == compoundStaticStates.size()) {
					last = true;
					timeoutIndex = 0;
				}
				
				if(active || last) {
					processSpatialContent(curr.getDirection(), curr.getDistance());
					
					previous.setID(i);previous.setState(curr);
					found = true;
				}
			}
			fnMatcher.updateUser(i, compoundStaticStates.get(i));
			addProperty(new Property("AttentionLevel "+i, ""+fnMatcher.getUser(i).getAttention()));
			addProperty(new Property("StaticState"+i, compoundStaticStates.get(i).toString()));
		}
	}

	@Override
	public void notifyUserLost(int id) {
		lsa.removeProperty("StaticState"+id);
		lsa.removeProperty("SpatioTState"+id);
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property("user", this.agentName));
		addSubDescription("orientation", new Property("orientation", "*"));
		addSubDescription("content", new Property("spatial", "*"));
	}
}

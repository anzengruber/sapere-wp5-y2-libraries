/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

import java.awt.Component;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.OpenNI.CalibrationProgressEventArgs;
import org.OpenNI.CalibrationProgressStatus;
import org.OpenNI.Context;
import org.OpenNI.GeneralException;
import org.OpenNI.IObservable;
import org.OpenNI.IObserver;
import org.OpenNI.License;
import org.OpenNI.Plane3D;
import org.OpenNI.Point3D;
import org.OpenNI.PoseDetectionCapability;
import org.OpenNI.PoseDetectionEventArgs;
import org.OpenNI.SceneAnalyzer;
import org.OpenNI.SkeletonCapability;
import org.OpenNI.SkeletonJoint;
import org.OpenNI.SkeletonJointOrientation;
import org.OpenNI.SkeletonJointPosition;
import org.OpenNI.SkeletonProfile;
import org.OpenNI.StatusException;
import org.OpenNI.UserEventArgs;
import org.OpenNI.UserGenerator;

import com.jme3.math.Matrix3f;
import com.jme3.math.Vector3f;

public class UserTracker extends Component
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2712901391864335982L;
	Vector<UserLostListener> listeners;
	
	class NewUserObserver implements IObserver<UserEventArgs>
	{
		@Override
		public void update(IObservable<UserEventArgs> observable,
				UserEventArgs args)
		{
			try {
				if (skeletonCap.needPoseForCalibration())
					poseDetectionCap.startPoseDetection(calibPose, args.getId());
				else
					skeletonCap.requestSkeletonCalibration(args.getId(), true);
			
			} catch (StatusException e) {
				e.printStackTrace();
			}
		}
	}
	
	class LostUserObserver implements IObserver<UserEventArgs>
	{
		@Override
		public void update(IObservable<UserEventArgs> observable, UserEventArgs args) {
			joints.remove(args.getId());
			
			for(UserLostListener list : listeners) list.notifyUserLost(args.getId());
		}
	}
	
	class CalibrationCompleteObserver implements IObserver<CalibrationProgressEventArgs>
	{
		@Override
		public void update(IObservable<CalibrationProgressEventArgs> observable, CalibrationProgressEventArgs args) {
			try {
				if (args.getStatus() == CalibrationProgressStatus.OK) {
					skeletonCap.startTracking(args.getUser());
					joints.put(new Integer(args.getUser()), new HashMap<SkeletonJoint, SkeletonJointPosition>());
				}
				else if (args.getStatus() != CalibrationProgressStatus.MANUAL_ABORT) {
					if (skeletonCap.needPoseForCalibration()) 
						poseDetectionCap.startPoseDetection(calibPose, args.getUser());
					else
						skeletonCap.requestSkeletonCalibration(args.getUser(), true);
				}
			} catch (StatusException e) {
				e.printStackTrace();
			}
		}
	}
	
	class PoseDetectedObserver implements IObserver<PoseDetectionEventArgs>
	{
		@Override
		public void update(IObservable<PoseDetectionEventArgs> observable, PoseDetectionEventArgs args) {
			try {
				poseDetectionCap.stopPoseDetection(args.getUser());
				skeletonCap.requestSkeletonCalibration(args.getUser(), true);
			} catch (StatusException e) {
				e.printStackTrace();
			}
		}
	}
	
    private Context context;
    private SceneAnalyzer scene;
    private Plane3D floor;
    private Matrix3f rot = new Matrix3f();
//    private DepthGenerator depthGen;
    private UserGenerator userGen;
    private SkeletonCapability skeletonCap;
    private PoseDetectionCapability poseDetectionCap;
//    private byte[] imgbytes;
//    private float histogram[];
    private String calibPose = null;
    private HashMap<Integer, HashMap<SkeletonJoint, SkeletonJointPosition>> joints;

//    private boolean drawBackground = true;
//    private boolean draw = true;
//    private boolean printState = true;
//    private Color colors[] = {Color.RED, Color.BLUE, Color.CYAN, Color.GREEN, Color.MAGENTA, Color.PINK, Color.YELLOW, Color.WHITE};
//    
//    private BufferedImage bimg;
//    private int width, height;
    
    public UserTracker(boolean doPaint) {
    	int attempts = 5;
    	while(attempts > 0) {
    		try {
//	    		draw = doPaint;
	    		floor = new Plane3D(new Point3D(0,1,0), new Point3D(0,0,0));
	    		Vector3f normal = new Vector3f(floor.getNormal().getX(), floor.getNormal().getY(), floor.getNormal().getZ());
	        	rot.fromStartEndVectors(new Vector3f(0,1,0), normal);
	        	
	        	listeners = new Vector<UserLostListener>();
	        	
	        	context = new Context();
	            // add the NITE Licence 
	            License licence = new License("PrimeSense", "0KOIk2JeIBYClPWVnMoRKn5cdY4="); 
	            context.addLicense(licence);
	
	            scene = SceneAnalyzer.create(context);
//	            depthGen = DepthGenerator.create(context);
//	            DepthMetaData depthMD = depthGen.getMetaData();
//	
//	            histogram = new float[10000];
//	            width = depthMD.getFullXRes();
//	            height = depthMD.getFullYRes();
//	            
//	            imgbytes = new byte[width*height*3];
	
	            userGen = UserGenerator.create(context);
	            skeletonCap = userGen.getSkeletonCapability();
	            poseDetectionCap = userGen.getPoseDetectionCapability();
	            
	            userGen.getNewUserEvent().addObserver(new NewUserObserver());
	            userGen.getLostUserEvent().addObserver(new LostUserObserver());
	            skeletonCap.getCalibrationCompleteEvent().addObserver(new CalibrationCompleteObserver());
	            poseDetectionCap.getPoseDetectedEvent().addObserver(new PoseDetectedObserver());
	            
	            calibPose = skeletonCap.getSkeletonCalibrationPose();
	            joints = new HashMap<Integer, HashMap<SkeletonJoint,SkeletonJointPosition>>();
	            
	            skeletonCap.setSkeletonProfile(SkeletonProfile.ALL);
				context.startGeneratingAll();
				
				attempts = 0;
				
	        } catch (GeneralException e) {
	        	attempts--;
	        }
    	}
    }

    public void registerListener(UserLostListener listener) {
    	listeners.add(listener);
    }
    
    public void update() {
    	try {
			context.waitAnyUpdateAll();
			floor = scene.getFloor();
			
			Vector3f normal = new Vector3f(floor.getNormal().getX(), floor.getNormal().getY(), floor.getNormal().getZ());
	    	rot.fromStartEndVectors(new Vector3f(0,1,0), normal);
		} catch (StatusException e1) {
//			e1.printStackTrace();
		}
    }
    
    public void paint(Graphics g) {
//    	if (draw) {
//    		updateDepth();
//    		
//            DataBufferByte dataBuffer = new DataBufferByte(imgbytes, width*height*3);
//
//            WritableRaster raster = Raster.createInterleavedRaster(dataBuffer, width, height, width * 3, 3, new int[]{0, 1, 2}, null); 
//
//            ColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[]{8, 8, 8}, false, false, ComponentColorModel.OPAQUE, DataBuffer.TYPE_BYTE);
//
//            bimg = new BufferedImage(colorModel, raster, false, null);
//
//    		g.drawImage(bimg, 0, 0, null);
//    	}
//    	
//        try {
//			int[] users = userGen.getUsers();
//			for (int i = 0; i < users.length; ++i) {
//				
//		    	if(draw) {
//		    		Color c = colors[users[i]%colors.length];
//			    	c = new Color(255-c.getRed(), 255-c.getGreen(), 255-c.getBlue());
//			    	g.setColor(c);
//		    	
//			    	if (skeletonCap.isSkeletonTracking(users[i]))
//			    		drawSkeleton(g, users[i]);
//				
//					Point3D com = depthGen.convertRealWorldToProjective(userGen.getUserCoM(users[i]));
//					String label = null;
//					if (!printState)
//						label = new String(""+users[i]);
//					else if (skeletonCap.isSkeletonTracking(users[i]))
//						// Tracking
//						label = new String(users[i] + " - Tracking");
//					else if (skeletonCap.isSkeletonCalibrating(users[i]))
//						// Calibrating
//						label = new String(users[i] + " - Calibrating");
//					else
//						// Nothing
//						label = new String(users[i] + " - Looking for pose (" + calibPose + ")");
//
//					g.drawString(label, (int)com.getX(), (int)com.getY());
//				}
//			}
//		} catch (StatusException e) {
//			e.printStackTrace();
//		}
    }
    
    private Vector3f correctViewingPlane(Vector3f node) {
    	Vector3f rotated = rot.mult(node);
    	return rotated;
    }
    
//    --------------------ComputeSpatialProperties-------------------------------------------------------
    
    public Map<Integer, Float> computeOrientation() throws StatusException {
    	Map<Integer, Float> orientations = new HashMap<Integer, Float>();
    	Vector3f compare = new Vector3f(0,0,-1);
    	int[] users = userGen.getUsers();
		for (int i = 0; i < users.length; ++i) {
			if (skeletonCap.isSkeletonTracking(users[i])) {
				SkeletonJointOrientation neck_or = skeletonCap.getSkeletonJointOrientation(users[i], SkeletonJoint.NECK);
				Vector3f neck_z = new Vector3f(neck_or.getZ1(), neck_or.getZ2(), neck_or.getZ3());
				float neck_orientation = (float)(neck_z.angleBetween(compare)*180f/Math.PI);
		    	if(neck_z.x < 0) neck_orientation = 360-neck_orientation;
		    	orientations.put(users[i], neck_orientation);
		    	
//				SkeletonJointOrientation left_or = skeletonCap.getSkeletonJointOrientation(users[i], SkeletonJoint.LEFT_SHOULDER);
//				Vector3f left_z = new Vector3f(left_or.getZ1(), left_or.getZ2(), left_or.getZ3());
//		    	float left_orientation = (float)(left_z.angleBetween(compare)*180f/Math.PI);
//		    	if(left_z.x < 0) left_orientation = 360-left_orientation;
//		    	orientations.add(left_orientation);
		    	
//				SkeletonJointOrientation right_or = skeletonCap.getSkeletonJointOrientation(users[i], SkeletonJoint.RIGHT_SHOULDER);
//				Vector3f right_z = new Vector3f(right_or.getZ1(), right_or.getZ2(), right_or.getZ3());
//		    	float right_orientation = (float)(right_z.angleBetween(compare)*180f/Math.PI);
//		    	if(right_z.x < 0) right_orientation = 360-right_orientation;
//				orientations.add(right_orientation);
		    	
//		    	orientations.add((neck_orientation+left_orientation+right_orientation)/3.0f);
			}
		}
		
		return orientations;
	}
    
    public Map<Integer, Float> computeDirection() throws StatusException {
    	Map<Integer, Float> directions = new HashMap<Integer, Float>();
    	Vector3f compare = new Vector3f(0,0,1);
    	int[] users = userGen.getUsers();
		for (int i = 0; i < users.length; ++i) {
			if (skeletonCap.isSkeletonTracking(users[i])) {
				Point3D torso = skeletonCap.getSkeletonJointPosition(users[i], SkeletonJoint.TORSO).getPosition();
				Vector3f torso_v = correctViewingPlane(new Vector3f(torso.getX(), torso.getY(), torso.getZ()).normalize());
				
				float direction = (float)(torso_v.angleBetween(compare)*180f/Math.PI);
		    	if(torso_v.x > 0) direction = 360-direction;
		    	directions.put(users[i], direction);
		    	
//				SkeletonJointOrientation left_or = skeletonCap.getSkeletonJointOrientation(users[i], SkeletonJoint.LEFT_SHOULDER);
//				Vector3f left_z = new Vector3f(left_or.getZ1(), left_or.getZ2(), left_or.getZ3());
//		    	float left_orientation = (float)(left_z.angleBetween(compare)*180f/Math.PI);
//		    	if(left_z.x < 0) left_orientation = 360-left_orientation;
//		    	orientations.add(left_orientation);
		    	
//				SkeletonJointOrientation right_or = skeletonCap.getSkeletonJointOrientation(users[i], SkeletonJoint.RIGHT_SHOULDER);
//				Vector3f right_z = new Vector3f(right_or.getZ1(), right_or.getZ2(), right_or.getZ3());
//		    	float right_orientation = (float)(right_z.angleBetween(compare)*180f/Math.PI);
//		    	if(right_z.x < 0) right_orientation = 360-right_orientation;
//				orientations.add(right_orientation);
		    	
//		    	orientations.add((neck_orientation+left_orientation+right_orientation)/3.0f);
			}
		}
		
		return directions;
	}
    
    public Map<Integer, Float> computeDistance() throws StatusException {
    	Map<Integer, Float> distances = new HashMap<Integer, Float>();
    	int[] users = userGen.getUsers();
		for (int i = 0; i < users.length; ++i) {
			if (skeletonCap.isSkeletonTracking(users[i])) {
				Point3D torso = skeletonCap.getSkeletonJointPosition(users[i], SkeletonJoint.TORSO).getPosition();
				Vector3f torso_v = correctViewingPlane(new Vector3f(torso.getX(), torso.getY(), torso.getZ()));
				distances.put(users[i], torso_v.getZ());
			}
		}
		
		return distances;
	}
    
//    ------------------Visualization Methods------------------------------------------------------------

//    private void calcHist(ShortBuffer depth) {
//        // reset
//        for (int i = 0; i < histogram.length; ++i)
//            histogram[i] = 0;
//        
//        depth.rewind();
//
//        int points = 0;
//        while(depth.remaining() > 0) {
//            short depthVal = depth.get();
//            if (depthVal != 0)
//            {
//                histogram[depthVal]++;
//                points++;
//            }
//        }
//        
//        for (int i = 1; i < histogram.length; i++)
//        	histogram[i] += histogram[i-1];
//
//        if (points > 0)
//        	for (int i = 1; i < histogram.length; i++)
//        		histogram[i] = 1.0f - (histogram[i] / (float)points);
//    }
//
//    private void updateDepth() {
//    	DepthMetaData depthMD = depthGen.getMetaData();
//    	SceneMetaData sceneMD = userGen.getUserPixels(0);
//
//    	ShortBuffer scene = sceneMD.getData().createShortBuffer();
//    	ShortBuffer depth = depthMD.getData().createShortBuffer();
//    	calcHist(depth);
//    	depth.rewind();
//            
//    	while(depth.remaining() > 0) {
//    		int pos = depth.position();
//    		short pixel = depth.get();
//    		short user = scene.get();
//                
//    		imgbytes[3*pos] = 0;
//        	imgbytes[3*pos+1] = 0;
//        	imgbytes[3*pos+2] = 0;                	
//
//        	if (drawBackground || pixel != 0) {
//        		int colorID = user % (colors.length-1);
//        		if (user == 0)
//        			colorID = colors.length-1;
//                	
//        		if (pixel != 0) {
//        			float histValue = histogram[pixel];
//        			imgbytes[3*pos] = (byte)(histValue*colors[colorID].getRed());
//        			imgbytes[3*pos+1] = (byte)(histValue*colors[colorID].getGreen());
//        			imgbytes[3*pos+2] = (byte)(histValue*colors[colorID].getBlue());
//                }
//        	}
//        }
//    }
//
//    public Dimension getPreferredSize() {
//        return new Dimension(width, height);
//    }
//
//    private void getJoint(int user, SkeletonJoint joint) throws StatusException {
//        SkeletonJointPosition pos = skeletonCap.getSkeletonJointPosition(user, joint);
//        
//		if (pos.getPosition().getZ() != 0)
//			joints.get(user).put(joint, new SkeletonJointPosition(depthGen.convertRealWorldToProjective(pos.getPosition()), pos.getConfidence()));
//		else
//			joints.get(user).put(joint, new SkeletonJointPosition(new Point3D(), 0));
//    }
//		
//	private void getJoints(int user) throws StatusException {
//    	getJoint(user, SkeletonJoint.HEAD);
//    	getJoint(user, SkeletonJoint.NECK);
//    	
//    	getJoint(user, SkeletonJoint.LEFT_SHOULDER);
//    	getJoint(user, SkeletonJoint.LEFT_ELBOW);
//    	getJoint(user, SkeletonJoint.LEFT_HAND);
//
//    	getJoint(user, SkeletonJoint.RIGHT_SHOULDER);
//    	getJoint(user, SkeletonJoint.RIGHT_ELBOW);
//    	getJoint(user, SkeletonJoint.RIGHT_HAND);
//
//    	getJoint(user, SkeletonJoint.TORSO);
//
//    	getJoint(user, SkeletonJoint.LEFT_HIP);
//        getJoint(user, SkeletonJoint.LEFT_KNEE);
//        getJoint(user, SkeletonJoint.LEFT_FOOT);
//
//    	getJoint(user, SkeletonJoint.RIGHT_HIP);
//        getJoint(user, SkeletonJoint.RIGHT_KNEE);
//        getJoint(user, SkeletonJoint.RIGHT_FOOT);
//    }
//
//    private void drawLine(Graphics g, HashMap<SkeletonJoint, SkeletonJointPosition> jointHash, SkeletonJoint joint1, SkeletonJoint joint2) {
//		Point3D pos1 = jointHash.get(joint1).getPosition();
//		Point3D pos2 = jointHash.get(joint2).getPosition();
//
//		if (jointHash.get(joint1).getConfidence() == 0 || jointHash.get(joint2).getConfidence() == 0)
//			return;
//
//		g.drawLine((int)pos1.getX(), (int)pos1.getY(), (int)pos2.getX(), (int)pos2.getY());
//    }
//    
//    private void drawSkeleton(Graphics g, int user) throws StatusException {
//    	getJoints(user);
//    	HashMap<SkeletonJoint, SkeletonJointPosition> dict = joints.get(new Integer(user));
//
//    	drawLine(g, dict, SkeletonJoint.HEAD, SkeletonJoint.NECK);
//
//    	drawLine(g, dict, SkeletonJoint.LEFT_SHOULDER, SkeletonJoint.TORSO);
//    	drawLine(g, dict, SkeletonJoint.RIGHT_SHOULDER, SkeletonJoint.TORSO);
//
//    	drawLine(g, dict, SkeletonJoint.NECK, SkeletonJoint.LEFT_SHOULDER);
//    	drawLine(g, dict, SkeletonJoint.LEFT_SHOULDER, SkeletonJoint.LEFT_ELBOW);
//    	drawLine(g, dict, SkeletonJoint.LEFT_ELBOW, SkeletonJoint.LEFT_HAND);
//
//    	drawLine(g, dict, SkeletonJoint.NECK, SkeletonJoint.RIGHT_SHOULDER);
//    	drawLine(g, dict, SkeletonJoint.RIGHT_SHOULDER, SkeletonJoint.RIGHT_ELBOW);
//    	drawLine(g, dict, SkeletonJoint.RIGHT_ELBOW, SkeletonJoint.RIGHT_HAND);
//
//    	drawLine(g, dict, SkeletonJoint.LEFT_HIP, SkeletonJoint.TORSO);
//    	drawLine(g, dict, SkeletonJoint.RIGHT_HIP, SkeletonJoint.TORSO);
//    	drawLine(g, dict, SkeletonJoint.LEFT_HIP, SkeletonJoint.RIGHT_HIP);
//
//    	drawLine(g, dict, SkeletonJoint.LEFT_HIP, SkeletonJoint.LEFT_KNEE);
//    	drawLine(g, dict, SkeletonJoint.LEFT_KNEE, SkeletonJoint.LEFT_FOOT);
//
//    	drawLine(g, dict, SkeletonJoint.RIGHT_HIP, SkeletonJoint.RIGHT_KNEE);
//    	drawLine(g, dict, SkeletonJoint.RIGHT_KNEE, SkeletonJoint.RIGHT_FOOT);
//    }
}


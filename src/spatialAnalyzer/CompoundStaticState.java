/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

public class CompoundStaticState {

	private int key;
	private int orientation;
	private int distance;
	private int direction;
	
	public CompoundStaticState(int key, Integer tmp_orientation, Integer tmp_distance, Integer tmp_direction) {
		this.key = key;
		this.orientation = tmp_orientation;
		this.distance = tmp_distance;
		this.direction = tmp_direction;
	}
	
	public int getDirection() {
		return direction;
	}

	public int getDistance() {
		return distance;
	}

	public int getOrientation() {
		return orientation;
	}
	
	public int getKey() {
		return key;
	}
	
	public String toString() {
		AbstractionTable table = AbstractionTable.instance();
		return "Direction: "+table.getQualDirection(direction)+
				"; Distance: "+table.getQualDistance(distance)+
				"; Orientation: "+table.getQualDirection(orientation)+";";
	}
}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

import java.util.HashMap;
import java.util.Map;

public class StaticAbstractionEngine {
	
	public StaticAbstractionEngine() {
	}
	
	public Map<Integer, Integer> abstractOrientation(Map<Integer, Float> orientations) {
		Map<Integer, Integer> res = new HashMap<Integer, Integer>();
		
		for(int user : orientations.keySet()) {
			float f = orientations.get(user);
			
			res.put(user, (int)(((f+11.25)%360)/22.5));
		}
			
		return res;
	}
	
	public Map<Integer, Integer> abstractDistance(Map<Integer, Float> distances) {
		
		Map<Integer, Integer> res = new HashMap<Integer, Integer>();
		
		for(int user : distances.keySet()) {
			float d = distances.get(user);
			
			if (d==0) ;
			else if(d < 1000) res.put(user, 0); //Intimate
			else if(d < 2000) res.put(user, 1); //Personal
			else if(d < 3000) res.put(user, 2); //Social
			else res.put(user, 3); //Public			
		}
			
		return res;
	}

	public Map<Integer, Integer> abstractDirection(Map<Integer, Float> directions) {
		return abstractOrientation(directions);
	}

}

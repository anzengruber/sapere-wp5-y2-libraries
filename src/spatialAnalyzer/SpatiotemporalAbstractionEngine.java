/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Thomas Schmittner, Bernhard Anzengruber
*/

package spatialAnalyzer;

import java.util.HashMap;
import java.util.Map;

import javax.vecmath.Point2d;
import javax.vecmath.Vector2d;

public class SpatiotemporalAbstractionEngine {
	
	private Map<Integer, CompoundStaticState> priorStaticStates;
	private Map<Integer, SpatiotemporalState> priorStates;
	private Map<Integer, SpatiotemporalState> lostUsers;
	
//	-----------SpeedState Variables------------------
	private long lastTime;
	private double distanceTraveled;
	
	
	
	public SpatiotemporalAbstractionEngine() {
		priorStaticStates = new HashMap<Integer, CompoundStaticState>();
		priorStates = new HashMap<Integer, SpatiotemporalState>();
		lostUsers = new HashMap<Integer, SpatiotemporalState>();
		
		lastTime = System.currentTimeMillis();
		distanceTraveled = 0;
	}
	
	public Map<Integer, SpatiotemporalState> computeCurrentStates(Map<Integer, CompoundStaticState> csStates) {
		Map<Integer, SpatiotemporalState> currentStates = new HashMap<Integer, SpatiotemporalState>(); 
		lostUsers.clear();lostUsers.putAll(priorStates);
		
		for(int userID:csStates.keySet()) {
			currentStates.put(userID, createDynamicState(csStates.get(userID)));
		}
		priorStaticStates.clear();priorStaticStates.putAll(csStates);
				
		for(int userID : currentStates.keySet()) {
			SpatiotemporalState tmpState = currentStates.get(userID);
			lostUsers.remove(userID);
			
			if(tmpState.getSemanticMotion() == 0) continue;
				
			currentStates.put(userID, updateSemanticState(tmpState)); //update
		}
		priorStates.clear();priorStates.putAll(currentStates);
		
		for(int userID : lostUsers.keySet())
			currentStates.put(userID, finalizeLostState(lostUsers.get(userID))); //Lost
		
		return currentStates;
	}
	
//	------------------------------------Private Functions---------------------------------------------------
	
	private SpatiotemporalState createDynamicState(CompoundStaticState current) {
		CompoundStaticState prior = priorStaticStates.get(current.getKey());
		if(prior == null)
			return new SpatiotemporalState(current.getKey(), 0, 0, 0);
		
		int priorDis = prior.getDistance(), priorDir = prior.getDirection();
		int currentDis = current.getDistance(), currentDir = current.getDirection();
		
		int semMotion = -1, dirMotion = -1, spMotion = -1;
		
		//compute Dynamic Semantic State 
		if(priorDis<currentDis)
			semMotion = 3; //MovingAway
		else if(priorDis==currentDis) {
			if(priorDir == currentDir) semMotion = 11; //Still
			else semMotion = 5; //CrossingPath
		}
		else {
			semMotion = 1; //ClosingIn
		}
		
		Point2d arc_prior = computeAbstractRegionCenter(priorDir, priorDis);
		Point2d arc_current = computeAbstractRegionCenter(currentDir, currentDis);
		
		Vector2d dirVec = new Vector2d(arc_current.x - arc_prior.x, arc_current.y - arc_prior.y); 
		double vecLength = dirVec.length();
		
		if(vecLength > 0) {
			double dirOfMotion = 180*Math.asin(dirVec.x/vecLength)/Math.PI;
			if(dirVec.y < 0) dirOfMotion = 180-dirOfMotion;
			else if(dirVec.x < 0) dirOfMotion = 360+dirOfMotion;
			dirMotion = (int)(((dirOfMotion+11.25)%360)/22.5);
		}
		
		spMotion = getCurrentSpeed(computeDistanceTraveled(arc_prior, arc_current));
		
		return new SpatiotemporalState(current.getKey(), semMotion, dirMotion, spMotion);
	}
	
	private double computeDistanceTraveled(Point2d arc_prior, Point2d arc_current) {
		Vector2d motion = new Vector2d(arc_prior.x-arc_current.x, arc_prior.y-arc_current.y);
		
		return Math.sqrt(Math.pow(motion.x, 2) + Math.pow(motion.y, 2));
	}

	private Point2d computeAbstractRegionCenter(int dir, int dist) {
		Point2d center = new Point2d();
		double center_dist = 0;
		
		switch(dist) {
			case 0: center_dist = 400; break;
			case 1: center_dist = 1000; break;
			case 2: center_dist = 2600; break;
			case 3: center_dist = 4000; break;
		}
		
		double angle = 22.5f * dir;
		angle = angle*Math.PI/180;
		center.x = center_dist * Math.sin(angle);
		
		angle = 90 - 22.5f * dir;
		angle = angle*Math.PI/180;
		center.y = center_dist * Math.sin(angle);
		
		return center;
	}
	
	private int getCurrentSpeed(double currentDistTraveled) {
		int speed = -1;
		distanceTraveled += currentDistTraveled;
		
		if(System.currentTimeMillis()-lastTime > 1000) {
			if(distanceTraveled<1000) speed = 1; // Slow - slower than 1 m/s
			else if(distanceTraveled<1500) speed = 2; // Medium - slower than 1.5 m/s
			else speed = 3; // Fast - faster than 1.5 m/s
			
			distanceTraveled = 0;
			lastTime = System.currentTimeMillis();
		}
		
		return speed;
	}

	private SpatiotemporalState updateSemanticState(SpatiotemporalState current) {
		SpatiotemporalState prior = priorStates.get(current.getId());
		if(prior == null) {
			System.out.println("Error: List Inconsistencies");
			return current;
		}
		
		int curr_semanticState = current.getSemanticMotion();
		int prior_semanticState = prior.getSemanticMotion();
		
		int curr_directionalState = current.getDirectionOfMotion();
		if(curr_directionalState == -1) curr_directionalState = prior.getDirectionOfMotion();
		
		int curr_speedState = current.getSpeedOfMotion();
		if(curr_speedState == -1) curr_speedState = prior.getSpeedOfMotion();
		
		//Spatiotemporal Rule Table
		switch(prior_semanticState) {
			case 1: /*CI*/ case 2: /*SCI*/ case 9: /*TT*/
				if(curr_semanticState == 11) curr_semanticState = 2; // CI|SCI|TT - S -> SCI
				else if(curr_semanticState == 3) curr_semanticState = 10; // CI|SCI|TT - MA -> TA
				break;
			
			case 3: /*MA*/ case 4: /*SMA*/ case 10: /*TA*/
				if(curr_semanticState == 11) curr_semanticState = 4; // MA|SMA|TA - S -> SMA
				else if(curr_semanticState == 1) curr_semanticState = 9; // MA|SMA|TA - CI -> TT
				break;
				
			case 5: /*CP*/ case 6: /*SCP*/
				if(curr_semanticState == 11) curr_semanticState = 6; // CP|SCP - S -> SCP
				else if(curr_semanticState == 1) curr_semanticState = 9; // CP|SCP - CI -> TT
				else if(curr_semanticState == 3) curr_semanticState = 10; // CP|SCP - MA -> TA
				break;	
		}
		
		return new SpatiotemporalState(current.getId(), curr_semanticState, curr_directionalState, curr_speedState);
	}
	
	private SpatiotemporalState finalizeLostState(SpatiotemporalState lostUserState) {
		int semanticState = lostUserState.getSemanticMotion();
		int directionalState = lostUserState.getDirectionOfMotion();
		int speedState = lostUserState.getSpeedOfMotion();
		
		switch(semanticState) {
			case 1: case 2: case 9: //CI, SCI, TT
				semanticState = 7; // PB
				break;
			default: semanticState = 12; //G
				break;
		}
		
		return new SpatiotemporalState(lostUserState.getId(), semanticState, directionalState, speedState);
	}
}

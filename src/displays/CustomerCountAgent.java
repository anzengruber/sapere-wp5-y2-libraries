/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.util.Timer;
import java.util.TimerTask;

import visualization.PresentationVars;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class CustomerCountAgent extends SapereAgent {
	private volatile int currentCustomerCount;
	private int overallCustomerCount;
	private int currentTimeslotID;
	
	private Timer timeSlotTimer;
	private TimeSlotTimerTask timeSlotTimerTask;
	
	public CustomerCountAgent(String name) {
		super(name);
		currentCustomerCount = 0;
		overallCustomerCount = 0;
		currentTimeslotID = 0;
		
		timeSlotTimer = new Timer();
		timeSlotTimerTask = new TimeSlotTimerTask();
		timeSlotTimer.schedule(timeSlotTimerTask, 0);
	}

	private class TimeSlotTimerTask extends TimerTask {
		@Override
		public void run() {
			PresentationVars.instance();
			long runtime_ms = PresentationVars.RUN_TIME_S * 1000;
			long inc = runtime_ms/10;
			
			while(currentTimeslotID<10) {
				try {
					Thread.sleep(inc);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				currentTimeslotID++;
				currentCustomerCount = 0;
			}
			currentTimeslotID = -1;
		}
	}
	
	public void onCustomerArrived(ILsa customer) {
		currentCustomerCount++;
		overallCustomerCount++;
		
		if(currentTimeslotID == -1) {
			removeProperty("current customers");
			removeProperty("overall customers");
		}
		else {
			addProperty(new Property("current customers", currentTimeslotID+":"+currentCustomerCount));
			addProperty(new Property("overall customers", ""+overallCustomerCount));
		}
	}
	
	public void onCustomerDeparted(ILsa customer) {
		
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {		
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property("user", this.agentName));
		addProperty(new Property("current customers", currentTimeslotID+":"+currentCustomerCount));
		addProperty(new Property("overall customers", ""+overallCustomerCount));
	}
}

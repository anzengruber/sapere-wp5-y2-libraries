/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.util.Iterator;
import java.util.Vector;

import eu.sapere.middleware.agent.DataLsa;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Lsa;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class SportAgent extends SapereAgent {

	private Vector<Property> properties = new Vector<Property>();
	private volatile Vector<String> currentContent = new Vector<String>();
	
	public SportAgent(String name) {
		super(name);
	}

	private void addDataLsa(Property property) {
		properties.add(property);
		new DataLsa(new Lsa().addProperty(property));
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		currentContent.clear();
		Property sportProp = event.getBondedLsa().getProperty("sport");
		for(int i = 0; i<sportProp.getValue().size(); i++) {
			String content = sportProp.getValue().elementAt(i);
			
			for(Iterator<Property> it = properties.iterator();it.hasNext();) {
				Property tmpProp = it.next();
				if(tmpProp.getName().equals(content)) {
					System.out.println(tmpProp.getValue().elementAt(0).toString());
					currentContent.add(tmpProp.getValue().elementAt(0).toString());
				}
			}
		}
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		currentContent.clear();
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
			
	}
	
	public Vector<String> getCurrentContent() {
		return currentContent;
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property("user", this.agentName));
		addSubDescription("content", new Property("sport", "*"));
		
//		addDataLsa(new Property("Rafting", "rafting.jpg"));
		addDataLsa(new Property("Rafting", "SASO_WS_II.jpg"));
//		addDataLsa(new Property("Racing", "racing.jpg"));
//		addDataLsa(new Property("Soccer", "soccer.jpg"));
//		addDataLsa(new Property("Tennis", "tennis.jpg"));
//		addDataLsa(new Property("Starcraft2", "starcraft2.jpg"));
	}

}

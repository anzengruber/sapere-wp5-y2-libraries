/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class OrientationThread extends Thread {
	public static final String ADBPATH_64 = "C:\\Program Files (x86)\\Android\\android-sdk\\platform-tools\\adb.exe";
	public static final String ADBPATH_32 = "C:\\Program Files\\Android\\android-sdk\\platform-tools\\adb.exe";
	public static final int INTERVALL = 2000;
	public static final int PORT_PHONE = 38300;
	public static final int PORT_PC = 38300;
	private boolean run = true;
	
	private SteeringAgent agent;
	private static long time = System.currentTimeMillis();
	private Socket socket;
	
	private String longitude = "", latitude = "", or = "";
	
	public OrientationThread(SteeringAgent steeringAgent) {
		this.agent = steeringAgent;
	}

	@Override
	public void run() {
		String adbPath;
		String property = System.getProperty("os.arch");
		if (property.indexOf("64") >= 0) {
			adbPath = ADBPATH_64;
		} else {
			adbPath = ADBPATH_32;
		}
		try {
			Runtime.getRuntime().exec(
					adbPath + " forward tcp:" + PORT_PHONE + " tcp:" + PORT_PC);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String str;
		Scanner sc;
		while(run) {
			try {
				socket = new Socket("localhost", 38300);
				sc = new Scanner(socket.getInputStream());
				while (sc.hasNext()) {
					str = sc.next();
					if(str.startsWith("NWL")) {
						String[] nwl = str.split(";");
						latitude = nwl[1];
						longitude = nwl[2];
					}
					
					if (System.currentTimeMillis() - time > INTERVALL) {
						if(str.startsWith("AZI")) {
							time = System.currentTimeMillis();
							or = (str.substring(4));
						}
						agent.updateData(or, latitude, longitude);
					}
					
					if (str.startsWith("CLOSE")) {
						socket.close();
						break;
					}
				}
			} catch (UnknownHostException e) {
//				e.printStackTrace();
			} catch (IOException e) {
//				e.printStackTrace();
			}
		}
	}
}

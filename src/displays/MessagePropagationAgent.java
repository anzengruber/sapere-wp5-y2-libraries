/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.util.Vector;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class MessagePropagationAgent extends SapereAgent {

	public MessagePropagationAgent(String name) {
		super(name);
	}
	
	public void propagateLSA(Vector<Property> tmpProperties) {
		Property[] properties = new Property[tmpProperties.size()];
		tmpProperties.toArray(properties);
		
		addProperty(properties);
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		event.getLsa().addProperty(new Property(PropertyName.DECAY.toString(), "1"));
	}

	public Vector<String> getCurrentContent() {
		return null;
	}

	@Override
	public void setInitialLSA() {
		
	}
}

/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.awt.Canvas;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeCanvasContext;
import com.sun.jna.NativeLibrary;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.node.NodeManager;
import spatialAnalyzer.SpatialAgent;
import visualization.MyProperties;
import visualization.PresentationService;
import visualization.PresentationVars;
import visualization.VisualizationService;

public class Start {

	private static JFrame frame;
	private static Application app;
	private static JmeCanvasContext context;
    private static Canvas canvas;
    private static Container currentPanel;
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PresentationVars.instance();
		if (!PresentationVars.DISABLE_GUI) {
			currentPanel = new JPanel();
			createCanvas();
		       
	        try {
	        	Thread.sleep(2000);
	        } catch (InterruptedException ex) {
	        	;
	        }
	        
	        SwingUtilities.invokeLater(new Runnable(){
	            public void run(){
	                createFrame();
	                frame.setUndecorated(true);
	                currentPanel.add(canvas);
	                frame.getContentPane().add(currentPanel);
	                frame.pack();
	                startApp();
	                frame.setLocation(-5, -5);
	                frame.setVisible(true);
	                frame.setAlwaysOnTop(false);
	            }
	        });
		}
		else {
			startSapere();
		}
	}
	
	public static void startApp(){
        app.startCanvas();
        
        app.enqueue(new Callable<Void>(){
            public Void call(){
                if (app instanceof SimpleApplication){
                    SimpleApplication simpleApp = (SimpleApplication) app;
                    simpleApp.getFlyByCamera().setDragToRotate(true);
                    
                }
                return null;
            }
        });
    }

	private static void createFrame(){
        frame = new JFrame("SAPERE");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                app.stop();
            }
        });
    }

    public static void createCanvas(){
    	app = PresentationService.instance();
    	
    	Logger.getLogger("com.jme3").setLevel(Level.SEVERE);
		PresentationService.instance().setShowSettings(false);
		
		NativeLibrary.addSearchPath("libvlc", ".\\vlc");
		
		AppSettings settings = new AppSettings(true);
		settings.setFrameRate(60);
		settings.setAudioRenderer(null);
		settings.put("Width", PresentationVars.SCR_WIDTH);
		settings.put("Height", PresentationVars.SCR_HEIGHT);
		settings.put("Fullscreen", PresentationVars.FULLSCREEN);
		settings.put("Title", "SAPERE");
		settings.put("VSync", true);
		settings.put("Samples", 1);

        app.setPauseOnLostFocus(false);
        app.setSettings(settings);
        app.createCanvas();

        context = (JmeCanvasContext) app.getContext();
        canvas = context.getCanvas();
        canvas.setSize(settings.getWidth(), settings.getHeight());
    }
    
    public static void startSapere() {
    	VisualizationService vis = new VisualizationService();
    	
		/*Implements steering functionality*/
		//SteeringAgent steering = new SteeringAgent("Steering Agent");
		
		/*Counts current and overall customers*/
//		CustomerCountAgent cca = new CustomerCountAgent("Customer Count Agent");

		/*Interacts with customers and displays content*/
		CustomerInformationAgent cia = new CustomerInformationAgent("Customer Information Agent");
		
		/*Used for Experimentation on SAPERE metrics*/
//		MetricExperimentationAgent metrics = new MetricExperimentationAgent("Metric Experimentation Agent");
		
		//steering.addVisualizationListener(vis);
		//steering.setInitialLSA();
		
		cia.addVisualizationListener(vis);
		cia.setInitialLSA();
		
//		cca.setInitialLSA();
		
		if(MyProperties.instance().get("qsr").equals("true")) {
			//SpatialAgent spatial = new SpatialAgent("Spatial Agent", MyProperties.instance().get("showCameraFrame").equals("true"));
			//spatial.addVisualizationListener(vis);
			//spatial.setInitialLSA();
		}
    }

}

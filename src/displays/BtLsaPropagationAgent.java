/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.networking.physical.bluetooth.ListChangedListener;
import eu.sapere.middleware.node.networking.physical.bluetooth.RegistrationService;
import eu.sapere.middleware.node.networking.topology.NeighbourAnalyzer;
import eu.sapere.middleware.node.networking.topology.NeighbourListener;
import eu.sapere.middleware.node.networking.topology.physical.PhysicalNeighbour;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

/**
 * @author Thomas Schmittner
 * 
 */
public class BtLsaPropagationAgent extends SapereAgent implements
		ListChangedListener, NeighbourListener {

	private RegistrationService service;
	private Map<String, PhysicalNeighbour> services;
	private List<String> propagatedLSAs;
	private HashMap<String, HashMap<String, String>> neighbors;

	public BtLsaPropagationAgent(String name) {
		super(name);
		neighbors = new HashMap<String, HashMap<String, String>>();
		service = RegistrationService.instance();
		service.start();
		service.addListChangedListener(this);
		services = RegistrationService.instance().getRegisteredDevices();
		propagatedLSAs = new ArrayList<String>();
		NeighbourAnalyzer.addListener(this);
	}

	private PhysicalNeighbour parseCsvString(String csv) {
		String[] s = csv.split(";");
		return new PhysicalNeighbour(s[0], s[1], s[2], s[3]);
	}

	private String createCsvStringOfPhysicalNeighbor(PhysicalNeighbour neighbor) {
		StringBuilder builder = new StringBuilder();
		builder.append(neighbor.getBtMac());
		builder.append(";");
		builder.append(neighbor.getIpAddress());
		builder.append(";");
		builder.append(neighbor.getNodeName());
		builder.append(";");
		builder.append(neighbor.getNodeType());
		return builder.toString();
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		ILsa lsa = event.getBondedLsa();
		Property previous;
		String lsaId = lsa.getProperty("id").getValue().firstElement();
		System.out.println("propagatedLSAs " + propagatedLSAs);
		if (!propagatedLSAs.contains(lsaId)) {
			propagatedLSAs.add(lsaId);
			System.out.println("Bonded LSA: " + lsa);
			Property bluetooth = lsa.getProperty("bluetooth");
			Vector<String> neighbors = bluetooth.getValue();
			services.clear();
			for (String neighbor : neighbors) {
				services.put(neighbor.substring(0, neighbor.indexOf(";"))
						.replace(":", ""), parseCsvString(neighbor));
			}
		}
		if ((previous = lsa.getProperty(PropertyName.PREVIOUS.toString())) != null) {
			propagateLsa(previous.getValue().firstElement());
		}
		System.out.println("NEW SERVICES KEYS = " + services.keySet());
		System.out.println("NEW SERVICES VALS = " + services.values());
	}

	private void propagateLsa(String previous) {
		String target = new String();
		UUID uuid = null;
		for (HashMap<String, String> map : neighbors.values()) {
			boolean isInfrastructure = false;
			for (String key : map.keySet()) {
				if (key.equals("nodeType")) {
					if (map.get(key).equals("pc")) {
						isInfrastructure = true;
					}
				}
				if (key.equals("neighbour")) {
					target = map.get(key);
				}
			}
			if (isInfrastructure && !target.equals(previous)) {
				System.out.println("Propagate LSA to " + target);
				Vector<String> registeredDevices = new Vector<String>();
				for (String btMac : services.keySet()) {
					registeredDevices
							.add(createCsvStringOfPhysicalNeighbor(services
									.get(btMac)));
				}
				Vector<Property> tmpProperties = new Vector<Property>();
				Property property = new Property("bluetooth");
				property.setValue(registeredDevices);

				tmpProperties.add(property);

				uuid = UUID.randomUUID();
				// propagatedLSAs.add(uuid.toString());

				tmpProperties.add(new Property("id", uuid.toString()));
				tmpProperties.add(new Property(PropertyName.DESTINATION
						.toString(), target));
				tmpProperties.add(new Property(PropertyName.DECAY.toString(),
						"10"));
				tmpProperties.add(new Property(PropertyName.DIFFUSION_OP
						.toString(), "direct"));
				new MessagePropagationAgent("BTA").propagateLSA(tmpProperties);
			}
		}
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property("name", agentName));
		addSubDescription("bt-prop", new Property("bluetooth", "*"));
	}

	@Override
	public void listChanged() {
		propagateLsa(null);
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		// TODO Auto-generated method stub
	}

	public Vector<String> getCurrentContent() {
		return null;
	}

	@Override
	public void onNeighbourFound(String key, HashMap<String, String> data) {
		neighbors.put(key, data);
	}

	@Override
	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		if (neighbors.containsKey(key)) {
			neighbors.remove(key);
		}
	}
}
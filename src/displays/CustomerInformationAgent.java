/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import visualization.VisualizationListener;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.node.networking.topology.NeighbourAnalyzer;
import eu.sapere.middleware.node.networking.topology.NeighbourListener;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class CustomerInformationAgent extends SapereAgent implements NeighbourListener{

	private static final String PERSONAL_CONTENT = "personal content";
	private static final String OVERALL_CUSTOMERS = "overall customers";
	private static final String CURRENT_CUSTOMERS = "current customers";
	private static final String FOOD_PREFERENCES = "food preferences";

	private Vector<String> currentContent = new Vector<String>();
	private Vector<VisualizationListener> listeners = new Vector<VisualizationListener>();
	private Map<String, String> bondedUsers = new HashMap<String, String>();
	
	public CustomerInformationAgent(String name) {
		super(name);
		NeighbourAnalyzer.addListener(this);
	}

	public void addVisualizationListener(VisualizationListener list) {
		listeners.add(list);
	}

	public void notifyContentChanged() {
		for (VisualizationListener list : listeners)
			list.fireContentsChanged(this.agentName, currentContent);
	}

	@Override
	public void onBondAddedNotification(BondAddedEvent event) {		
		Property currentCustomers = event.getBondedLsa().getProperty(
				CURRENT_CUSTOMERS);
		Property overallCustomers = event.getBondedLsa().getProperty(
				OVERALL_CUSTOMERS);
		Property personalContent = event.getBondedLsa().getProperty(
				PERSONAL_CONTENT);
		Property behavior = event.getBondedLsa().getProperty("behavior");
		Property sci = event.getBondedLsa().getProperty("sci");
		Property contentChannel = event.getBondedLsa().getProperty("content channel");
		Property contentIndex = event.getBondedLsa().getProperty("content index");
		Property foodPreferences = event.getBondedLsa().getProperty(
				FOOD_PREFERENCES);

//		System.out.println("Bonded to: " + event.getBondedLsa());

		if (currentCustomers != null && overallCustomers != null) {
			processCustomerCount(currentCustomers.getValue().elementAt(0),
					overallCustomers.getValue().elementAt(0));
		}
		if (personalContent != null) {
			processPersonalContent(event.getBondedLsa().getProperty("previous").toString(), 
					personalContent.getValue(), behavior.getValue(), sci.getValue().elementAt(0),
//					personalContent.getValue().elementAt(0), ""+0);
					contentChannel.getValue().elementAt(0), contentIndex.getValue().elementAt(0));
			
			bondedUsers.get(event.getBondedLsa().getProperty("previous").toString());
			
			bondedUsers.put(event.getBondId(), event.getBondedLsa().getProperty("previous").toString());
		}
		if (foodPreferences != null) {
			processFoodPreferences(event.getBondedLsa().getProperty("previous")
					.toString(), foodPreferences.getValue());
		}
		
	}

	private void processPersonalContent(String name, Vector<String> value, Vector<String> weights, 
			String sci, String contentChannel, String contentIndex) {
		 
		
		 currentContent.clear();
		 currentContent.add("Personal");
		 currentContent.add(name.substring(1, name.length()-1));
		 currentContent.add(sci);
		 currentContent.add(weights.elementAt(0));
		 currentContent.add(weights.elementAt(1));
		 currentContent.add(weights.elementAt(2));
		 
		 if(contentChannel.equals("")) contentChannel = value.elementAt(0);
		 currentContent.add(contentChannel);
		 
		 if(contentIndex.equals("")) contentIndex = ""+0;
		 currentContent.add(contentIndex);
		 
		 currentContent.addAll(value);
		 notifyContentChanged();
	}

	private void processFoodPreferences(String name, Vector<String> value) {
		// currentContent.clear();
		// currentContent.add("Personal");
		// currentContent.add(name);
		// currentContent.addAll(value);
		// notifyContentChanged();
	}

	private void processCustomerCount(String currentCustomers,
			String overallCustomers) {
		currentContent.clear();
		currentContent.add("Count");
		currentContent.add(currentCustomers);
		currentContent.add(overallCustomers);
		notifyContentChanged();
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
		Property currentCustomers = event.getLsa().getProperty(
				"current customers");
		Property overallCustomers = event.getLsa().getProperty(
				"overall customers");
		if (currentCustomers != null && overallCustomers != null)
			processCustomerCount(currentCustomers.getValue().elementAt(0),
					overallCustomers.getValue().elementAt(0));
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setInitialLSA() {
		addProperty(new Property("user", this.agentName));
		addSubDescription("Count-1", new Property(CURRENT_CUSTOMERS, "*"));
		addSubDescription("Count-2", new Property(OVERALL_CUSTOMERS, "*"));
		addSubDescription("UserContent", new Property(PERSONAL_CONTENT, "*"));
	}

	@Override
	public void onNeighbourFound(String key, HashMap<String, String> data) {
	}

	@Override
	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		String nodespace = data.get("neighbour");
		currentContent.clear();
		currentContent.add("Delete");
		currentContent.add(nodespace);
		notifyContentChanged();
	}
}
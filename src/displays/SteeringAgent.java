/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import javax.vecmath.Vector2f;

import visualization.MyProperties;
import visualization.VisualizationListener;
import visualization.VisualizationService;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.interfaces.ILsa;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.networking.topology.NeighbourAnalyzer;
import eu.sapere.middleware.node.networking.topology.NeighbourListener;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;


public class SteeringAgent extends SapereAgent implements NeighbourListener {
	private volatile Vector<String> currentContent = new Vector<String>();
	private Vector<VisualizationListener> listeners = new Vector<VisualizationListener>();

	private volatile Map<String, HashMap<String, String>> neighbours;
	private Map<String, Boolean> steeringReqs;
	private volatile Map<String, String> directions;
	private volatile Map<String, String> returnedReqs;
	
	private boolean alive = true;
	
	private String orientation = "0", longitude = "", latitude= "";
	private int myX, myY;
	
	private Vector<TimeoutTask> timeoutVector;
//					private FileWriter fos;
	
	public SteeringAgent(String name) {
		super(name);
		NeighbourAnalyzer.addListener(this);
		neighbours = new HashMap<String, HashMap<String, String>>();
		steeringReqs = new HashMap<String, Boolean>();
		returnedReqs = new HashMap<String, String>();
		directions = new HashMap<String, String>();
		
		myX = Integer.parseInt(MyProperties.instance().getProperty("myX"));
		myY = Integer.parseInt(MyProperties.instance().getProperty("myY"));
		
		timeoutVector = new Vector<TimeoutTask>();
		new UpdateTask().start();
		
		if(MyProperties.instance().get("orSensor").equals("true"))
			new OrientationThread(this).start();
		
//					try {
//						fos = new FileWriter(new File("steering-"+this.opMng.getSpaceName()+".txt"));
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					
//					steeringTimer.schedule(new ExperimentTask(), 0);
	}

//	private class ExperimentTask extends TimerTask {
//		
//		public ExperimentTask() {
//		}
//		
//		public void run() {
//			Vector<Property> tmpProperties = new Vector<Property>();
//			tmpProperties.add(new Property("steering", "PrivateDisplay:Library"));
//			tmpProperties.add(new Property(PropertyName.DECAY.toString(), "10"));
//			
//			new MessagePropagationAgent("Init").propagateLSA(tmpProperties);
//		}
//	}
	private class UpdateTask extends Thread {
		public void run() {
			while(alive) {
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				notifyShowContent();
			}
		}
	}
	
	private class TimeoutTask extends Thread {
		private String myId;
		private long timeOut, initTime;
		
		public TimeoutTask(String id) {
			myId = id;
			timeOut = 300000;
			initTime = System.currentTimeMillis();
		}
		
		public String getSteeringId() {
			return myId;
		}
		
		public void resetTimeout() {
			initTime = System.currentTimeMillis();
		}
		
		public void run() {
			while(System.currentTimeMillis()-initTime < timeOut) {
				try {
					sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			returnedReqs.remove(myId);
			directions.remove(myId);
			steeringReqs.remove(myId);
				
			removeSubDescription(myId);
			notifyContentTimedOut(myId);
		}
	}
	
	public void notifyContentTimedOut(String id) {
		currentContent.clear();
		currentContent.add(this.agentName);
		currentContent.add("Hide");
		currentContent.add(id);
		for(VisualizationListener list:listeners) 
			list.fireContentsChanged(this.agentName, currentContent);
	}
	
	public void notifyShowContent() {
		currentContent.clear();
		currentContent.add(this.agentName);
		currentContent.add("Data");
		currentContent.add(orientation);
		currentContent.add(longitude);
		currentContent.add(latitude);
		
		Vector<String> send = new Vector<String>();
		send.addAll(currentContent);
		for(VisualizationListener list:listeners) 
			list.fireContentsChanged(this.agentName, send);
		
		for(String id : returnedReqs.keySet()) {
			currentContent.clear();
			currentContent.add(this.agentName);
			currentContent.add("Show");
			currentContent.add(""+directions.get(id));
			currentContent.add(orientation);
			currentContent.add(id);
			currentContent.add(returnedReqs.get(id));
			
			send = new Vector<String>();
			send.addAll(currentContent);
			for(VisualizationListener list:listeners) 
				list.fireContentsChanged(this.agentName, send);
		}
	}	
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		String id = "";
				
		Property steering = event.getBondedLsa().getProperty("steering");
		Property steeringRet = event.getBondedLsa().getProperty("steering-ret");
		Property steeringReq = event.getBondedLsa().getProperty("steering-req");
		
		if(steering != null) {
			id = steering.getValue().elementAt(0);
			
//			check for steering consume
//			SubDescription steeringSub = ((Lsa)event.getLsa()).getSubDescriptionByName(id);
//			if(steeringSub != null) {
//				if(steeringReqs.containsKey(id)) {
//					
//					return;
//				}
//			}
			
			//Initiate Req
			if(!steeringReqs.containsKey(id)) {
				String destination = id.split(":")[1];
				
				if(destination == null) {
					System.out.println("Steering destination null");
					return;
				}
					
				steeringReqs.put(id, false);
				setTimer(id);
				if(steering.getValue().size() >= 1) {
					if(!destination.equals(this.opMng.getSpaceName()))
						initSteeringRequest(event.getBondedLsa(), id);
					else {
						//Already at destination
						System.out.println("Already at destination");
						steeringReqs.remove(id);
					}
					return;
				}
			}
//			else {
//				try {
//					fos.write("Received: " + event.getBondedLsa().toString() + "\r\n");
//					fos.flush();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
		}
		else if(steeringRet != null) {
			id = steeringRet.getValue().elementAt(0);
			if(!steeringReqs.containsKey(id) || !steeringReqs.get(id)) {
//				Req has returned
				steeringReqs.put(id, true);
				
				for(Iterator<TimeoutTask> it = timeoutVector.iterator(); it.hasNext();) {
					TimeoutTask t = it.next();
					if(t.getSteeringId().equals(id)) t.resetTimeout();
				}
				
				returnDestinationFound(event.getBondedLsa(), id);
			}
//			else {
//				try {
//					fos.write("Received: " + event.getBondedLsa().toString() + "\r\n");
//					fos.flush();
//				} catch (IOException e) {
//				// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
			return;
		}
		else if(steeringReq != null) {
			//Propagated Req
			id = steeringReq.getValue().elementAt(0);
			
			if(!steeringReqs.containsKey(id)) {
				String destination = id.split(":")[1];
				
				if(destination == null) {
					System.out.println("Steering destination null");
					return;
				}
				
				if(destination.equals(this.opMng.getSpaceName())) {
//					Destination is here
					returnDestinationFound(event.getBondedLsa(), id);
					steeringReqs.put(id, true);
				}
				else {
					propagateSteeringRequest(event.getBondedLsa(), id);
					steeringReqs.put(id, false);
				}
				setTimer(id);
				return;
			}
//			else {
//				try {
//					fos.write("Received: " + event.getBondedLsa().toString() + "\r\n");
//					fos.flush();
//				} catch (IOException e) {
//				// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
		}
		else {
			System.out.println("No steering requests found");
			return;
		}
	}

	private void setTimer(String id) {
		TimeoutTask t = new TimeoutTask(id);
		timeoutVector.add(t);
		t.start();
	}
	
	private void returnDestinationFound(ILsa bondedLsa, String id) {
		Vector<String> path = bondedLsa.getProperty("path").getValue();
		String targetLoc = ""; 
		String destination = id.split(":")[1];
		String nextHop = "";
		String myLoc = myX+":"+myY;
		
		if(bondedLsa.getProperty("steering-req") != null ) {
			path.add(destination);
			targetLoc = myLoc;
		}
		else {
			nextHop = path.remove(path.size()-1);
			targetLoc = bondedLsa.getProperty("loc").getValue().elementAt(0);
		}
		
		if(path.size() == 1) {
//			System.out.println("Returned to First Node");
//					try {
//						fos.write("Returned " +System.currentTimeMillis()+ " "+ bondedLsa.toString() + "\r\n");
//						fos.flush();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
			
			computeDirection(id, targetLoc);
			returnedReqs.put(id, nextHop);
			addSubDescription(id, new Property("next", nextHop), new Property("loc", targetLoc));
			return;
		}
		
//					try {
//						fos.write("Return " +System.currentTimeMillis()+ " "+ bondedLsa.toString() + "\r\n");
//						fos.flush();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
		
		String target = "";
		Set<Entry<String, HashMap<String, String>>> entries = neighbours.entrySet();
		for(Iterator<Entry<String, HashMap<String, String>>> j = entries.iterator(); j.hasNext();) {
			Iterator<Entry<String, String>> i = j.next().getValue().entrySet().iterator();
			while(i.hasNext()){
				Entry<String, String> e = i.next();
				if(e.getKey().equals("neighbour") && e.getValue().equals(path.get(path.size()-2)))
					target = e.getValue();
			}
		}
		
		if(target.equals("")) {
			System.out.println("ReturnPath broken");
			return;
		}

		Property pathProp = new Property("path","");
		pathProp.setValue(path);
		
		Vector<Property> tmpProperties = new Vector<Property>();
		tmpProperties.add(new Property("steering-ret", id));
		tmpProperties.add(pathProp);
		tmpProperties.add(new Property("loc", myLoc));
		tmpProperties.add(new Property(PropertyName.DECAY.toString(), "15"));
		tmpProperties.add(new Property(PropertyName.DESTINATION.toString(), target));
		tmpProperties.add(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
		
		new MessagePropagationAgent("Ret").propagateLSA(tmpProperties);
		
		computeDirection(id, targetLoc);
		returnedReqs.put(id, nextHop);
		addSubDescription(id, new Property("next", nextHop), new Property("loc", targetLoc));		
	}

	private void propagateSteeringRequest(ILsa bondedLsa, String id) {
//					try {
//						fos.write("Prop " +System.currentTimeMillis()+ " "+ bondedLsa.toString() + "\r\n");
//						fos.flush();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
		
		Set<Entry<String, HashMap<String, String>>> entries = neighbours.entrySet();		
		Vector<String> path = bondedLsa.getProperty("path").getValue();
		path.add(this.opMng.getSpaceName());
		
		for(Iterator<Entry<String, HashMap<String, String>>> j = entries.iterator(); j.hasNext();) {
			boolean isInfrastructure = false;
			String target = "";
			Iterator<Entry<String, String>> i = j.next().getValue().entrySet().iterator();
			while(i.hasNext()){
				Entry<String, String> e = i.next();
				if(e.getKey().equals("nodeType")) {
					if(e.getValue().equals("pc")) isInfrastructure = true;
					else break;
				}
				if(e.getKey().equals("neighbour")) target = e.getValue();
			}
			
			if(isInfrastructure) {
				Property pathProp = new Property("path","");
				pathProp.setValue(path);
				
				Vector<Property> tmpProperties = new Vector<Property>();
				tmpProperties.add(new Property("steering-req", id));
				tmpProperties.add(pathProp);
				tmpProperties.add(new Property(PropertyName.DECAY.toString(), "15"));
				tmpProperties.add(new Property(PropertyName.DESTINATION.toString(), target));
				tmpProperties.add(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
				
				new MessagePropagationAgent("Prop: "+target).propagateLSA(tmpProperties);
			}
		}	
	}

	private void initSteeringRequest(ILsa bondedLsa, String id) {
//					try {
//						fos.write("Init " +System.currentTimeMillis()+ " "+ bondedLsa.toString() + "\r\n");
//						fos.flush();
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
		
		Set<Entry<String, HashMap<String, String>>> entries = neighbours.entrySet();
		for(Iterator<Entry<String, HashMap<String, String>>> j = entries.iterator(); j.hasNext();) {
			boolean isInfrastructure = false;
			String target = "";
			Iterator<Entry<String, String>> i = j.next().getValue().entrySet().iterator();
			while(i.hasNext()){
				Entry<String, String> e = i.next();
				if(e.getKey().equals("nodeType")) {
					if(e.getValue().equals("pc")) isInfrastructure = true;
					else break;
				}
				if(e.getKey().equals("neighbour")) target = e.getValue();
			}
			
			if(isInfrastructure) {
				Vector<Property> tmpProperties = new Vector<Property>();
				tmpProperties.add(new Property("steering-req", id));
				tmpProperties.add(new Property("path", this.opMng.getSpaceName()));
				tmpProperties.add(new Property(PropertyName.DECAY.toString(), "15"));
				tmpProperties.add(new Property(PropertyName.DESTINATION.toString(), target));
				tmpProperties.add(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
				
				new MessagePropagationAgent("Init: "+target).propagateLSA(tmpProperties);
			}
		}
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
//		System.out.println("Update");
	}

	public Vector<String> getCurrentContent() {
		return currentContent;
	}

	public void onNeighbourFound(String key, HashMap<String, String> data) {
		neighbours.put(key, data);		
	}

	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		if(neighbours.containsKey(key))
			neighbours.remove(key);	
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		setInitialLSA();		
	}

	@Override
	public void setInitialLSA() {		
		addProperty(new Property("user", this.agentName));
		
		addSubDescription("steering-1", new Property("steering", "*"));
		addSubDescription("steering-2", new Property("steering-req", "*"));
		addSubDescription("steering-3", new Property("steering-ret", "*"));
	}

	public void addVisualizationListener(VisualizationService list) {
		listeners.add(list);
	}

	private void computeDirection(String id, String loc) {
		int targetX = Integer.parseInt(loc.split(":")[0]);
		int targetY = Integer.parseInt(loc.split(":")[1]);

		Vector2f ab = new Vector2f(targetX-myX, targetY-myY); 
		float angle = (float) (180/Math.PI * ab.angle(new Vector2f(0,1)));
		
		if(myX > targetX) angle += 180;
		
//		angle -= Float.parseFloat(orientation);
		float length = ((int)(ab.length()*100))/100;
		directions.put(id, (int)(angle)+":"+length);

//		addProperty(new Property("direction: "+id, ""+(int)(angle)));
	}

	public void updateData(String or, String lat, String lon) {
		orientation = or;
		this.longitude = lon;
		this.latitude = lat;
		
		addProperty(new Property("orientation", orientation), new Property("longitude", longitude), new Property("latitude", latitude));
	}
}

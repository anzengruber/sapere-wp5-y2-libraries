/*
This file is part of the SAPERE WP5 Y2 demonstrator.

The SAPERE WP5 Y2 demonstrator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The SAPERE WP5 Y2 demonstrator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the SAPERE WP5 Y2 demonstrator. If not, see <http://www.gnu.org/licenses/>.

Copyright 2012 Alberto Rosi, Gabriella Castelli, Thomas Schmittner, Bernhard Anzengruber
*/

package displays;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.Map.Entry;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.values.PropertyName;
import eu.sapere.middleware.node.lsaspace.ecolaws.AggregationOperators;
import eu.sapere.middleware.node.networking.topology.NeighbourAnalyzer;
import eu.sapere.middleware.node.networking.topology.NeighbourListener;
import eu.sapere.middleware.node.notifier.event.BondAddedEvent;
import eu.sapere.middleware.node.notifier.event.BondRemovedEvent;
import eu.sapere.middleware.node.notifier.event.BondedLsaUpdateEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;

public class MetricExperimentationAgent extends SapereAgent implements NeighbourListener {
	private int bondsEstablished = 0;
	private long meanDelay = 0;
	
	private volatile Map<String, HashMap<String, String>> neighbours;
	private volatile Vector<String> currentContent = new Vector<String>();
	private Timer experimentationTimer;
	private FileWriter fos;
	public MetricExperimentationAgent(String name) {
		super(name);
		NeighbourAnalyzer.addListener(this);
		neighbours = new HashMap<String, HashMap<String, String>>();
		experimentationTimer = new Timer();
		
		try {
			fos = new FileWriter(new File("mean-delays.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private class Throughput extends TimerTask {
		private int nrOfMessages = 0;
		private ThroughputThread thread;
		
		public Throughput() {
			thread = new ThroughputThread();
			thread.start();
		}
		
		public void run() {
			while(nrOfMessages < 500) {
				Set<Entry<String, HashMap<String, String>>> entries = neighbours.entrySet();
				for(Iterator<Entry<String, HashMap<String, String>>> j = entries.iterator(); j.hasNext();) {
					boolean isInfrastructure = false;
					String target = "";
					Iterator<Entry<String, String>> i = j.next().getValue().entrySet().iterator();
					while(i.hasNext()){
						Entry<String, String> e = i.next();
						if(e.getKey().equals("nodeType")) {
							if(e.getValue().equals("pc")) isInfrastructure = true;
							else break;
						}
						if(e.getKey().equals("neighbour")) target = e.getValue();
					}
						
					if(isInfrastructure) {
						Vector<Property> tmpProperties = new Vector<Property>();
						tmpProperties.add(new Property("msgID", ""+nrOfMessages));
						tmpProperties.add(new Property("metric", "myMetric"));
						tmpProperties.add(new Property(PropertyName.DESTINATION.toString(), target));
						tmpProperties.add(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
	
						new MessagePropagationAgent(""+nrOfMessages).propagateLSA(tmpProperties);
					}
				}
				nrOfMessages++;
			
				long tmpTime = System.currentTimeMillis();
				while(System.currentTimeMillis()-tmpTime < (long)1500) {};
			}
			thread.stopRun();
		}
	}
	
	private class ThroughputThread extends Thread {
		private long lastUpdate;
		private int bondsAlreadyEstablished;
		boolean run;
		
		public ThroughputThread() {
			lastUpdate = System.currentTimeMillis();
			bondsAlreadyEstablished = 0;
			run = true;
		}
		
		public void stopRun() {
			run = false;	
		}

		public void run() {
			while(true) {
				if(System.currentTimeMillis()-lastUpdate > 1500) {
					int lsas = bondsEstablished-bondsAlreadyEstablished;
					System.out.println(lsas);
					bondsAlreadyEstablished = bondsEstablished;
					lastUpdate = System.currentTimeMillis();
					
					try {
						fos.write(lsas+"\r\n");
						fos.flush();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private class BondingDelay extends TimerTask {
		private int nrOfMessages = 0;
		
		public BondingDelay() {
			
		}
		
		public void run() {
			while(nrOfMessages < 500) {
				Set<Entry<String, HashMap<String, String>>> entries = neighbours.entrySet();
				for(Iterator<Entry<String, HashMap<String, String>>> j = entries.iterator(); j.hasNext();) {
					boolean isInfrastructure = false;
					String target = "";
					Iterator<Entry<String, String>> i = j.next().getValue().entrySet().iterator();
					while(i.hasNext()){
						Entry<String, String> e = i.next();
						if(e.getKey().equals("nodeType")) {
							if(e.getValue().equals("pc")) isInfrastructure = true;
							else break;
						}
						if(e.getKey().equals("neighbour")) target = e.getValue();
					}
						
					if(isInfrastructure) {
						Vector<Property> tmpProperties = new Vector<Property>();
						tmpProperties.add(new Property("msgID", ""+nrOfMessages));
						tmpProperties.add(new Property("metric", ""+System.currentTimeMillis()));
						tmpProperties.add(new Property(PropertyName.DESTINATION.toString(), target));
						tmpProperties.add(new Property(PropertyName.DIFFUSION_OP.toString(), "direct"));
	
						new MessagePropagationAgent(""+nrOfMessages).propagateLSA(tmpProperties);
					}
				}
				nrOfMessages++;
			
				long tmpTime = System.currentTimeMillis();
				while(System.currentTimeMillis()-tmpTime < (long)1500) {};
			}
		}
	}

	
//	--------------private Methods----------------------------------------------------
	
//	private void addDataLsa(Property property) {
//		properties.add(property);
//		new DataLsa(new Lsa().addProperty(property));
//	}
	
//	--------------Overrides----------------------------------------------------------
	
	@Override
	public void setInitialLSA() {
		addProperty(new Property("user", this.agentName));
		addSubDescription("content", new Property("metric", "*"));

		addGradient(3, AggregationOperators.MAX.toString(), "dist");
		
//		experimentationTimer.schedule(new Throughput(), 10000);
//		experimentationTimer.schedule(new BondingDelay(), 10000);
	}
	
	@Override
	public void onBondAddedNotification(BondAddedEvent event) {
		
		//Uncomment for Troughput Experiment
		Property destination = event.getBondedLsa().getProperty(PropertyName.DESTINATION.toString());
		if(destination == null || destination.getValue().elementAt(0).equals(this.opMng.getSpaceName())) {
			++bondsEstablished;
			event.getBondedLsa().addProperty(new Property(PropertyName.DECAY.toString(), "1"));
		}
		
		//Uncomment for BondDelay Experiment
//		Property destination = event.getBondedLsa().getProperty(PropertyName.DESTINATION.toString());
//		if(destination == null || destination.getValue().elementAt(0).equals(this.opMng.getSpaceName())) {
//			++bondsEstablished;
//			long currentTime = System.currentTimeMillis();
//			long propTime = Long.parseLong(event.getBondedLsa().getProperty("metric").getValue().elementAt(0));
//			meanDelay = ( meanDelay*(bondsEstablished-1) + (currentTime-propTime)) /bondsEstablished; 
//			event.getBondedLsa().addProperty(new Property(PropertyName.DECAY.toString(), "1"));
//			
//			System.out.println("Bond: "+bondsEstablished+"; MeanDelay: " + meanDelay);
//			try {
//				fos.write(meanDelay+"\r\n");
//				fos.flush();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}

	@Override
	public void onBondRemovedNotification(BondRemovedEvent event) {
	}

	@Override
	public void onBondedLsaUpdateEventNotification(BondedLsaUpdateEvent event) {
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		
	}

	public Vector<String> getCurrentContent() {
		return currentContent;
	}

	@Override
	public void onNeighbourFound(String key, HashMap<String, String> data) {
		neighbours.put(key, data);		
	}

	@Override
	public void onNeighbourExpired(String key, HashMap<String, String> data) {
		if(neighbours.containsKey(key))
			neighbours.remove(key);	
	}

	

}
